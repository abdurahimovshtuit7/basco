import 'dart:convert';

import 'package:basco/constants/endpoints.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/account.dart';
import 'package:basco/models/location.dart';
import 'package:basco/models/orders.dart';
import 'package:basco/models/payment.dart';
import 'package:basco/models/revise_model.dart';
import 'package:basco/models/user.dart';
import 'package:basco/models/user_base.dart';
import 'package:basco/models/user_client.dart';
import 'package:basco/utils/exceptions.dart';
import 'package:basco/utils/header_options.dart';
import 'package:basco/utils/services.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:dio/dio.dart';

class ServicesRepository {
  static final UserHive _hive = di.get<UserHive>();

  Future<dynamic> signIn(String username, String password) async {
    List<AccountData> baza = await _hive.getAccount();

    try {
      var device = await SharedPref().read('deviceID') ?? '';

      await syncOrders();
      await syncPayments();
      await sendLocation();
      var data = {
        "login": username,
        "password": password,
        "device_id":  device,//"afb2387d5c8f661b"//
      };

      Response response = await (await HeaderOptions.getDio()).post(
        Endpoint.login,
        data: json.encode(data),
        options: await HeaderOptions().option(),
      );
      print("REQUEST:${data}");

      var clients;
      var userBase;
      print("RES:${response.data}");
      if (response.data["error"] != "null") {
        throw response.data["error"];
      } else {
        print("ID:${response.data["user_id"]}");
// get clients
        try {
          Response res = await (await HeaderOptions.getDio()).post(
            Endpoint.clients,
            data: json.encode({"user_id": response.data["user_id"]}),
            options: await HeaderOptions().option(),
          );
          if (res.data["error"] != "null") {
            throw res.data["error"];
          } else {
            clients = await UserClient.fromJson(res.data);
          }
        } catch (e) {
          throw e;
        }
        print("RESPONSE_@");
// get user base
        try {
          Response response_data = await (await HeaderOptions.getDio()).post(
            Endpoint.getUserBase,
            data: json.encode({"user_id": response.data["user_id"]}),
            options: await HeaderOptions().option(),
          );
          if (response_data.data["error"] != "null") {
            throw response_data.data["error"];
          } else {
            print("RES:${response_data.data}");
            userBase = UserBase.fromJson(response_data.data);
          }
        } catch (e) {
          throw e;
        }
//
        String name = await SharedPref().read("activeUser");
        print("RESPONSE_1");

        var data = await User.fromJson(response.data);

        Map<String, dynamic> map = {
          "user": data,
          "clients": clients,
          "user_base": userBase,
          "login": username,
          "password": password,
          "trade": [],
          "payment": [],
          "location": []
        };

        baza.firstWhere((x) => x.name == name).data.addAll(map);

        await _hive.saveAccount(baza);

        // activeData.data.addAll(map);
        print("NAME:${name}");
        print("ACtive:${baza.firstWhere((x) => x.name == name).data}");
      }
    } on DioError catch (e) {
      var errorMessage = DioExceptions.fromDioError(e).toString();
      if (e.type == DioErrorType.response) {
        print("Error:${e}");
      }

      throw errorMessage;
    }
  }

  Future<dynamic> revise(
      String fromDate, String toDate, String userId, String clientId) async {
    List<AccountData> baza = await _hive.getAccount();
    try {
      var data = {
        "data1": fromDate,
        "data2": toDate,
        "mijoz_id": clientId,
        "user_id": userId,
      };
      Response response = await (await HeaderOptions.getDio()).post(
        Endpoint.revise,
        data: json.encode(data),
        options: await HeaderOptions().option(),
      );
      var responseData;
      if (response.data["error"] != "null") {
        throw response.data["error"];
      } else {
        print(response.data);

        responseData = ReviseModel.fromJson(response.data);
        return responseData;
      }
    } on DioError catch (e) {
      var errorMessage = DioExceptions.fromDioError(e).toString();
      if (e.type == DioErrorType.response) {
        print("Error:${e.response!.data}");
      }

      throw errorMessage;
    }
  }

  Future<dynamic> order(dynamic order) async {
    List<AccountData> baza = await _hive.getAccount();
    try {
      Orders data = order;

      Response response = await (await HeaderOptions.getDio()).post(
        Endpoint.order,
        data: json.encode(data),
        options: await HeaderOptions().option(),
      );
      print("JSON:${json.encode(data)}");
      var responseData;
      if (response.data["error"] != "null") {
        throw response.data["error"];
      } else {
        print(response.data);

        // responseData = ReviseModel.fromJson(response.data);
        return responseData;
      }
    } on DioError catch (e) {
      var errorMessage = DioExceptions.fromDioError(e).toString();
      if (e.type == DioErrorType.response) {
        print("Error:${e.response!.data}");
       errorMessage = e.response!.data;

      }

      throw errorMessage;
    }
  }

  Future<dynamic> orders(dynamic orderList) async {
    List<AccountData> baza = await _hive.getAccount();
    try {
      dynamic data = {"hujjatlar": orderList};

      Response response = await (await HeaderOptions.getDio()).post(
        Endpoint.orders,
        data: json.encode(data),
        options: await HeaderOptions().option(),
      );
      var responseData;
      if (response.data["error"] != "null") {
        throw response.data["error"];
      } else {
        print(response.data);

        // responseData = ReviseModel.fromJson(response.data);
        return responseData;
      }
    } on DioError catch (e) {
      var errorMessage = DioExceptions.fromDioError(e).toString();
      if (e.type == DioErrorType.response) {
        print("Error:${e.response!.data}");
      }

      throw errorMessage;
    }
  }

  Future<dynamic> payment(dynamic payment) async {
    List<AccountData> baza = await _hive.getAccount();
    try {
      Payment data = payment;

      Response response = await (await HeaderOptions.getDio()).post(
        Endpoint.payment,
        data: json.encode(data),
        options: await HeaderOptions().option(),
      );
      var responseData;
      if (response.data["error"] != "null") {
        throw response.data["error"];
      } else {
        print(response.data);

        // responseData = ReviseModel.fromJson(response.data);
        return responseData;
      }
    } on DioError catch (e) {
      var errorMessage = DioExceptions.fromDioError(e).toString();
      if (e.type == DioErrorType.response) {
        print("Error:${e.response!.data}");
      }

      throw errorMessage;
    }
  }

  Future<dynamic> payments(dynamic paymentList) async {
    List<AccountData> baza = await _hive.getAccount();
    try {
      dynamic data = {"hujjatlar": paymentList};

      Response response = await (await HeaderOptions.getDio()).post(
        Endpoint.payments,
        data: json.encode(data),
        options: await HeaderOptions().option(),
      );
      var responseData;
      if (response.data["error"] != "null") {
        throw response.data["error"];
      } else {
        print(response.data);

        // responseData = ReviseModel.fromJson(response.data);
        return responseData;
      }
    } on DioError catch (e) {
      var errorMessage = DioExceptions.fromDioError(e).toString();
      if (e.type == DioErrorType.response) {
        print("Error:${e.response!.data}");
      }

      throw errorMessage;
    }
  }

  Future<dynamic> syncOrders() async {
    String name = await SharedPref().read("activeUser");
    List<AccountData> baza = await _hive.getAccount();

    List<Orders> trades =
        baza.firstWhere((x) => x.name == name).data["trade"] != null
            ? (List<Orders>.from(baza
                .firstWhere((x) => x.name == name)
                .data["trade"]
                .map((x) => Orders.fromJson(x))))
            : [];
    List<Orders> array = [];
    for (int i = 0; i < trades.length; i++) {
      if (!trades[i].send) {
        array.add(trades[i]);
      }
    }
    print("DATASD");
    if (array.isNotEmpty) {
      final res = await orders(array);
      // print("RESPONSE:${res}");
    }
    print("Arrray:${array}");
  }

  Future<dynamic> syncPayments() async {
    String name = await SharedPref().read("activeUser");
    List<AccountData> baza = await _hive.getAccount();

    List<Payment> payment =
        baza.firstWhere((x) => x.name == name).data["payment"] != null
            ? (List<Payment>.from(baza
                .firstWhere((x) => x.name == name)
                .data["payment"]
                .map((x) => Payment.fromJson(x))))
            : [];
    List<Payment> array = [];
    for (int i = 0; i < payment.length; i++) {
      if (!payment[i].send) {
        array.add(payment[i]);
      }
    }
    print("DATASD");
    if (array.isNotEmpty) {
      final res = await payments(array);
      // print("RESPONSE:${res}");
    }
    print("Arrray:${array}");
  }

  Future<dynamic> sendLocation() async {
    String name = await SharedPref().read("activeUser");
    List<AccountData> baza = await _hive.getAccount();
    List<ClientLocation> tracking =
        baza.firstWhere((x) => x.name == name).data["location"] != null
            ? (List<ClientLocation>.from(baza
                .firstWhere((x) => x.name == name)
                .data["location"]
                .map((x) => ClientLocation.fromJson(x))))
            : [];
    print("Client_location:${tracking}");
    if (tracking.isNotEmpty) {
      try {
        dynamic data = {"tracker": tracking};

        Response response = await (await HeaderOptions.getDio()).post(
          Endpoint.location,
          data: json.encode(data),
          options: await HeaderOptions().option(),
        );

        print("TRACK:${response.data}");

        return response.data;
      } on DioError catch (e) {
        var errorMessage = DioExceptions.fromDioError(e).toString();
        if (e.type == DioErrorType.response) {
          print("Error:${e.response!.data}");
        }

        throw errorMessage;
      }
    }
  }
}
