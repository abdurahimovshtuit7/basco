// To parse this JSON data, do
//
//     final reviseModel = reviseModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

ReviseModel reviseModelFromJson(String str) => ReviseModel.fromJson(json.decode(str));

String reviseModelToJson(ReviseModel data) => json.encode(data.toJson());

class ReviseModel {
    ReviseModel({
        required this.mijozName,
        required this.data1,
        required this.data2,
        required this.malumotlar,
        required this.error,
        required this.sinxronVaqti,
    });

    final String mijozName;
    final DateTime data1;
    final DateTime data2;
    final List<ReviseModelMalumotlar> malumotlar;
    final String? error;
    final DateTime sinxronVaqti;

    factory ReviseModel.fromJson(Map<String, dynamic> json) => ReviseModel(
        mijozName: json["mijoz_name"],
        data1: DateTime.parse(json["data1"]),
        data2: DateTime.parse(json["data2"]),
        malumotlar: List<ReviseModelMalumotlar>.from(json["malumotlar"].map((x) => ReviseModelMalumotlar.fromJson(x))),
        error: json["error"],
        sinxronVaqti: DateTime.parse(json["sinxron_vaqti"]),
    );

    Map<String, dynamic> toJson() => {
        "mijoz_name": mijozName,
        "data1": data1.toIso8601String(),
        "data2": data2.toIso8601String(),
        "malumotlar": List<dynamic>.from(malumotlar.map((x) => x.toJson())),
        "error": error,
        "sinxron_vaqti": sinxronVaqti.toIso8601String(),
    };
}

class ReviseModelMalumotlar {
    ReviseModelMalumotlar({
        required this.valyutaId,
        required this.valyuta,
        required this.boshQoldiq,
        required this.bizgaKirim,
        required this.bizdanChiqim,
        required this.aylanma,
        required this.oxirgiQoldiq,
        required this.malumotlar,
    });

    final String valyutaId;
    final String valyuta;
    final num boshQoldiq;
    final num bizgaKirim;
    final num bizdanChiqim;
    final num aylanma;
    final num oxirgiQoldiq;
    final List<MalumotlarMalumotlar> malumotlar;

    factory ReviseModelMalumotlar.fromJson(Map<String, dynamic> json) => ReviseModelMalumotlar(
        valyutaId: json["valyuta_id"],
        valyuta: json["valyuta"],
        boshQoldiq: json["bosh_qoldiq"],
        bizgaKirim: json["bizga_kirim"],
        bizdanChiqim: json["bizdan_chiqim"].toDouble(),
        aylanma: json["aylanma"].toDouble(),
        oxirgiQoldiq: json["oxirgi_qoldiq"].toDouble(),
        malumotlar: List<MalumotlarMalumotlar>.from(json["malumotlar"].map((x) => MalumotlarMalumotlar.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "valyuta_id": valyutaId,
        "valyuta": valyuta,
        "bosh_qoldiq": boshQoldiq,
        "bizga_kirim": bizgaKirim,
        "bizdan_chiqim": bizdanChiqim,
        "aylanma": aylanma,
        "oxirgi_qoldiq": oxirgiQoldiq,
        "malumotlar": List<dynamic>.from(malumotlar.map((x) => x.toJson())),
    };
}

class MalumotlarMalumotlar {
    MalumotlarMalumotlar({
        required this.data,
        required this.izox,
        required this.summa,
    });

    final DateTime data;
    final String izox;
    final num summa;

    factory MalumotlarMalumotlar.fromJson(Map<String, dynamic> json) => MalumotlarMalumotlar(
        data: DateTime.parse(json["data"]),
        izox: json["izox"],
        summa: json["summa"].toDouble(),
    );

    Map<String, dynamic> toJson() => {
        "data": data.toIso8601String(),
        "izox": izox,
        "summa": summa,
    };
}
