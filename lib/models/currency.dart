// To parse this JSON data, do
//
//     final clientCurrency = clientCurrencyFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<ClientCurrency> clientCurrencyFromJson(String str) => List<ClientCurrency>.from(json.decode(str).map((x) => ClientCurrency.fromJson(x)));

String clientCurrencyToJson(List<ClientCurrency> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ClientCurrency {
    ClientCurrency({
        required this.clientId,
        required this.valyutaId,
    });

    final String clientId;
    final String valyutaId;

    factory ClientCurrency.fromJson(Map<String, dynamic> json) => ClientCurrency(
        clientId: json["client_id"],
        valyutaId: json["valyuta_id"],
    );

    Map<String, dynamic> toJson() => {
        "client_id": clientId,
        "valyuta_id": valyutaId,
    };
}
