// To parse this JSON data, do
//
//     final userBase = userBaseFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

UserBase userBaseFromJson(String str) => UserBase.fromJson(json.decode(str));

String userBaseToJson(UserBase data) => json.encode(data.toJson());

class UserBase {
    UserBase({
        required this.jamiSoni,
        required this.jamiSummaSum,
        required this.jamiSummaVal,
        required this.sinxronVaqti,
        required this.response,
        required this.strixKodlar,
        required this.sotishNarxlari,
        required this.error,
    });

    final double jamiSoni;
    final double? jamiSummaSum;
    final double? jamiSummaVal;
    final DateTime sinxronVaqti;
    final List<Res> response;
    final List<StrixKodlar> strixKodlar;
    final List<SotishNarxlari> sotishNarxlari;
    final String? error;

    factory UserBase.fromJson(Map<String, dynamic> json) => UserBase(
        jamiSoni: json["JamiSoni"].toDouble(),
        jamiSummaSum: json["JamiSummaSum"].toDouble(),
        jamiSummaVal: json["JamiSummaVal"].toDouble(),
        sinxronVaqti: DateTime.parse(json["sinxron_vaqti"]),
        response: List<Res>.from(json["response"].map((x) => Res.fromJson(x))),
        strixKodlar: List<StrixKodlar>.from(json["strix_kodlar"].map((x) => StrixKodlar.fromJson(x))),
        sotishNarxlari: List<SotishNarxlari>.from(json["sotish_narxlari"].map((x) => SotishNarxlari.fromJson(x))),
        error: json["error"],
    );

    Map<String, dynamic> toJson() => {
        "JamiSoni": jamiSoni,
        "JamiSummaSum": jamiSummaSum,
        "JamiSummaVal": jamiSummaVal,
        "sinxron_vaqti": sinxronVaqti.toIso8601String(),
        "response": List<dynamic>.from(response.map((x) => x.toJson())),
        "strix_kodlar": List<dynamic>.from(strixKodlar.map((x) => x.toJson())),
        "sotish_narxlari": List<dynamic>.from(sotishNarxlari.map((x) => x.toJson())),
        "error": error,
    };
}

// enum Error { EMPTY, ERROR, PURPLE, NULL }

// final errorValues = EnumValues({
//     "": Error.EMPTY,
//     "Чакана Нарх": Error.ERROR,
//     "null": Error.NULL,
//     "Улгуржи Савдо": Error.PURPLE
// });

class Res {
    Res({
        required this.ombor,
        required this.omborId,
        required this.tovarlar,
    });

    final String ombor;
    final String omborId;
    final List<Tovarlar> tovarlar;

    factory Res.fromJson(Map<String, dynamic> json) => Res(
        ombor: json["Ombor"],
        omborId: json["Ombor_id"],
        tovarlar: List<Tovarlar>.from(json["tovarlar"].map((x) => Tovarlar.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "Ombor": ombor,
        "Ombor_id": omborId,
        "tovarlar": List<dynamic>.from(tovarlar.map((x) => x.toJson())),
    };
}

class Tovarlar {
    Tovarlar({
        required this.tovarId,
        required this.tovarName,
        required this.xarakId,
        required this.xarakName,
        required this.tovarSoni,
        required this.summaSum,
        required this.summaVal,
        required this.tannarxSum,
        required this.tannarxVal,
        required this.omborId,
    });

    final String tovarId;
    final String tovarName;
    final String xarakId;
    final String xarakName;
    final double tovarSoni;
    final double? summaSum;
    final double? summaVal;
    final double? tannarxSum;
    final double? tannarxVal;
    final String omborId;

    factory Tovarlar.fromJson(Map<String, dynamic> json) => Tovarlar(
        tovarId: json["tovar_id"],
        tovarName: json["tovar_name"],
        xarakId: json["xarak_id"],
        xarakName: json["xarak_name"],
        tovarSoni: json["tovar_soni"].toDouble(),
        summaSum: json["SummaSum"].toDouble(),
        summaVal: json["SummaVal"].toDouble(),
        tannarxSum: json["TannarxSum"].toDouble(),
        tannarxVal: json["TannarxVal"].toDouble(),
        omborId: json["ombor_id"],
    );

    Map<String, dynamic> toJson() => {
        "tovar_id": tovarId,
        "tovar_name": tovarName,
        "xarak_id": xarakId,
        "xarak_name": xarakName,
        "tovar_soni": tovarSoni,
        "SummaSum": summaSum,
        "SummaVal": summaVal,
        "TannarxSum": tannarxSum,
        "TannarxVal": tannarxVal,
        "ombor_id": omborId,
    };
}



class SotishNarxlari {
    SotishNarxlari({
        required this.tovarId,
        required this.tovarName,
        required this.xarakId,
        required this.xarakName,
        required this.narxTuriId,
        required this.narxTuriName,
        required this.sumNarxi,
        required this.dollarNarxi,
        required this.quyiSumNarxi,
        required this.quyiDollarNarxi,
        required this.yuqoriSumNarxi,
        required this.yuqoriDollarNarxi,
    });

    final String tovarId;
    final String tovarName;
    final String xarakId;
    final String xarakName;
    final String narxTuriId;
    final String narxTuriName;
    final double? sumNarxi;
    final double? dollarNarxi;
    final double? quyiSumNarxi;
    final double? quyiDollarNarxi;
    final double? yuqoriSumNarxi;
    final double? yuqoriDollarNarxi;

    factory SotishNarxlari.fromJson(Map<String, dynamic> json) => SotishNarxlari(
        tovarId: json["tovar_id"],
        tovarName: json["tovar_name"],
        xarakId: json["xarak_id"],
        xarakName: json["xarak_name"],
        narxTuriId: json["narxTuri_id"],
        narxTuriName: json["narxTuri_name"],
        sumNarxi: json["sum_narxi"].toDouble(),
        dollarNarxi: json["dollar_narxi"].toDouble(),//.toDouble(),
        quyiSumNarxi: json["quyi_sum_narxi"].toDouble(),
        quyiDollarNarxi: json["quyi_dollar_narxi"].toDouble(),
        yuqoriSumNarxi: json["yuqori_sum_narxi"].toDouble(),
        yuqoriDollarNarxi: json["yuqori_dollar_narxi"].toDouble(),
    );

    Map<String, dynamic> toJson() => {
        "tovar_id": tovarId,
        "tovar_name": tovarName,
        "xarak_id": xarakId,
        "xarak_name": xarakName,
        "narxTuri_id": narxTuriId,
        "narxTuri_name": narxTuriName,
        "sum_narxi": sumNarxi,
        "dollar_narxi": dollarNarxi,
        "quyi_sum_narxi": quyiSumNarxi,
        "quyi_dollar_narxi": quyiDollarNarxi,
        "yuqori_sum_narxi": yuqoriSumNarxi,
        "yuqori_dollar_narxi": yuqoriDollarNarxi,
    };
}

class StrixKodlar {
    StrixKodlar({
        required this.tovarId,
        required this.tovarName,
        required this.xarakId,
        required this.xarakName,
        required this.shtrixKod,
        required this.soni,
    });

    final String tovarId;
    final String tovarName;
    final String xarakId;
    final String xarakName;
    final String shtrixKod;
    final int soni;

    factory StrixKodlar.fromJson(Map<String, dynamic> json) => StrixKodlar(
        tovarId: json["tovar_id"],
        tovarName: json["tovar_name"],
        xarakId: json["xarak_id"],
        xarakName: json["xarak_name"],
        shtrixKod: json["shtrix_kod"],
        soni: json["soni"],
    );

    Map<String, dynamic> toJson() => {
        "tovar_id": tovarId,
        "tovar_name": tovarName,
        "xarak_id": xarakId,
        "xarak_name": xarakName,
        "shtrix_kod": shtrixKod,
        "soni": soni,
    };
}

