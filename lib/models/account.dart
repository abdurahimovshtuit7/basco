//     final accounts = accountsFromJson(jsonString);

import 'dart:convert';

List<AccountData> accountsFromJson(String str) => List<AccountData>.from(json.decode(str).map((x) => AccountData.fromJson(x)));

String accountsToJson(List<AccountData> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AccountData {
    AccountData({
        required this.name,
        required this.parol,
        required this.id,
        required this.data,
    });
    
    final String name;
    final String parol;
    final String id;
    Map<String,dynamic> data;
     factory AccountData.empty() => AccountData.fromJson({"name":'',"parol":"","id":'',"data":  Map<String, dynamic>()});

    factory AccountData.fromJson(Map<String, dynamic> json) => AccountData(
        name: json["name"],
        parol: json["parol"],
        id: json["id"],
        data:json["data"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "parol": parol,
        "id": id,
        "data": data,
    };
}

class Data {
    Data();

    factory Data.empty() => Data.fromJson({});


    factory Data.fromJson(Map<String, dynamic> json) => Data(
    );

    Map<String, dynamic> toJson() => {
    };

}
