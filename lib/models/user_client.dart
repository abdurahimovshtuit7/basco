// To parse this JSON data, do
//
//     final userClient = userClientFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

UserClient userClientFromJson(String str) => UserClient.fromJson(json.decode(str));

String userClientToJson(UserClient data) => json.encode(data.toJson());

class UserClient {
    UserClient({
        required this.mijozlar,
        required this.mijozOstatkalari,
        required this.oxirgiOperatsiyalar,
        required this.error,
        required this.sinxronVaqti,
    });
    final List<Mijozlar> mijozlar;
    final List<MijozOstatkalari> mijozOstatkalari;
    final List<OxirgiOperatsiyalar> oxirgiOperatsiyalar;
    final String? error;
    final DateTime sinxronVaqti;

    factory UserClient.fromJson(Map<String, dynamic> json) => UserClient(
        mijozlar: List<Mijozlar>.from(json["mijozlar"].map((x) => Mijozlar.fromJson(x))),
        mijozOstatkalari: List<MijozOstatkalari>.from(json["mijoz_ostatkalari"].map((x) => MijozOstatkalari.fromJson(x))),
        oxirgiOperatsiyalar: List<OxirgiOperatsiyalar>.from(json["oxirgi_operatsiyalar"].map((x) => OxirgiOperatsiyalar.fromJson(x))),
        error: json["error"],
        sinxronVaqti: DateTime.parse(json["sinxron_vaqti"]),
    );

    Map<String, dynamic> toJson() => {
        "mijozlar": List<dynamic>.from(mijozlar.map((x) => x.toJson())),
        "mijoz_ostatkalari": List<dynamic>.from(mijozOstatkalari.map((x) => x.toJson())),
        "oxirgi_operatsiyalar": List<dynamic>.from(oxirgiOperatsiyalar.map((x) => x.toJson())),
        "error": error,
        "sinxron_vaqti": sinxronVaqti.toIso8601String(),
    };
}

class MijozOstatkalari {
    MijozOstatkalari({
        required this.mijozId,
        required this.mijozName,
        required this.valyutaId,
        required this.valyuta,
        required this.qarzimiz,
        required this.haqqimiz,
    });

    final String mijozId;
    final String mijozName;
    final String valyutaId;
    final String valyuta;
    final double qarzimiz;
    final double haqqimiz;

    factory MijozOstatkalari.fromJson(Map<String, dynamic> json) => MijozOstatkalari(
        mijozId: json["mijoz_id"],
        mijozName: json["mijoz_name"],
        valyutaId: json["valyuta_id"],
        valyuta:json["valyuta"],
        qarzimiz: json["qarzimiz"].toDouble(),
        haqqimiz: json["haqqimiz"].toDouble(),
    );

    Map<String, dynamic> toJson() => {
        "mijoz_id": mijozId,
        "mijoz_name": mijozName,
        "valyuta_id": valyutaId,
        "valyuta": valyuta,
        "qarzimiz": qarzimiz,
        "haqqimiz": haqqimiz,
    };
}

class Mijozlar {
    Mijozlar({
        required this.mijozId,
        required this.mijozName,
        required this.mijozPhone,
        required this.mijozAddress,
        required this.narxTuri,
        required this.narxTuriId,
        required this.mijozLat,
        required this.mijozLong
    });

    final String mijozId;
    final String mijozName;
    final String mijozPhone;
    final String mijozAddress;
    final String narxTuri;
    final String narxTuriId;
    final String mijozLat;
    final String mijozLong;


    factory Mijozlar.fromJson(Map<String, dynamic> json) => Mijozlar(
        mijozId: json["mijoz_id"],
        mijozName: json["mijoz_name"],
        mijozPhone: json["mijoz_phone"],
        mijozAddress: json["mijoz_address"],
        narxTuri: json["narx_turi"],
        narxTuriId: json["narx_turi_id"],
        mijozLat:json["mijoz_lat"],
        mijozLong:json["mijoz_long"]
    );

    Map<String, dynamic> toJson() => {
        "mijoz_id": mijozId,
        "mijoz_name": mijozName,
        "mijoz_phone": mijozPhone,
        "mijoz_address": mijozAddress,
        "narx_turi": narxTuri,
        "narx_turi_id": narxTuriId,
        "mijoz_lat": mijozLat,
        "mijoz_long":mijozLong,
    };
}

class OxirgiOperatsiyalar {
    OxirgiOperatsiyalar({
        required this.mijozId,
        required this.mijozName,
        required this.sana,
        required this.izoh,
        required this.valyuta,
        required this.summa,
    });

    final String mijozId;
    final String mijozName;
    final DateTime sana;
    final String izoh;
    final String valyuta;
    final double summa;

    factory OxirgiOperatsiyalar.fromJson(Map<String, dynamic> json) => OxirgiOperatsiyalar(
        mijozId: json["mijoz_id"],
        mijozName: json["mijoz_name"],
        sana: DateTime.parse(json["sana"]),
        izoh: json["izoh"],
        valyuta: json["valyuta"],
        summa: json["summa"].toDouble(),
    );

    Map<String, dynamic> toJson() => {
        "mijoz_id": mijozId,
        "mijoz_name": mijozName,
        "sana": sana.toIso8601String(),
        "izoh": izoh,
        "valyuta":valyuta,
        "summa": summa,
    };
}


