// To parse this JSON data, do
//
//     final clientLocation = clientLocationFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<ClientLocation> clientLocationFromJson(String str) =>
    List<ClientLocation>.from(
        json.decode(str).map((x) => ClientLocation.fromJson(x)));

String clientLocationToJson(List<ClientLocation> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ClientLocation {
  ClientLocation({
    required this.userId,
    required this.mijozId,
    required this.latitude,
    required this.longtitude,
    required this.startLocation,
    required this.data,
  });

  final String userId;
  final String mijozId;
  String latitude;
  String longtitude;
  final String startLocation;
  final String data;

  set setLat(String lat) {
    latitude = lat;
  }

  set setLong(String long) {
    longtitude = long;
  }

  factory ClientLocation.fromJson(Map<String, dynamic> json) => ClientLocation(
        userId: json["user_id"],
        mijozId: json["mijoz_id"],
        latitude: json["latitude"],
        longtitude: json["longtitude"],
        startLocation: json["start_location"],
        data: json["data"],
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "mijoz_id": mijozId,
        "latitude": latitude,
        "longtitude": longtitude,
        "start_location": startLocation,
        "data": data,
      };
}
