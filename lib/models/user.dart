// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
    User({
        required this.userName,
        required this.userId,
        required this.userType,
        required this.narxUzgartirish,
        required this.quyiNarxFoizi,
        required this.yuqoriNarxFoizi,
        required this.omborlar,
        required this.kassalar,
        required this.valyutalar,
        required this.dollarId,
        required this.sumId,
        required this.deviceId,
        required this.status,
        required this.error,
    });

    final String userName;
    final String userId;
    final String userType;
    final bool narxUzgartirish;
    final double? quyiNarxFoizi;
    final double? yuqoriNarxFoizi;
    final List<Omborlar> omborlar;
    final List<Kassalar> kassalar;
    final List<Valyutalar> valyutalar;
    final String dollarId;
    final String sumId;
    final String deviceId;
    final String status;
    final String? error;

    factory User.fromJson(Map<String, dynamic> json) => User(
        userName: json["user_name"],
        userId: json["user_id"],
        userType: json["user_type"],
        narxUzgartirish: json["narx_uzgartirish"],
        quyiNarxFoizi: json["quyi_narx_foizi"].toDouble(),
        yuqoriNarxFoizi: json["yuqori_narx_foizi"].toDouble(),
        omborlar: List<Omborlar>.from(json["Omborlar"].map((x) => Omborlar.fromJson(x))),
        kassalar: List<Kassalar>.from(json["Kassalar"].map((x) => Kassalar.fromJson(x))),
        valyutalar: List<Valyutalar>.from(json["valyutalar"].map((x) => Valyutalar.fromJson(x))),
        dollarId: json["dollar_id"],
        sumId: json["sum_id"],
        deviceId: json["device_id"],
        status: json["status"],
        error: json["error"],
    );

    Map<String, dynamic> toJson() => {
        "user_name": userName,
        "user_id": userId,
        "user_type": userType,
        "narx_uzgartirish": narxUzgartirish,
        "quyi_narx_foizi": quyiNarxFoizi,
        "yuqori_narx_foizi": yuqoriNarxFoizi,
        "Omborlar": List<dynamic>.from(omborlar.map((x) => x.toJson())),
        "Kassalar": List<dynamic>.from(kassalar.map((x) => x.toJson())),
        "valyutalar": List<dynamic>.from(valyutalar.map((x) => x.toJson())),
        "dollar_id": dollarId,
        "sum_id": sumId,
        "device_id": deviceId,
        "status": status,
        "error": error,
    };
}

class Kassalar {
    Kassalar({
        required this.kassaId,
        required this.kassa,
    });

    final String kassaId;
    final String kassa;

    factory Kassalar.fromJson(Map<String, dynamic> json) => Kassalar(
        kassaId: json["kassa_id"],
        kassa: json["Kassa"],
    );

    Map<String, dynamic> toJson() => {
        "kassa_id": kassaId,
        "Kassa": kassa,
    };
}

class Omborlar {
    Omborlar({
        required this.omborId,
        required this.ombor,
    });

    final String omborId;
    final String ombor;

    factory Omborlar.fromJson(Map<String, dynamic> json) => Omborlar(
        omborId: json["ombor_id"],
        ombor: json["Ombor"],
    );

    Map<String, dynamic> toJson() => {
        "ombor_id": omborId,
        "Ombor": ombor,
    };
}

class Valyutalar {
    Valyutalar({
        required this.valyutaId,
        required this.valyutaNomi,
        required this.valyutaKursi,
    });

    final String valyutaId;
    final String valyutaNomi;
    final double valyutaKursi;

    factory Valyutalar.fromJson(Map<String, dynamic> json) => Valyutalar(
        valyutaId: json["valyuta_id"],
        valyutaNomi: json["valyuta_nomi"],
        valyutaKursi: json["valyuta_kursi"].toDouble(),
    );

    Map<String, dynamic> toJson() => {
        "valyuta_id": valyutaId,
        "valyuta_nomi": valyutaNomi,
        "valyuta_kursi": valyutaKursi,
    };
}
