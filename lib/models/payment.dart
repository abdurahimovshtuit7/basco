import 'package:meta/meta.dart';
import 'dart:convert';

// Payment paymentFromJson(String str) => Payment.fromJson(json.decode(str));
// String paymentToJson(Payment data) => json.encode(data.toJson());

List<Payment> paymentFromJson(String str) =>
    List<Payment>.from(json.decode(str).map((x) => Payment.fromJson(x)));

String paymentToJson(List<Payment> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Payment {
    Payment({
        required this.id,
        required this.userId,
        required this.mijozId,
        required this.kassaId,
        required this.kassaValyutaId,
        required this.kirimSummasi,
        required this.mijozValyutaId,
        required this.mijozSummasi,
        required this.sinxronTime,
        required this.izox,
        required this.send,
        required this.date,
    });

    final int id;
    final String userId;
    final String mijozId;
    final String kassaId;
    final String kassaValyutaId;
    final String kirimSummasi;
    final String mijozValyutaId;
    final String mijozSummasi;
    final String sinxronTime;
    final String izox;
    bool send;
    final String date;


    set setSend(bool d) {
    send = d;
    }

    factory Payment.fromJson(Map<String, dynamic> json) => Payment(
        id: json["id"],
        userId: json["user_id"],
        mijozId: json["mijoz_id"],
        kassaId: json["kassa_id"],
        kassaValyutaId: json["kassa_valyuta_id"],
        kirimSummasi: json["kirim_summasi"],
        mijozValyutaId: json["mijoz_valyuta_id"],
        mijozSummasi: json["mijoz_summasi"],
        sinxronTime: json["sinxron_time"],
        izox: json["izox"],
        send:json["send"],
        date:json["date"]
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "mijoz_id": mijozId,
        "kassa_id": kassaId,
        "kassa_valyuta_id": kassaValyutaId,
        "kirim_summasi": kirimSummasi,
        "mijoz_valyuta_id": mijozValyutaId,
        "mijoz_summasi": mijozSummasi,
        "sinxron_time": sinxronTime,
        "izox": izox,
        "send":send,
        "date":date
    };
}
