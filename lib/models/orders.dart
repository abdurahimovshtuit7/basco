// To parse this JSON data, do
//
//     final orders = ordersFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<Orders> ordersFromJson(String str) =>
    List<Orders>.from(json.decode(str).map((x) => Orders.fromJson(x)));

String ordersToJson(List<Orders> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Orders {
  Orders({
    required this.userId,
    required this.mijozId,
    required this.omborId,
    required this.valyutaId,
    required this.sinxronTime,
    required this.izox,
    required this.ruyxat,
    required this.send,
    required this.id,
    required this.date,
    required this.deviceId,
    required this.vozvrat,
    required this.total
  });

  final String userId;
  final String mijozId;
  final String omborId;
  final String valyutaId;
  final String sinxronTime;
  final String izox;
  final List<Ruyxat> ruyxat;
  bool send;
  final int id;
  final String date;
  final String? deviceId;
  final bool vozvrat;
  final String total;

  set setSend(bool d) {
    send = d;
  }

  factory Orders.fromJson(Map<String, dynamic> json) => Orders(
      userId: json["user_id"],
      mijozId: json["mijoz_id"],
      omborId: json["ombor_id"],
      valyutaId: json["valyuta_id"],
      sinxronTime: json["sinxron_time"],
      izox: json["izox"],
      ruyxat: List<Ruyxat>.from(json["ruyxat"].map((x) => Ruyxat.fromJson(x))),
      send: json["send"],
      id: json["id"],
      date:json["date"],
      deviceId: json["device_id"],
      vozvrat:json["vozvrat"],
      total:json["total"],
      );
     

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "mijoz_id": mijozId,
        "ombor_id": omborId,
        "valyuta_id": valyutaId,
        "sinxron_time": sinxronTime,
        "izox": izox,
        "ruyxat": List<dynamic>.from(ruyxat.map((x) => x.toJson())),
        "send": send,
        "id":id,
        "date":date,
        "device_id":deviceId,
        "vozvrat":vozvrat,
        "total":total

      };
}

class Ruyxat {
  Ruyxat({
    required this.tovarId,
    required this.xarakId,
    required this.soni,
    required this.narxi,
  });

  final String tovarId;
  final String xarakId;
  final String soni;
  final String narxi;

  factory Ruyxat.fromJson(Map<String, dynamic> json) => Ruyxat(
        tovarId: json["tovar_id"],
        xarakId: json["xarak_id"],
        soni: json["soni"],
        narxi: json["narxi"],
      );

  Map<String, dynamic> toJson() => {
        "tovar_id": tovarId,
        "xarak_id": xarakId,
        "soni": soni,
        "narxi": narxi,
      };
}
