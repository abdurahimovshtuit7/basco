import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/models/currency.dart';
import 'package:basco/models/orders.dart';
import 'package:basco/models/payment.dart';
import 'package:basco/models/user_client.dart';
import 'package:basco/utils/services.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';
import 'package:collection/collection.dart';

class ExpandedWidget extends StatefulWidget {
  final dynamic item;
  final dynamic payments;
  final dynamic trades;
  final Function edit;
  final dynamic billingHistory;
  final dynamic currency;

  @override
  State<ExpandedWidget> createState() => _ExpandedWidgetState();

  const ExpandedWidget(
      {Key? key,
      required this.item,
      required this.payments,
      required this.trades,
      required this.currency,
      required this.edit,
      required this.billingHistory})
      : super(key: key);
}

class _ExpandedWidgetState extends State<ExpandedWidget> {
  ExpandableController _controller =
      ExpandableController(initialExpanded: false);
  List currency = [];

  @override
  Widget build(BuildContext context) {
    final item = widget.item[0];
    // print(widget.billingHistory);
    print("Itm: $item");
    List billingHistory = (widget.billingHistory as List<MijozOstatkalari>)
        .where((x) => x.mijozId == item.mijozId)
        .toList();
    final List clientTrades = (widget.trades as List<Orders>)
        .where((element) => element.mijozId == item.mijozId)
        .toList();
    final List clientPayment = (widget.payments as List<Payment>)
        .where((element) => element.mijozId == item.mijozId)
        .toList();

    List list = [];
    List listById = [];
    var seen = Set<String>();
//check
    for (int k = 0; k < clientTrades.length; k++) {
      bool isExist = false;

      for (int i = 0; i < billingHistory.length; i++) {
        if (clientTrades[k].valyutaId == billingHistory[i].valyutaId) {
          isExist = true;
          break;
        }
      }
      if (!isExist) {
        listById.add(ClientCurrency(
            clientId: clientTrades[k].mijozId,
            valyutaId: clientTrades[k].valyutaId));
      }
    }
    for (int k = 0; k < clientPayment.length; k++) {
      bool isExist = false;
      for (int i = 0; i < billingHistory.length; i++) {
        if (clientPayment[k].mijozValyutaId == billingHistory[i].valyutaId) {
          isExist = true;
          break;
        }
      }
      if (!isExist) {
        listById.add(ClientCurrency(
            clientId: clientPayment[k].mijozId,
            valyutaId: clientPayment[k].mijozValyutaId));
      }
    }
    list = listById.where((element) => seen.add(element.valyutaId)).toList();
    // for (int i = 0; i < billingHistory.length; i++) {
    //   for (int k = 0; k < clientTrades.length; k++) {
    //     if (clientTrades[k].valyutaId != billingHistory[i].valyutaId) {
    //       listById.add(ClientCurrency(clientId: clientTrades[k].mijozId, valyutaId: clientTrades[k].valyutaId));
    //     }
    //   }
    //    for (int k = 0; k < clientPayment.length; k++) {
    //     if (clientPayment[k].mijozValyutaId != billingHistory[i].valyutaId) {
    //       listById.add(ClientCurrency(clientId: clientPayment[k].mijozId, valyutaId: clientPayment[k].mijozValyutaId));
    //     }
    //   }
    // }
    print("ksdkd:${list.length}");
    for (int i = 0; i < list.length; i++) {
      String name = billingHistory[0].mijozName;
      String valyuta = widget.currency
          .firstWhere((element) => element.valyutaId == list[i].valyutaId)
          .valyutaNomi;
      billingHistory.add(MijozOstatkalari(
          mijozId: list[i].clientId,
          mijozName: name,
          valyuta: valyuta,
          valyutaId: list[i].valyutaId,
          haqqimiz: 0,
          qarzimiz: 0));
    }

    _toggleExpandables(int index) {
      setState(() {
        _controller.value = !_controller.value;

        // _getController(index).value = true;
      });
    }

    Widget buildItem(text, {color = AppColor.white, align = TextAlign.start}) {
      return Padding(
        padding: const EdgeInsets.only(left: 5, top: 15, bottom: 15),
        child: SelectableText(
          text,
          style: TextStyle(
              color: color, fontSize: 12, fontWeight: FontWeight.w600),
          textAlign: align,
        ),
      );
    }

    // buildItem(String label) {
    //   return Padding(
    //     padding: const EdgeInsets.all(10.0),
    //     child: Text(
    //       label,
    //       style: whiteText,
    //     ),
    //   );
    // }

    buildList() {
      return Column(children: [
        buildTableTop("Валюта", "Карзимиз", "Хаккимиз"),
        Container(
          height: billingHistory.length * 50, //230,
          child: MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: ListView(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: [
                Table(
                  defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                  columnWidths: {
                    0: FlexColumnWidth(1),
                    1: FlexColumnWidth(4),
                    2: FlexColumnWidth(4),
                    3: FlexColumnWidth(4),
                  },
                  children: billingHistory.asMap().entries.map((element) {
                    var index = element.key;
                    var value = element.value;
                    double fee = 0;
                    double loan = 0;

                    if (value.haqqimiz == 0 && value.qarzimiz == 0) {
                      if (storageLoan(item.mijozId, value.valyutaId) -
                              storageOrders(item.mijozId, value.valyutaId) >
                          0) {
                        loan = storageLoan(item.mijozId, value.valyutaId) -
                            storageOrders(item.mijozId, value.valyutaId);
                      } else {
                        fee = storageOrders(item.mijozId, value.valyutaId) -
                            storageLoan(item.mijozId, value.valyutaId);
                      }
                    } else {
                      if (value.haqqimiz == 0) {
                        loan = value.qarzimiz +
                            storageLoan(item.mijozId, value.valyutaId);
                      } else {
                        if ((value.haqqimiz -
                                storageLoan(item.mijozId, value.valyutaId)) >
                            0) {
                          fee = value.haqqimiz -
                              storageLoan(item.mijozId, value.valyutaId);
                          loan = 0;
                        } else {
                          loan = (value.haqqimiz -
                                  storageLoan(item.mijozId, value.valyutaId))
                              .abs();
                          fee = 0;
                        }
                      }
                      if (loan == 0) {
                        fee = value.haqqimiz +
                            storageOrders(item.mijozId, value.valyutaId);
                      } else {
                        if ((loan -
                                storageOrders(item.mijozId, value.valyutaId)) >
                            0) {
                          loan = loan -
                              storageOrders(item.mijozId, value.valyutaId);
                          fee = 0;
                        } else {
                          fee = fee +
                              (loan -
                                      storageOrders(
                                          item.mijozId, value.valyutaId))
                                  .abs();
                          loan = 0;
                        }
                      }
                    }

                    return TableRow(
                        decoration: BoxDecoration(
                            color: index % 2 == 0
                                ? AppColor.transparent
                                : AppColor.transparent,
                            border: Border(
                              bottom: BorderSide(
                                color: AppColor.brandColor,
                                width: 1,
                              ),
                            )),
                        children: [
                          buildItem((index + 1).toString(),
                              align: TextAlign.center),
                          buildItem(value.valyuta),
                          buildItem(Services.currencyFormatter(loan.toString()),
                              align: TextAlign.end),
                          buildItem(Services.currencyFormatter(fee.toString()),
                              align: TextAlign.end),
                        ]);
                  }).toList(),
                ),
              ],
            ),
          ),
        )
      ]);
      // return Container(
      //   padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      //   child: Column(
      //     children: <Widget>[
      //       ...widget.item.map((e) {
      //         var value = e;
      //         bool check = value.qarzimiz == 0;
      //         var loan = check
      //             ? value.haqqimiz
      //             : -value.qarzimiz +
      //                 storageLoan(value.mijozId, value.valyutaId);
      //         print("jj:${storageLoan(value.mijozId, value.valyutaId)}");
      //         return Column(
      //           children: [
      //             TextRow(
      //               text1: value.valyuta,
      //               text2: Services.currencyFormatter(loan.toString()),
      //               color: loan.toString()[0]=='-'?AppColor.red:AppColor.white,
      //             ),
      //             Padding(
      //               padding: const EdgeInsets.symmetric(vertical: 6),
      //               child: Divider(
      //                 color: AppColor.white,
      //               ),
      //             ),
      //           ],
      //         );
      //       }).toList()

      //     ],
      //   ),
      // );
    }

    return ExpandableNotifier(
        // controller: _controller,
        child: Padding(
      padding: const EdgeInsets.all(0),
      child: ScrollOnExpand(
        child: Card(
          clipBehavior: Clip.antiAlias,
          color: AppColor.black.withOpacity(0.3),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Column(
            children: <Widget>[
              ExpandablePanel(
                controller: _controller,
                theme: const ExpandableThemeData(
                    headerAlignment: ExpandablePanelHeaderAlignment.center,
                    tapBodyToExpand: false,
                    tapBodyToCollapse: false,
                    tapHeaderToExpand: false,
                    hasIcon: false,
                    useInkWell: true),
                header: Container(
                  decoration: BoxDecoration(
                      color: AppColor.black.withOpacity(0.7),
                      borderRadius: BorderRadius.circular(10)),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Material(
                            color: AppColor.transparent,
                            child: InkWell(
                              onTap: () => widget.edit(),
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 15.0),
                                child: Text(
                                  widget.item[0].mijozName,
                                  style: whiteText,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Material(
                          color: AppColor.transparent,
                          child: InkWell(
                            onTap: () {
                              print("object");
                              _toggleExpandables(0);
                            },
                            child: ExpandableIcon(
                              theme: const ExpandableThemeData(
                                expandIcon: Icons.chevron_right,
                                collapseIcon: Icons.keyboard_arrow_down_rounded,
                                iconColor: Colors.white,
                                iconSize: 30.0,
                                iconRotationAngle: math.pi / 2,
                                // iconPadding: EdgeInsets.only(right: 5),
                                hasIcon: false,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                collapsed: Container(),
                expanded: buildList(),
              ),
            ],
          ),
        ),
      ),
    ));
  }

  // storageLoan(mijozId, valyuta) {
  //   print(mijozId);
  //   print(valyuta);
  //   double sum = 0;
  //   print(widget.payments.map((e) => e.mijozId));
  //   print(widget.payments.map((e) => e.mijozValyutaId));
  //   for (int i = 0; i < widget.payments.length; i++) {
  //     if (widget.payments[i].mijozId == mijozId &&
  //         widget.payments[i].mijozValyutaId == valyuta) {
  //       sum += double.tryParse(widget.payments[i].mijozSummasi) ?? 0;
  //     }
  //   }
  //   return sum;
  // }
  Widget buildTableTop(title1, title2, title3) {
    return Table(
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        columnWidths: {
          0: FlexColumnWidth(1),
          1: FlexColumnWidth(4),
          2: FlexColumnWidth(4),
          3: FlexColumnWidth(4),
        },
        children: [
          TableRow(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  AppColor.brandColor,
                  AppColor.brandColor2,
                  AppColor.brandColor
                ],
              )),
              children: [
                buildItemLabel("N"),
                buildItemLabel(title1),
                buildItemLabel(title2),
                buildItemLabel(title3),
              ]),
        ]);
  }

  Widget buildItemLabel(text) {
    return Padding(
      padding: const EdgeInsets.only(left: 5, top: 15, bottom: 15),
      child: Text(
        text,
        style: TextStyle(
            fontSize: 15,
            color: AppColor.black,
            fontFamily: AppFont.MontserratBold,
            fontWeight: FontWeight.w600),
        // textAlign: TextAlign.center,
      ),
    );
  }

  storageLoan(mijozId, valyuta) {
    print(mijozId);
    print(valyuta);
    double sum = 0;
    for (int i = 0; i < widget.payments.length; i++) {
      if (widget.payments[i].mijozId == mijozId &&
          widget.payments[i].mijozValyutaId == valyuta) {
        sum += double.tryParse(widget.payments[i].mijozSummasi) ?? 0;
      }
    }
    //  sum += double.tryParse(payments.where((e) => (e.mijozId == mijozId )&&(e.mijozValyutaId == valyuta)).single.mijozSummasi)??0;
    print(sum);
    return sum;
  }

  storageOrders(mijozId, valyuta) {
    dynamic newlist =
        groupBy(widget.trades, (x) => (x as Orders).mijozId) ?? [];
    dynamic clientOrderList = newlist[mijozId] ?? [];
    dynamic currencyList =
        groupBy(clientOrderList, (x) => (x as Orders).valyutaId) ?? [];
    dynamic currency = currencyList[valyuta] ?? [];
    double c = 0;
    for (int i = 0; i < currency.length; i++) {
      c += double.tryParse(currency[i].total) ?? 0;
    }
    return c;
  }
}
