import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';

class LoanDescription extends StatefulWidget {
  const LoanDescription({Key? key}) : super(key: key);
  static const String routeName = 'loan-description';

  @override
  _LoanDescriptionState createState() => _LoanDescriptionState();
}

class _LoanDescriptionState extends State<LoanDescription> {
  late final args;
  // late Map item={};

  @override
  void didChangeDependencies() {
    setState(() {
      args = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
      // item = args["item"] ;
    });

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: BackgroundView(
        child: Column(
          children: [
            CustomAppBar(
              title: args['item'].mijozName,
              rightMenu: false,
            
            ),
            Expanded(
                child: SingleChildScrollView(
              child: Column(
                children: [item()],
              ),
            ))
          ],
        ),
      ),
    );
  }

  Widget item() {
    var item = args["item"];
    bool check = item.qarzimiz == 0;
    var value = check?item.haqqimiz.toString():'-${item.qarzimiz}';
    return Container(
      margin:EdgeInsets.symmetric(vertical: 15,horizontal: 13),
      padding: EdgeInsets.symmetric(vertical: 15,horizontal: 15),
      decoration: BoxDecoration(color: AppColor.black.withOpacity(0.5),
      borderRadius: BorderRadius.circular(16)
      ),
      child: Column(
        children: [
          Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            item.valyuta,
            style: whiteText,
          ),
          Expanded(
            child: Text(
              value,
              textAlign: TextAlign.end,
              overflow: TextOverflow.ellipsis,
               maxLines: 2,
              style: whiteText.copyWith(color: check?AppColor.white:AppColor.red),
            ),
          ),
        ],
      ),
        ],
      ),
    );
  }
}
