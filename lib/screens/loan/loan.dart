import 'package:basco/constants/app_text_style.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/orders.dart';
import 'package:basco/models/payment.dart';
import 'package:basco/models/user.dart';
import 'package:basco/models/user_client.dart';
import 'package:basco/routes.dart';
import 'package:basco/screens/loan/expand_widget.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:collection/collection.dart';

class Loan extends StatefulWidget {
  const Loan({Key? key}) : super(key: key);
  static const String routeName = 'loan';

  @override
  _LoanState createState() => _LoanState();
}

class _LoanState extends State<Loan> {
  late final UserHive _hive = di.get<UserHive>();
  Map<String, dynamic> data = {};
  late dynamic billingHistory = [];
  List<Payment> payments = [];
  List billingHistoryById = [];
  List trades = [];
  List currency = [];


  @override
  void initState() {
    super.initState();
    getHive();
  }

  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    setState(() {
      data = accountData.data;
      billingHistory = data['clients'] != null
          ? UserClient.fromJson(data["clients"]).mijozOstatkalari
          : [];
      payments = data["payment"] != null
          ? (List<Payment>.from(data["payment"].map((x) => Payment.fromJson(x))))
          : [];

     billingHistoryById = groupBy(billingHistory, (x) => (x as MijozOstatkalari).mijozId).values.toList();
       trades = data["trade"] != null
          ? (List<Orders>.from(data["trade"].map((x) => Orders.fromJson(x))))
          : [];
       currency =
          data["user"] != null ? User.fromJson(data['user']).valyutalar : [];
    });
  }


 

  @override
  Widget build(BuildContext context) { 
    // print("billingHistoryById:${billingHistoryById.length}");
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: BackgroundView(
        child: Column(children: [
          CustomAppBar(
            title: "Дебитор-Кредитор:Карздорлик",
            rightMenu: false,
            size: 15,
          ),
          // Padding(
          //   padding: const EdgeInsets.only(top: 15.0, right: 15,left: 15,bottom: 5),
          //   child: Row(
          //     children: [
          //       // Expanded(
          //       //     flex: 4,
          //       //     child: Text(
          //       //       "Мижозлар",
          //       //       textAlign: TextAlign.center,
          //       //       style: ligthTextStyle.copyWith(fontSize: 16),
          //       //     )),
          //       // Expanded(
          //       //     flex: 2,
          //       //     child: Text(
          //       //       "Валюта",
          //       //       textAlign: TextAlign.center,
          //       //       style: ligthTextStyle.copyWith(fontSize: 16),
          //       //     )),
          //       // Expanded(
          //       //     flex: 3,
          //       //     child: Text(
          //       //       "Карздорлик",
          //       //       textAlign: TextAlign.center,
          //       //       style: ligthTextStyle.copyWith(fontSize: 16),
          //       //     ))
          //     ],
          //   ),
          // ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child: ListView.builder(
                  itemBuilder: (BuildContext context, index) {
                    var value = billingHistoryById[index];
                    // bool check = value.qarzimiz == 0;
                    // var loan = check
                    //     ? value.haqqimiz
                    //     : -value.qarzimiz + storageLoan(value.mijozId,value.valyutaId);
                    //     print(storageLoan(value.mijozId,value.valyutaId));
                    return  ExpandedWidget(item: value, payments:payments,trades:trades,currency:currency, billingHistory:billingHistory,edit: (){onPress(value[0]);});
                    
                    // LoanItem(
                    //     name: value.mijozName,
                    //     currency: value.valyuta,
                    //     value: loan.toString(),
                    //     onPress: () => onPress(value),
                    //     index: index);
                  },
                  itemCount: billingHistoryById.length,
                ),
              ),
            ),
          ),
        ]),
      ),
    );
  }

  onPress(item) {
    Navigator.pushNamed(context, Routes.aktScreen,
        arguments: {"data":item});
  }

  storageLoan(mijozId,valyuta){
    print(mijozId);
    print(valyuta);
   double sum = 0;
     print(payments.map((e) => e.mijozId));
   print(payments.map((e) => e.mijozValyutaId));
   for(int i=0;i<payments.length;i++){
     if(payments[i].mijozId == mijozId && payments[i].mijozValyutaId == valyuta ){
       sum +=double.tryParse(payments[i].mijozSummasi)??0;
     }
   }
  //  sum += double.tryParse(payments.where((e) => (e.mijozId == mijozId )&&(e.mijozValyutaId == valyuta)).single.mijozSummasi)??0;
  print(sum);
   return sum;
  }
}
