import 'dart:typed_data';

import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/utils/map_utils.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:ui' as ui;

class SingleBranchScreen extends StatefulWidget {
  const SingleBranchScreen({Key? key}) : super(key: key);
  static const String routeName = 'single-branch';

  @override
  _SingleBranchScreenState createState() => _SingleBranchScreenState();
}

class _SingleBranchScreenState extends State<SingleBranchScreen>
    with WidgetsBindingObserver {
  late BitmapDescriptor pinLocationIcon;
  bool _mapLoading = true;
  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> _markers = {};
  final TextEditingController select = TextEditingController(text: "Toshkent");
  int selectedIndex = 0;
  late double long, lat;

  void setCustomMapPin() async {
    pinLocationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 1.5), Images.vector);
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
    getLocation();
    setCustomMapPin();
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  void getLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    var coordinates = {"lat": position.latitude, "long": position.longitude};
    print(coordinates["lat"]);

    setState(() {
      lat = position.latitude;
      long = position.longitude;
    });
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if (state == AppLifecycleState.resumed) {
      final GoogleMapController controller = await _controller.future;
      onMapCreated(controller);
    } else {
      print(state.toString());
    }
  }

  void onMapCreated(GoogleMapController controller) {
    controller.setMapStyle("[]");
    _controller.complete(controller);
    // setState(() {
    //   _mapLoading = false;
    // });
  }

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)?.settings.arguments as Map;
    double latitude = double.tryParse(args['lat']) ?? lat;
    double longitude = double.tryParse(args['long']) ?? long;
    String name = args["name"];

    LatLng pinPosition = LatLng(latitude, longitude);
    CameraPosition _kGooglePlex = CameraPosition(
      target: pinPosition,
      zoom: 14.4746,
    );

    return Scaffold(
      // resizeToAvoidBottomInset: true,
      appBar: AppBar(
        brightness: Brightness.dark,
        // elevation: 5,
        backgroundColor: AppColor.black.withOpacity(0.7),
        iconTheme: IconThemeData(
          color: AppColor.white, //change your color here
        ),
        title: Text(name, style: ligthTextStyle),
        centerTitle: true,
      ),
      body: Stack(children: [
        GoogleMap(
          myLocationButtonEnabled: false,
          // zoomControlsEnabled: false,
          markers: _markers,
          // mapType: MapType.normal,
          initialCameraPosition: _kGooglePlex,
          onMapCreated: (GoogleMapController controller) {
            // controller.setMapStyle(Utils.mapStyles);
            setState(() {
              _mapLoading = false;
              _markers.add(Marker(
                onTap: () {
                  print("object");
                },
                markerId: const MarkerId('<MARKER_ID>'),
                position: pinPosition,
                // infoWindow: InfoWindow(
                //   title: 'Name of location',
                //   snippet: 'Marker Description',
                // ),

                // icon: pinLocationIcon
              ));
            });
            _controller.complete(controller);
          },
        ),
        Positioned(
            bottom: 40,
            left: 10,
            child: Material(
              color: AppColor.transparent,
              child: Ink(
                decoration: ShapeDecoration(
                  color: Colors.blue,
                  shape: CircleBorder(),
                ),
                child: IconButton(
                  color: Colors.white,
                  padding: EdgeInsets.all(5),
                  icon: Icon(Icons.directions,),
                  onPressed: () {
                    MapUtils.openMap(latitude, longitude);
                  },
                ),
              ),
            )
            // InkWell(
            //   onTap: () {
            //
            //   },
            //   child: Container(
            //     padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
            //     decoration: BoxDecoration(color: Colors.blue),
            //     child: Text("dsd"),
            //   ),
            // ),
            ),
        (_mapLoading)
            ? Container(
                height: double.infinity,
                width: double.infinity,
                color: Colors.grey[100],
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              )
            : Container(),
        // Container(
        //   height: MediaQuery.of(context).size.height * 0.2,
        //   width: double.infinity,
        //   color: AppColor.white,
        //   child: Column(
        //     children: [
        //       Container(
        //         padding: EdgeInsets.symmetric(vertical: 3),
        //         height: MediaQuery.of(context).size.height * 0.07,
        //         child: ListView.builder(
        //           itemBuilder: (context, index) {
        //             return InkWell(
        //               onTap: () {
        //                 setState(() {
        //                   selectedIndex = index;
        //                 });
        //               },
        //               child: Container(
        //                 // height: 60,
        //                 // width: 60,
        //                 margin:
        //                     EdgeInsets.symmetric(horizontal: 4, vertical: 3),
        //                 padding: EdgeInsets.symmetric(horizontal: 8),
        //                 decoration: BoxDecoration(
        //                   color: selectedIndex == index
        //                       ? AppColor.brandColor
        //                       : AppColor.gray,
        //                   borderRadius: BorderRadius.circular(10),
        //                 ),
        //                 child: Center(
        //                     child: Text(
        //                   "Toshkent",
        //                   style: TextStyle(
        //                       color: selectedIndex == index
        //                           ? AppColor.white
        //                           : AppColor.red,
        //                       fontSize: 12,
        //                       fontWeight: FontWeight.w500),
        //                 )),
        //               ),
        //             );
        //           },
        //           itemCount: 20,
        //           scrollDirection: Axis.horizontal,
        //         ),
        //       )
        //     ],
        //   ),
        // )
      ]),
    );
  }

  void _isSelected(String number) {
    setState(() {
      select.text = number;
    });
  }
}
