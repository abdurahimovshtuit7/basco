import 'dart:async';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/dialog/custom_dialogs.dart';
import 'package:basco/dialog/custom_loading_dialog.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/screens/map/bloc/location_bloc.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:map_picker/map_picker.dart';
import 'package:geocoding/geocoding.dart';

import 'package:flutter_svg/flutter_svg.dart';

class SelectLocation extends StatefulWidget {
  const SelectLocation({Key? key}) : super(key: key);
  static const String routeName = 'select-location';

  @override
  _SelectLocationState createState() => _SelectLocationState();
}

class _SelectLocationState extends State<SelectLocation> {
  dynamic _controller = Completer<GoogleMapController>();
  MapPickerController mapPickerController = MapPickerController();
  final LocationBloc _bloc = LocationBloc();

  CameraPosition? cameraPosition;
   late double lat, long;
  var textController = TextEditingController();
  late final UserHive _hive = di.get<UserHive>();
  Map<String, dynamic> data = {};
  @override
  void initState() {
    getHive();
    getLocation();
    super.initState();
  }

  void getLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    var coordinates = {"lat": position.latitude, "long": position.longitude};
    print(coordinates["lat"]);
    cameraPosition =  CameraPosition(
      target: LatLng(position.latitude, position.longitude), //LatLng(41.311158, 69.279737),
      zoom: 14.4746,
    );
    setState(() {
      lat = position.latitude;
      long = position.longitude;
    });
    
  }

  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");
    final accountData = baza.firstWhere((x) => x.name == name);
    setState(() {
      data = accountData.data;
    });
  }

  @override
  Widget build(BuildContext context) {
    final args =
        ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
    // cameraPosition = CameraPosition(
    //   target: LatLng(lat, long), //LatLng(41.311158, 69.279737),
    //   zoom: 14.4746,
    // );
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
       appBar: AppBar(
        brightness: Brightness.dark,
        // elevation: 5,
        backgroundColor: AppColor.black.withOpacity(0.7),
        iconTheme: IconThemeData(
          color: AppColor.white, //change your color here
        ),
        title: Text('${textController.text}', style: ligthTextStyle),
        centerTitle: true,
      ),
        body:cameraPosition==null?Container(
          height: double.infinity,
          width: double.infinity,
          child:Center(child: CircularProgressIndicator())):BlocConsumer<LocationBloc, LocationState>(
          listener: (context, state) {
            CustomLoadingDialog.hide(context);
            if (state is LocationLoadingState) {
              CustomLoadingDialog.show(context);
            } else if (state is LocationSuccessState) {
              DialogUtils.showMessageDialog(context,
                  okBtnFunction: ()  {
                    Navigator.pop(context);
                   

                  }, text: "Жойлашув сақланди.");
                  
            } else if (state is LocationErrorState) {
              DialogUtils.showLangDialog(context,
                  okBtnFunction: () {}, text: state.message);
              // showSnackBar(state.message);
            }
          },
          builder: (context, state) {
            return Stack(
              alignment: Alignment.topCenter,
              children: [
                MapPicker(
                  // pass icon widget
                  iconWidget: SvgPicture.asset(
                    Images.location,
                    height: 40,
                    color: AppColor.red,
                  ),
                  //add map picker controller
                  mapPickerController: mapPickerController,
                  child: GoogleMap(
                    myLocationEnabled: true,
                    zoomControlsEnabled: false,
                    // hide location button
                    myLocationButtonEnabled: false,
                    mapType: MapType.normal,
                    //  camera position
                    initialCameraPosition: cameraPosition!,
                    onMapCreated: (GoogleMapController controller) {
                      _controller.complete(controller);
                    },
                    onCameraMoveStarted: () {
                      mapPickerController.mapMoving!();
                      textController.text = "checking ...";
                    },
                    onCameraMove: (cameraPosition) {
                      this.cameraPosition = cameraPosition;
                    },
                    onCameraIdle: () async {
                      // notify map stopped moving
                      mapPickerController.mapFinishedMoving!();
                      //get address name from camera position
                      List<Placemark> placemarks =
                          await placemarkFromCoordinates(
                        cameraPosition!.target.latitude,
                        cameraPosition!.target.longitude,
                      );

                      // update the ui with the address
                      textController.text =
                          '${placemarks.first.name}, ${placemarks.first.administrativeArea}, ${placemarks.first.country}';
                    },
                  ),
                ),
                Positioned(
                  top: MediaQuery.of(context).viewPadding.top + 20,
                  width: MediaQuery.of(context).size.width - 50,
                  height: 50,
                  child: TextFormField(
                    maxLines: 3,
                    textAlign: TextAlign.center,
                    readOnly: true,
                    decoration: const InputDecoration(
                        contentPadding: EdgeInsets.zero,
                        border: InputBorder.none),
                    controller: textController,
                  ),
                ),
                Positioned(
                  bottom: 24,
                  left: 24,
                  right: 24,
                  child: SizedBox(
                    height: 50,
                    child: TextButton(
                      child: Text("Сақлаш",
                          style: blackColor.copyWith(fontSize: 17)),
                      onPressed: () {
                        print(
                            "Location ${cameraPosition!.target.latitude} ${cameraPosition!.target.longitude}");
                        print("Address: ${textController.text}");
                        _bloc.add(SelectLocal(
                            userId: args["user"],
                            clientId: args["data"],
                            long: cameraPosition!.target.longitude.toString(),
                            lat: cameraPosition!.target.latitude.toString(),
                            date: DateFormat("yyyyMMddhhmmss")
                                .format(DateTime.now())));
                      },
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(AppColor.orange),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
