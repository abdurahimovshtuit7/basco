part of 'location_bloc.dart';

abstract class LocationEvent extends Equatable {
  const LocationEvent();

  @override
  List<Object> get props => [userId,clientId,long,lat,date];
  get userId => null;
  get clientId => null;
  get long => null;
  get lat => null;
  get date => null;
}


class SelectLocal extends LocationEvent {
@override
  final String userId,clientId,long,lat,date;
const SelectLocal({required this.userId,required this.clientId,required this.long,required this.lat,required this.date});
  @override
  List<Object> get props => [userId,clientId,long,lat,date];
}