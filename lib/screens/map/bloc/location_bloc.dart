import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/account.dart';
import 'package:basco/models/location.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'location_event.dart';
part 'location_state.dart';

class LocationBloc extends Bloc<LocationEvent, LocationState> {
  LocationBloc() : super(LocationInitial()) {
    on<LocationEvent>((event, emit) async {
      if (event is SelectLocal) {
        await _location(event, emit);
      }
    });
  }

  Future<void> _location(
      LocationEvent event, Emitter<LocationState> emit) async {
    final UserHive _hive = di.get<UserHive>();

    emit(LocationLoadingState());
    try {
      List<AccountData> baza = await _hive.getAccount();
      //  final baza = await _hive.getAccount();
      String name = await SharedPref().read("activeUser");
      //  final accountData = baza.firstWhere((x) => x.name == name);

      dynamic datas = {
        "user_id": event.userId,
        "mijoz_id": event.clientId,
        "latitude": event.lat,
        "longtitude": event.long,
        "start_location": event.long,
        "data": event.date
      };

      Map<String, dynamic> map = {
        "location": [],
      };

      // event.data.setSend = true;

      // print("O RDER:${event.data}");
      print("DATA:$datas");

      baza.firstWhere((x) => x.name == name).data["location"] != null
          ? baza.firstWhere((x) => x.name == name).data["location"]
          : baza.firstWhere((x) => x.name == name).data.addAll(map);
      //  baza.firstWhere((x) => x.name == name).data["location"]; 69.279737 41.311158

      if (baza.firstWhere((x) => x.name == name).data["location"].length != 0) {
        for (int i = 0;
            i < baza.firstWhere((x) => x.name == name).data["location"].length;
            i++) {
          if (baza.firstWhere((x) => x.name == name).data["location"][i]
                  ["mijoz_id"] ==
              event.clientId) {
            baza.firstWhere((x) => x.name == name).data["location"][i]
                ["longtitude"] = event.long;
            baza.firstWhere((x) => x.name == name).data["location"][i]
                ["latitude"] = event.lat;
          } else {
            baza.firstWhere((x) => x.name == name).data["location"].add(datas);
          }
        }
      } else {
        baza.firstWhere((x) => x.name == name).data["location"].add(datas);
      }

      // baza.firstWhere((x) => x.name == name).data["location"].add(datas);
      print("DATA:$datas");
      print("DDDD${baza.firstWhere((x) => x.name == name).data["location"]}");
      await _hive.saveAccount(baza);

      emit(LocationSuccessState());
    } catch (e) {
      emit(LocationErrorState(message: e.toString()));
      // print("ERROR:$e");
    }
  }
}
