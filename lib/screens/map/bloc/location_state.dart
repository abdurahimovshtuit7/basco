part of 'location_bloc.dart';

abstract class LocationState extends Equatable {
  const LocationState();
  
  @override
  List<Object> get props => [];
}

class LocationInitial extends LocationState {}

class LocationLoadingState extends LocationState {}

class LocationSuccessState extends LocationState {}

class LocationErrorState extends LocationState {
final String message;

   LocationErrorState({required this.message});

  @override
  List<Object> get props => [message];
}