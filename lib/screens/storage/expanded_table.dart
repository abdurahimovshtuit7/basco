import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/orders.dart';
import 'package:basco/models/user_base.dart';
import 'package:basco/screens/storage/expand_character.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;
import "package:collection/collection.dart";

import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';

class ExpandableTable extends StatefulWidget {
  final dynamic storage;
  final String productId;
  const ExpandableTable({Key? key, required this.storage,required this.productId}) : super(key: key);

  @override
  State<ExpandableTable> createState() => _ExpandableTableState();
}

class _ExpandableTableState extends State<ExpandableTable> {
  ExpandableController _controller =
      ExpandableController(initialExpanded: false);
  late final UserHive _hive = di.get<UserHive>();
  Map<String, dynamic> data = {};
  // late dynamic storage = [];
   List<Orders> trades = [];

  @override
  void initState() {
    super.initState();
    getHive();
  }


  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    setState(() {
      data = accountData.data;
       trades = data["trade"] != null
          ? (List<Orders>.from(data["trade"].map((x) => Orders.fromJson(x))))
          : [];
    });
  }


  @override
  Widget build(BuildContext context) {
    print("ddd:${widget.storage}");
    dynamic data = widget.storage;
    _toggleExpandables(int index) {
      setState(() {
        _controller.value = !_controller.value;
        // _getController(index).value = true;
      });
    }

    buildItem(String label) {
      return Padding(
        padding: const EdgeInsets.all(10.0),
        child: Text(
          label,
          style: whiteText,
        ),
      );
    }

    buildList() {
      return Container(
        child: Column(
          children: <Widget>[
            for (var i in [1, 2, 3, 4]) buildItem("Item ${i}"),
          ],
        ),
      );
    }

    return ExpandableNotifier(
        // controller: _controller,
        child: Padding(
      padding: const EdgeInsets.all(0),
      child: ScrollOnExpand(
        child: Card(
          elevation: 1,
          clipBehavior: Clip.antiAlias,
          color: AppColor.transparent,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Column(
            children: <Widget>[
              ExpandablePanel(
                controller: _controller,
                theme: const ExpandableThemeData(
                    headerAlignment: ExpandablePanelHeaderAlignment.center,
                    tapBodyToExpand: true,
                    tapBodyToCollapse: false,
                    tapHeaderToExpand: true,
                    hasIcon: false,
                    useInkWell: true),
                header: Container(
                  decoration: BoxDecoration(
                    color: AppColor.black.withOpacity(0.3),
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            data.ombor,
                            style: whiteText,
                          ),
                        ),
                        Material(
                          color: AppColor.transparent,
                          child: InkWell(
                            onTap: () {
                              print("object");
                              _toggleExpandables(0);
                            },
                            child: ExpandableIcon(
                              theme: const ExpandableThemeData(
                                expandIcon: Icons.chevron_right,
                                collapseIcon: Icons.keyboard_arrow_down_rounded,
                                iconColor: Colors.white,
                                iconSize: 30.0,
                                iconRotationAngle: math.pi / 2,
                                // iconPadding: EdgeInsets.only(right: 5),
                                hasIcon: false,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                collapsed: Container(),
                expanded: table(),
              ),
            ],
          ),
        ),
      ),
    ));
  }

  table() {
    print("Table:${widget.storage.tovarlar.length}");

    List products = widget.storage.tovarlar;

    Map newMap = groupBy(products, (x) => (x as Tovarlar).tovarId) as Map;

    List newlist = newMap.values.toList();

    if(widget.productId.isNotEmpty)
    newlist.removeWhere((x)=>x[0].tovarId != widget.productId);
    print("Store:${widget.storage.omborId}");
    
    return Container(
      color: AppColor.black.withOpacity(0.3),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 0),
        child: Column(
          children: [
            buildTableTop("Товар номи", "Колдик"),
            Container(
              // height: newlist.length * 50.0,
              child: MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child: Column(
                  // defaultVerticalAlignment:
                  //     TableCellVerticalAlignment.middle,
                  // columnWidths: {
                  //   0: FlexColumnWidth(1),
                  //   1: FlexColumnWidth(4),
                  //   2: FlexColumnWidth(4),
                  // },
                  children:
                     newlist.asMap().entries.map((element) {
                    var index = element.key;
                    var value = element.value;
                    double count = 0;


                    print("VAL:${value[0].tovarId}");
                    
 
                    (value as List<dynamic>).map((e) {
                      count += (e as Tovarlar).tovarSoni;
                      return count;
                    }).toList();
                    return ExpandCharacter(index:(index + 1).toString(),name:value[0].tovarName,count:(count-counter(widget.storage.omborId,value[0].tovarId)).toString(),omborId: widget.storage.omborId,tovarId:value[0].tovarId,edit: (){},);
                   
                    // return Row(
                    //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     
                    //     children: [
                    //       Expanded(flex:1,child: buildItem((index + 1).toString())),
                    //       Expanded(flex:4,child: buildItem(value[0].tovarName)),
                    //       Expanded(flex:4,child: buildItem((count-counter(value[0].omborId,value[0].tovarId)).toString(),align: TextAlign.end)),
                    //       Expanded(flex:2,child: buildItem("rrr")),

                    //     ]);
                    
                  }).toList(),
                  
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildItemLabel(text) {
    return Padding(
      padding: const EdgeInsets.only(left: 8, top: 15, bottom: 15),
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: 15,
            color: AppColor.black,
            fontFamily: AppFont.MontserratBold,
            fontWeight: FontWeight.w600),
        // textAlign: TextAlign.center,
      ),
    );
  }

  Widget buildItem(text, {color = AppColor.white, align = TextAlign.center}) {
    return Padding(
      padding: const EdgeInsets.only(left: 8, top: 15, bottom: 15),
      child: SelectableText(
        text,
        textAlign:align,
        style:
            TextStyle(color: color, fontSize: 13, fontWeight: FontWeight.w600),
      ),
    );
  }

  Widget buildTableTop(title1, title2) {
    return Table(
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        columnWidths: {
          0: FlexColumnWidth(1),
          1: FlexColumnWidth(4),
          2: FlexColumnWidth(4),
        },
        children: [
          TableRow(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  AppColor.brandColor,
                  AppColor.brandColor2,
                  AppColor.brandColor
                ],
              )),
              children: [
                buildItemLabel("№"),
                buildItemLabel(title1),
                buildItemLabel(title2),
              ]),
        ]);
  }
  // counter(omborId, tovarId) {
  //   dynamic newlist = groupBy(trades, (x) => (x as Orders).omborId)??[];
  //   dynamic omborList = newlist[omborId]??[];
  //   // dynamic tovarlar =  groupBy(newlist[omborId],(e)=>(e as Orders).);
  //   print(omborList);
  //   dynamic productOf = [];
  //   omborList.map((e) => productOf.addAll(e.ruyxat)).toList();
  //   print(productOf);
  //   int c = 0;
  //   for (int i = 0; i < productOf.length; i++) {
  //     if (productOf[i].tovarId == tovarId) {
  //       c += int.tryParse(productOf[i].soni) ?? 0;
  //     }
  //   }
  //   return c;
  // }

    counter(omborId, tovarId) {
    dynamic newlist = groupBy(trades, (x) => (x as Orders).omborId)??[];
    dynamic omborList = newlist[omborId]??[];
    // dynamic tovarlar =  groupBy(newlist[omborId],(e)=>(e as Orders).);
    print(omborList);
    // dynamic productOf = [];
    // omborList.map((e) => productOf.addAll(e.ruyxat)).toList();
    // print(productOf);
    // int c = 0;
    // for (int i = 0; i < productOf.length; i++) {
    //   if (productOf[i].tovarId == tovarId) {
    //     c += int.tryParse(productOf[i].soni) ?? 0;
    //   }
    // }

    dynamic productOf = [];
    dynamic negativeProduct = [];
    // omborList.map((e) => productOf.addAll(e.ruyxat)).toList();

    for(int i =0;i<omborList.length;i++){
      if(omborList[i].vozvrat){
        negativeProduct.addAll(omborList[i].ruyxat);
      } else {
        productOf.addAll(omborList[i].ruyxat);
      }
    }
 
    print(productOf);
    double c = 0;
    for (int i = 0; i < productOf.length; i++) {
      if (productOf[i].tovarId == tovarId) {
        c += double.tryParse(productOf[i].soni) ?? 0;
      }
    }
    for(int i=0;i<negativeProduct.length;i++){
        if (negativeProduct[i].tovarId == tovarId) {
             c -= double.tryParse(negativeProduct[i].soni) ?? 0;
          }
    }

    return c;
  }
}
