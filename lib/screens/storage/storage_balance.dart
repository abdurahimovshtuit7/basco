import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/orders.dart';
import 'package:basco/models/user_base.dart';
import 'package:basco/routes.dart';
import 'package:basco/screens/storage/expanded_table.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:collection/collection.dart';

class StorageBalance extends StatefulWidget {
  const StorageBalance({Key? key}) : super(key: key);
  static const String routeName = 'storage-balance';

  @override
  _StorageBalanceState createState() => _StorageBalanceState();
}

class _StorageBalanceState extends State<StorageBalance> {
  String productName = "Товар номи";
  String selectedStoreName = "Омбор";
  String selectedStoreId = '';
  String selectedProductId = '';

  dynamic selectedStore = [];
  dynamic selectedProduct = [];
  late final UserHive _hive = di.get<UserHive>();
  Map<String, dynamic> data = {};
  late dynamic storage = [];
   List<Orders> trades = [];

  @override
  void initState() {
    super.initState();
    getHive();
  }


  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    setState(() {
      data = accountData.data;
      storage = data['user_base'] != null
          ? UserBase.fromJson(data["user_base"]).response
          : [];
       trades = data["trade"] != null
          ? (List<Orders>.from(data["trade"].map((x) => Orders.fromJson(x))))
          : [];
    });
  }

  @override
  Widget build(BuildContext context) {
    print("data:${storage}");
    return Scaffold(
      body: BackgroundView(
        child: Column(
          children: [
            CustomAppBar(title: "Товарлар колдиги", rightMenu: false),
            Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 15, horizontal: 13),
                  child: Column(
                    children: [
                      wrapper(productName, Routes.productScreen, storage,
                          () => onPress2(Routes.productScreen, storage)),
                      wrapper(selectedStoreName, Routes.storage, storage,
                          () => onPress(Routes.storage, storage)),
                      title(),
                      if (selectedStoreId == '' && selectedProductId == '')
                        ...storage.asMap().entries.map((entry) {
                          int index = entry.key;
                          dynamic value = entry.value;
                          return ExpandableTable(
                            storage: value,
                            productId: '',
                          );
                        })
                      else if (selectedStoreId.isNotEmpty &&
                          selectedProductId.isNotEmpty)
                        ...selectedStore.asMap().entries.map((entry) {
                          int index = entry.key;
                          dynamic value = entry.value;
                          return ExpandableTable(
                            storage: selectedStore[0],
                            productId: selectedProductId,
                          );
                        })
                      else if (selectedProductId.isNotEmpty)
                        ...storage.asMap().entries.map((entry) {
                          int index = entry.key;
                          dynamic value = entry.value;
                          return ExpandableTable(
                            storage: value,
                            productId: selectedProductId,
                          );
                        })
                      else if (selectedStoreId.isNotEmpty)
                        ...selectedStore.asMap().entries.map((entry) {
                          int index = entry.key;
                          dynamic value = entry.value;
                          return ExpandableTable(
                            storage: selectedStore[0],
                            productId: '',
                          );
                        }),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget wrapper(text1, route, storage, Function onpress) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5),
      child: Material(
        color: AppColor.transparent,
        child: InkWell(
          onTap: () => onpress(),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
            decoration: BoxDecoration(
              color: AppColor.black.withOpacity(0.5),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Text(text1, style: whiteText),
                ),
                SvgPicture.asset(Images.rigthArrow),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget title() {
    return InkWell(
      onTap: (){
        setState(() {
          selectedStoreId = '';
          selectedProductId = '';
          productName = "Товар номи";
          selectedStoreName = "Омбор";
        });
      },
      child: Container(
        width: double.infinity,
        margin: EdgeInsets.symmetric(vertical: 15),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
              AppColor.brandColor,
              AppColor.brandColor2,
              AppColor.brandColor
            ])),
        child: Text(
          "Товарлар колдиги",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 18,
              fontFamily: AppFont.MontserratMedium,
              fontWeight: FontWeight.w600),
        ),
      ),
    );
  }

  Future<void> onPress(routeName, storage) async {
    final dynamic data = await Navigator.pushNamed(context, routeName,
        arguments: {"storage": storage});
    if (data != null) {
      setState(() {
        selectedStoreId = data.omborId;
        selectedStoreName = data.ombor;
        selectedStore = [data];
      });
      print(selectedStore);
    }
  }

  Future<void> onPress2(routeName, storage) async {
    final dynamic data = await Navigator.pushNamed(context, routeName,
        arguments: {"storage": storage});
    if (data != null) {
      setState(() {
        selectedProductId = data.tovarId;
        productName = data.tovarName;
        selectedProduct = [data];
      });
      print(selectedProduct);
    }
  }
  // count(omborId, tovarId) {
  //   dynamic newlist = groupBy(trades, (x) => (x as Orders).omborId)??[];
  //   dynamic omborList = newlist[omborId]??[];
  //   // dynamic tovarlar =  groupBy(newlist[omborId],(e)=>(e as Orders).);
  //   print(omborList);
  //   // dynamic productOf = [];
  //   // omborList.map((e) => productOf.addAll(e.ruyxat)).toList();
  //   // print(productOf);
  //   // int c = 0;
  //   // for (int i = 0; i < productOf.length; i++) {
  //   //   if (productOf[i].tovarId == tovarId) {
  //   //     c += int.tryParse(productOf[i].soni) ?? 0;
  //   //   }
  //   // }

  //   dynamic productOf = [];
  //   dynamic negativeProduct = [];
  //   // omborList.map((e) => productOf.addAll(e.ruyxat)).toList();

  //   for(int i =0;i<omborList.length;i++){
  //     if(omborList[i].vozvrat){
  //       negativeProduct.addAll(omborList[i].ruyxat);
  //     } else {
  //       productOf.addAll(omborList[i].ruyxat);
  //     }
  //   }
 
  //   print(productOf);
  //   int c = 0;
  //   for (int i = 0; i < productOf.length; i++) {
  //     if (productOf[i].tovarId == tovarId) {
  //       c += int.tryParse(productOf[i].soni) ?? 0;
  //     }
  //   }
  //   for(int i=0;i<negativeProduct.length;i++){
  //       if (negativeProduct[i].tovarId == tovarId) {
  //            c -= int.tryParse(negativeProduct[i].soni) ?? 0;
  //         }
  //   }

  //   return c;
  // }
}
