import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/orders.dart';
import 'package:basco/models/user_base.dart';
import 'package:basco/utils/services.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:collection/collection.dart';
import 'dart:math' as math;

import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';

class ExpandCharacter extends StatefulWidget {
  final String index;
  final String name;
  final String count;
  final dynamic omborId;
  final dynamic tovarId;
  final Function edit;

  @override
  State<ExpandCharacter> createState() => _ExpandCharacterState();

  const ExpandCharacter(
      {Key? key,
      required this.index,
      required this.name,
      required this.count,
      required this.omborId,
      required this.tovarId,
      required this.edit})
      : super(key: key);
}

class _ExpandCharacterState extends State<ExpandCharacter> {
  ExpandableController _controller =
      ExpandableController(initialExpanded: false);
  late final UserHive _hive = di.get<UserHive>();
  late dynamic dataAccount;
  late List products = [];
  late dynamic newlist = [];
  late List trades = [];

  @override
  void initState() {
    super.initState();
    getHive();
  }

  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    setState(() {

      dataAccount = accountData.data;
     

      trades = dataAccount["trade"] != null
          ? (List<Orders>.from(
              dataAccount["trade"].map((x) => Orders.fromJson(x))))
          : [];
     
    });
  }

  @override
  Widget build(BuildContext context) {
    _toggleExpandables(int index) {
      setState(() {
        _controller.value = !_controller.value;

        // _getController(index).value = true;
      });
    }
     products = dataAccount["user_base"] != null
          ? UserBase.fromJson(dataAccount['user_base'])
              .response
              .firstWhere((element) => element.omborId == widget.omborId)
              .tovarlar
          : [];
   newlist = groupBy(products, (x) => (x as Tovarlar).tovarId);
    print("LIST: ${products}");
    print("object:${widget.tovarId}");
    // buildItem(String label) {
    //   return Padding(
    //     padding: const EdgeInsets.all(10.0),
    //     child: Text(
    //       label,
    //       style: whiteText,
    //     ),
    //   );
    // }

    buildList() {
      return Container(
        // padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: Column(
          children: <Widget>[
            ...newlist[widget.tovarId].map((x) {
              var value = x;
              return Container(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 0),
                margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color:(x == newlist[widget.tovarId].last) ? AppColor.transparent:Colors.white,
                      width: 0.5,
                    ),
                  ),
                  // borderRadius: BorderRadius.circular(10),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      value.xarakName.isEmpty ? "Стандарт" : value.xarakName,
                      style: whiteText.copyWith(fontSize: 15),
                    ),
                    Text(
                      (value.tovarSoni -
                              count(
                                  widget.omborId, value.tovarId, value.xarakId))
                          .toString(),
                      style: whiteText.copyWith(fontSize: 15),
                    ),
                  ],
                ),
              );
            }).toList()
            // TextRow(text1: "Характеристика",text2: widget.item["character"].xarakName.isEmpty?"Стандарт":widget.item["character"].xarakName,),
            //  Padding(
            //   padding: const EdgeInsets.symmetric(vertical: 8),
            //   child: Divider(color: AppColor.white,),
            // ),
            // TextRow(text1: "Cумма",text2: Services.currencyFormatter(widget.item["totalAmount"].toString()),),
            // Padding(
            //   padding: const EdgeInsets.symmetric(vertical: 8),
            //   child: Divider(color: AppColor.white,),
            // ),
            // TextRow(text1: "Сони",text2: widget.item["count"].toString(),),
          ],
        ),
      );
    }

    return ExpandableNotifier(
        // controller: _controller,
        child: Padding(
      padding: const EdgeInsets.all(0),
      child: ScrollOnExpand(
        child: Card(
          clipBehavior: Clip.antiAlias,
          color: AppColor.transparent,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(2.0),
          ),
          child: Column(
            children: <Widget>[
              ExpandablePanel(
                controller: _controller,
                theme: const ExpandableThemeData(
                    headerAlignment: ExpandablePanelHeaderAlignment.center,
                    tapBodyToExpand: false,
                    tapBodyToCollapse: false,
                    tapHeaderToExpand: false,
                    hasIcon: false,
                    useInkWell: true),
                header: Container(
                  decoration: BoxDecoration(
                      color: AppColor.black.withOpacity(0.5),
                      borderRadius: BorderRadius.circular(2)),
                  child: Row(
                    children: [
                      Expanded(flex: 1, child: buildItem(widget.index)),
                      Expanded(flex: 4, child: buildItem(widget.name)),
                      Expanded(
                          flex: 4,
                          child: buildItem(widget.count, align: TextAlign.end)),
                      Material(
                        color: AppColor.transparent,
                        child: InkWell(
                          onTap: () {
                            print("object");
                            _toggleExpandables(0);
                          },
                          child: ExpandableIcon(
                            theme: const ExpandableThemeData(
                              expandIcon: Icons.chevron_right,
                              collapseIcon: Icons.keyboard_arrow_down_rounded,
                              iconColor: Colors.white,
                              iconSize: 30.0,
                              iconRotationAngle: math.pi / 2,
                              // iconPadding: EdgeInsets.only(right: 5),
                              hasIcon: false,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                collapsed: Container(),
                expanded: buildList(),
              ),
            ],
          ),
        ),
      ),
    ));
  }

  Widget buildItem(text, {color = AppColor.white, align = TextAlign.center}) {
    return Padding(
      padding: const EdgeInsets.only(left: 8, top: 15, bottom: 15),
      child: SelectableText(
        text,
        textAlign: align,
        style:
            TextStyle(color: color, fontSize: 13, fontWeight: FontWeight.w600),
      ),
    );
  }

  count(omborId, tovarId, xarakId) {
    dynamic newlist = groupBy(trades, (x) => (x as Orders).omborId) ?? [];
    dynamic omborList = newlist[omborId] ?? [];
    // dynamic tovarlar =  groupBy(newlist[omborId],(e)=>(e as Orders).);
    print(omborList);
    dynamic productOf = [];
    dynamic negativeProduct = [];
    // omborList.map((e) => productOf.addAll(e.ruyxat)).toList();
    for (int i = 0; i < omborList.length; i++) {
      if (!omborList[i].vozvrat) {
        productOf.addAll(omborList[i].ruyxat);
      }
    }
    for (int i = 0; i < omborList.length; i++) {
      if (omborList[i].vozvrat) {
        negativeProduct.addAll(omborList[i].ruyxat);
      }
    }

    print(productOf);
    double c = 0;
    for (int i = 0; i < productOf.length; i++) {
      if ((productOf[i].tovarId == tovarId) &&
          (productOf[i].xarakId == xarakId)) {
        c += double.tryParse(productOf[i].soni) ?? 0;
      }
    }
    for (int i = 0; i < negativeProduct.length; i++) {
      if ((negativeProduct[i].tovarId == tovarId) &&
          (negativeProduct[i].xarakId == xarakId)) {
        c -= double.tryParse(negativeProduct[i].soni) ?? 0;
      }
    }

    return c;
  }
}
