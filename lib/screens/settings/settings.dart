import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/dialog/custom_dialogs.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/user.dart';
import 'package:basco/models/user_client.dart';
import 'package:basco/utils/header_options.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);
  static const String routeName = 'settings';

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool tabChange = true;
  TextEditingController addressServer = TextEditingController();
  TextEditingController port = TextEditingController();
  TextEditingController folder = TextEditingController();
  TextEditingController api = TextEditingController();
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();


  String deviceId = '';
  late final UserHive _hive = di.get<UserHive>();
  Map<String, dynamic> data = {};

  @override
  void initState() {
    super.initState();
    getHive();
  }

  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    bool isFind =accountData.data["settings"]!=null;
    
    setState(() {
      data = accountData.data;
      addressServer.value = TextEditingValue(text: isFind?accountData.data["settings"]["addressServer"]:"");
      port.text = isFind?accountData.data["settings"]["port"]:"";
      folder.text = isFind?accountData.data["settings"]["folder"]:"";
      api.text = isFind?accountData.data["settings"]["api"]:"";
      username.text = isFind?accountData.data["settings"]["username"]:"";
      password.text = isFind?accountData.data["settings"]["password"]:"";

    });
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    print("done");
    var device = await SharedPref().read('deviceID') ?? '';
    print(device);
      setState(() {
      deviceId = device;
    });
    var path =   HeaderOptions.setUrl(); 
      print("SET:${path.then((dynamic value) async {
           return await value;
    })}");
  
  }

  @override
  Widget build(BuildContext context) {
    final date = data["clients"] != null
        ? (UserClient.fromJson(data['clients']).sinxronVaqti)
        : '';
    
  
    return Scaffold(
      body: BackgroundView(
        child: Column(
          children: [
            CustomAppBar(title: "Сервер созламалари", rightMenu: false),
            Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 13),
                  child: Column(
                    children: [
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                        decoration: BoxDecoration(
                            color: AppColor.black.withOpacity(0.5),
                            borderRadius: BorderRadius.circular(16)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                              flex: 1,
                              child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      tabChange = true;
                                    });
                                  },
                                  child: Container(
                                      padding: EdgeInsets.only(
                                        bottom:
                                            5, // Space between underline and text
                                      ),
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom: BorderSide(
                                        color: tabChange
                                            ? AppColor.brandColor2
                                            : AppColor.transparent,
                                        width: 1.0, // Underline thickness
                                      ))),
                                      child: Text(
                                        "Созлаш",
                                        style: TextStyle(
                                            color: tabChange
                                                ? AppColor.white
                                                : AppColor.brandColor2
                                                    .withOpacity(0.8)),
                                      ))),
                            ),
                            SizedBox(width: 20),
                            Expanded(
                              flex: 2,
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    tabChange = false;
                                  });
                                },
                                child: Container(
                                  padding: EdgeInsets.only(
                                    bottom: 5, // Space between underline and text
                                  ),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                    color: tabChange
                                        ? AppColor.transparent
                                        : AppColor.brandColor2,
                                    width: 1.0, // Underline thickness
                                  ))),
                                  child: Text(
                                    "Девайс маьлумотлари",
                                    style: TextStyle(
                                        color: tabChange
                                            ? AppColor.brandColor2
                                                .withOpacity(0.8)
                                            : AppColor.white),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 10),
                      tabChange
                          ? Column(
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 14, horizontal: 19),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16),
                                      color: AppColor.black.withOpacity(0.5)),
                                  child: Form(
                                    key:_formKey,
                                    child: Column(
                                      children: [
                                        TextInputField(
                                          text1: "Сервер манзили",
                                          value: addressServer,
                                          shadow: false,
                                          keyboardType: TextInputType.text,
                                          validator: (value) {
                                            if (value == null || value.isEmpty) {
                                              return 'Сервер манзилини киритинг';
                                            }
                                            return null;
                                          },
                                        ),
                                        divider(),
                                        TextInputField(
                                          text1: "Порт раками",
                                          value: port,
                                          shadow: false,
                                          keyboardType: TextInputType.text,
                                          validator: (value) {
                                            if (value == null || value.isEmpty) {
                                              return 'Порт ракамини киритинг';
                                            }
                                            return null;
                                          },
                                        ),
                                        divider(),
                                        TextInputField(
                                          text1: "Папка",
                                          value: folder,
                                          shadow: false,
                                          keyboardType: TextInputType.text,
                                          validator: (value) {
                                            if (value == null || value.isEmpty) {
                                              return 'Папкани киритинг';
                                            }
                                            return null;
                                          },
                                        ),
                                        divider(),
                                        TextInputField(
                                          text1: "Апи версияси",
                                          value: api,
                                          shadow: false,
                                          keyboardType: TextInputType.text,
                                          validator: (value) {
                                            if (value == null || value.isEmpty) {
                                              return 'Апи версиясини киритинг';
                                            }
                                            return null;
                                          },
                                        ),
                                        divider(),
                                        TextInputField(
                                          text1: "Фойдаланувчи номи",
                                          value: username,
                                          shadow: false,
                                          keyboardType: TextInputType.text,
                                          validator: (value) {
                                            if (value == null || value.isEmpty) {
                                              return 'Фойдаланувчи номини киритинг';
                                            }
                                            return null;
                                          },
                                        ),
                                        divider(),
                                        TextInputField(
                                          text1: "Парол",
                                          value: password,
                                          shadow: false,
                                          keyboardType: TextInputType.text,
                                          validator: (value) {
                                            if (value == null || value.isEmpty) {
                                              return 'Паролни киритинг';
                                            }
                                            return null;
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Button(title: "Сақлаш", onPress: () => onPress())
                              ],
                            )
                          : Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 14, horizontal: 19),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(16),
                                  color: AppColor.black.withOpacity(0.5)),
                              child: Column(
                                children: [
                                  TextRow(
                                    text1: "Девайс ИД",
                                    text2: deviceId,
                                    shadow: false,
                                  ),
                                  divider(padding: 5),
                                  TextRow(
                                    text1: "Танланган омбор",
                                    text2: "c",
                                    shadow: false,
                                  ),
                                  divider(padding: 5),
                                  TextRow(
                                    text1: '''Сервер билан охирги
   синхрон вакти''',
                                    text2: date.toString(),
                                    shadow: false,
                                  ),
                                ],
                              ),
                            )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  divider({double padding = 0}) {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: padding),
        child: Divider(color: AppColor.white));
  }

  onPress() async {
    String name = await SharedPref().read("activeUser");
    final baza = await _hive.getAccount();
    Map<String, dynamic> settings = {
      "addressServer": addressServer.text,
      "port": port.text,
      "folder": folder.text,
      "api": api.text,
      "username":username.text,
      "password":password.text
    };

    Map<String, dynamic> map = {
      "settings": settings,
    };
    if (_formKey.currentState!.validate()) {
    baza.firstWhere((x) => x.name == name).data.addAll(map);

    await _hive.saveAccount(baza).then((value) => 
    print("Save"));
     DialogUtils.showLangDialog(context, okBtnFunction: (){},text: "Сервер маълумотлари муваффиқиятли сақланди");
    }

    
  }
}
