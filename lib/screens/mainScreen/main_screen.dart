import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/screens/auth/accounts/accounts.dart';
import 'package:basco/utils/gradient_mask.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_it/get_it.dart';
import '../../models/account.dart';
import '../../routes.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);
  static const String routeName = 'main';
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final GlobalKey<ScaffoldState> _key = GlobalKey(); // Create a key
  final di = GetIt.I;
  late final UserHive _hive = di.get<UserHive>();
  late List<AccountData> data;
  @override
  void initState() {
    getHive();
    super.initState();
  }

  Future<void> getHive() async {
    var device = await SharedPref().read('deviceID') ?? '';
    print("DEVICE_ID:$device");
  }

  @override
  Widget build(BuildContext context) {
    // print(data[0].name);
    // print(data[1].name);

    return Scaffold(
      key: _key, // Assign the key to Scaffold.
      drawer: Drawer(
        backgroundColor: AppColor.black.withOpacity(0.6),
        child: Container(
          height: double.infinity,
          width: 200,
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 80),
                drawerItem(Images.account, "Мижозлар",  () => onPress(Routes.clients)),
                drawerItem(Images.product, "Товарлар",  () => onPress(Routes.productsScreen)),
                drawerItem(Images.sale, "Савдо",  () => onPress(Routes.trade)),
                drawerItem(Images.cashflow, "Пул кирими",  () => onPress(Routes.payments)),
                drawerItem(Images.storage, "Омбор колдиги",  () => onPress(Routes.storageBalance)),
                drawerItem(Images.backlog, "Карздорлик",  () => onPress(Routes.loan)),
                drawerItem(Images.syncronization, "Синхронлаш",  () => onPress(Routes.synchrony)),
                drawerItem(Images.settings, "Созлаш",  () => onPress(Routes.settings)),
                
                ],
            ),
          ),
        ),
      ),
      body: BackgroundView(
        child: Column(
          children: [
            CustomHeader(
                menuPressed: () => _key.currentState!.openDrawer(),
                rightButton: () {
                  Navigator.pushNamedAndRemoveUntil(
                      context, Routes.accounts, (route) => false);
                },
                icon: true),
            Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 20),
                  child: Column(
                    children: [
                      row(
                          text1: "Мижозлар",
                          text2: "Товарлар",
                          image1: Images.account,
                          image2: Images.product,
                          onPress1: () => onPress(Routes.clients),
                          onPress2: () => onPress(Routes.productsScreen)),
                      row(
                          text1: "Савдо",
                          text2: "Пул кирими",
                          image1: Images.sale,
                          image2: Images.cashflow,
                          onPress1: () => onPress(Routes.trade),
                          onPress2: ()  => onPress(Routes.payments)),
                      row(
                          text1: "Омбор колдиги",
                          text2: "Карздорлик",
                          image1: Images.storage,
                          image2: Images.backlog,
                          onPress1: () => onPress(Routes.storageBalance),
                          onPress2: () => onPress(Routes.loan)),
                      row(
                          text1: "Синхронлаш",
                          text2: "Созлаш",
                          image1: Images.syncronization,
                          image2: Images.settings,
                          onPress1: () => onPress(Routes.synchrony),
                          onPress2: () => onPress(Routes.settings)),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget row(
      {required String text1,
      required String text2,
      required String image1,
      required String image2,
      required Function onPress1,
      required Function onPress2}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        rowItem(text1, image1, onPress1),
        rowItem(text2, image2, onPress2),
      ],
    );
  }

  Widget rowItem(String text, String image, Function onPress) {
    return Expanded(
        child: IconWithButton(
      image: image,
      text: text,
      onPress: onPress,
    ));
  }

  void onPress(routeName) {
    Navigator.pushNamed(context, routeName);
  }

  drawerItem(image, text, onTap) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 15,horizontal: 15),

        child: Row(
          children: [
            Container(
              width: 40,
              child: LinearGradientMask(
                  child: SvgPicture.asset(
                image,
                height: 28,
                width: 28,
              )),
            ),
            SizedBox(width: 20,),
            Expanded(child: Text(text,style: ligthTextStyle.copyWith(fontSize: 18,color: AppColor.brandColor2),))
          ],
        ),
      ),
    );
  }
}
