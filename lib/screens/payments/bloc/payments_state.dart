part of 'payments_bloc.dart';

abstract class PaymentsState extends Equatable {
  const PaymentsState();
  
  @override
  List<Object> get props => [];
}

class PaymentsInitial extends PaymentsState {}

class PaymentsLoadingState extends PaymentsState {}

class PaymentsSuccessState extends PaymentsState {
  final int id;
  const PaymentsSuccessState({required this.id});
   @override
  List<Object> get props => [id];
}

class PaymentsErrorState extends PaymentsState {
final String message;

const PaymentsErrorState({required this.message});

  @override
  List<Object> get props => [message];
}