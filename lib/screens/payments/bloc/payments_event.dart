part of 'payments_bloc.dart';

abstract class PaymentsEvent extends Equatable {
  const PaymentsEvent();

  @override
  List<Object> get props => [data];
    get data => null;

}
class MultiplePayment extends PaymentsEvent {
final List data;
const MultiplePayment({required this.data});
  @override
  List<Object> get props => [data];
}



class DeletePayment extends PaymentsEvent {
final List data;
const DeletePayment({required this.data});
  @override
  List<Object> get props => [data];
}