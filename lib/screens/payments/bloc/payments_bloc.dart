import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/account.dart';
import 'package:basco/models/payment.dart';
import 'package:basco/repositories/services_repositories.dart';
import 'package:basco/screens/clientsScreens/cashFlow/bloc/payment_bloc.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'payments_event.dart';
part 'payments_state.dart';

class PaymentsBloc extends Bloc<PaymentsEvent, PaymentsState> {
 final ServicesRepository _api = di.get();
  final UserHive _hive = di.get<UserHive>();
  
  PaymentsBloc() : super(PaymentsInitial()) {
    on<PaymentsEvent>((event, emit) async {
      if (event is MultiplePayment) {
        await _payments(event, emit);
      } 
      if (event is DeletePayment) {
        await _delete(event, emit);
      } 

    });
  }


  Future<void> _payments(PaymentsEvent event, Emitter<PaymentsState> emit) async {
    emit(PaymentsLoadingState());
    try {
       String name = await SharedPref().read("activeUser");
       List<AccountData> baza = await _hive.getAccount();

        // Map<String, dynamic> map = {
        //   "payment": [],
        // };
        await _api.payments(event.data);

        event.data.map((e){
          e.setSend = true;
        });
         List<Payment> payments = baza.firstWhere((x) => x.name == name).data["payment"] != null
          ? (List<Payment>.from(baza.firstWhere((x) => x.name == name).data["payment"].map((x) => Payment.fromJson(x))))
          : [];

        for(int i=0;i<event.data.length;i++){
         payments.firstWhere((s)=> s.id == event.data[i].id).setSend = true; 
         
        }
        
        // print(event.data.map((e)=>e.id).toList());
        // print(payments.map((e)=>e.id).toList());
        // print("changed:${trades}");
        
        baza.firstWhere((x) => x.name == name).data["payment"]=payments;
        
        await _hive.saveAccount(baza);
    
      emit(PaymentsSuccessState(id: 0));
    } catch (e) {
      emit(PaymentsErrorState(message: e.toString()));
      // print("ERROR:$e");
    }
  }
  Future<void> _delete(PaymentsEvent event, Emitter<PaymentsState> emit) async {
    emit(PaymentsLoadingState());
    try {
       String name = await SharedPref().read("activeUser");
       List<AccountData> baza = await _hive.getAccount();
      
         List<Payment> payments = baza.firstWhere((x) => x.name == name).data["payment"] != null
          ? (List<Payment>.from(baza.firstWhere((x) => x.name == name).data["payment"].map((x) => Payment.fromJson(x))))
          : [];

        for(int i=0;i<event.data.length;i++){
         payments.removeWhere((x)=>x.id==event.data[i].id); 
         
        }
        
        // print("changed:${trades}");
        
        baza.firstWhere((x) => x.name == name).data["payment"]=payments;
        
        await _hive.saveAccount(baza);
    
      emit(PaymentsSuccessState(id: 1));
    } catch (e) {
      emit(PaymentsErrorState(message: e.toString()));
      // print("ERROR:$e");
    }
  }
}
