import 'dart:convert';

import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/dialog/custom_dialogs.dart';
import 'package:basco/dialog/custom_loading_dialog.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/orders.dart';
import 'package:basco/models/payment.dart';
import 'package:basco/models/user.dart';
import 'package:basco/models/user_client.dart';
import 'package:basco/routes.dart';
import 'package:basco/screens/payments/bloc/payments_bloc.dart';
import 'package:basco/utils/services.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';

class Payments extends StatefulWidget {
  const Payments({Key? key}) : super(key: key);
  static const String routeName = 'payments';

  @override
  _PaymentsState createState() => _PaymentsState();
}

class _PaymentsState extends State<Payments> {
  late final UserHive _hive = di.get<UserHive>();
  final _bloc = PaymentsBloc();
  Map<String, dynamic> data = {};
  List selectedList = [];
  List clients = [];
  List currency = [];

  List<Payment> payments = [];

  @override
  void initState() {
    super.initState();
    getHive();
  }

  Future<void> getHive() async {
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    print("DATA:${accountData.data["payment"].length}");
    setState(() {
      data = accountData.data;

      payments = data["payment"] != null
          ? (List<Payment>.from(
              data["payment"].map((x) => Payment.fromJson(x))))
          : [];
      clients = data["clients"] != null
          ? (UserClient.fromJson(data["clients"]).mijozlar)
          : [];
      currency =
          data["user"] != null ? (User.fromJson(data["user"]).valyutalar) : [];
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        body: BackgroundView(
          child: Column(
            children: [
              CustomAppBar(title: "Мижоздан пул кирими", rightMenu: false),
              Expanded(
                  child: SingleChildScrollView(
                child: BlocConsumer<PaymentsBloc, PaymentsState>(
                  listener: (context, state) {
                    CustomLoadingDialog.hide(context);
                    if (state is PaymentsLoadingState) {
                      CustomLoadingDialog.show(context);
                    } else if (state is PaymentsSuccessState) {
                      if (state.id == 0) {
                        for (int i = 0; i < selectedList.length; i++) {
                          payments[selectedList[i]].setSend = true;
                        }
                        setState(() {});
                          DialogUtils.showLangDialog(context,
                          okBtnFunction: () {},
                          text: "Ҳужжатлар муваффиқиятли юборилди.");
                      }
                      else if(state.id == 1){
                        for (int i = 0; i < selectedList.length; i++) {
                          payments.removeAt(selectedList[i]);
                        }
                        selectedList = [];
                        setState(() {});
                          DialogUtils.showLangDialog(context,
                          okBtnFunction: () {},
                          text: "Ҳужжатлар муваффиқиятли ўчирилди.");
                      }

                     
                    } else if (state is PaymentsErrorState) {
                      DialogUtils.showLangDialog(context,
                          okBtnFunction: () {}, text: state.message);
                      // showSnackBar(state.message);
                    }
                  },
                  builder: (context, state) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, Routes.cashFlow,
                                    arguments: {"client": false})
                                .then((value) async {
                              await getHive();
                            });
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: AppColor.black.withOpacity(0.5)),
                            margin: EdgeInsets.symmetric(
                                vertical: 6, horizontal: 12),
                            padding: EdgeInsets.symmetric(
                                vertical: 10, horizontal: 10),
                            child: Text(
                              "+ Қўшиш",
                              style: whiteText.copyWith(fontSize: 16),
                            ),
                          ),
                        ),
                        ...payments.map((e) {
                          print("total:${e.send}");
                          return item(e);
                        }).toList(),
                        SizedBox(height: 70)
                      ],
                    );
                  },
                ),
              ))
            ],
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: selectedList.isNotEmpty
            ? Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 12.0, vertical: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: InkWell(
                        onTap: () => sale(),
                        child: Container(
                          // width: double.infinity/2,
                          padding: EdgeInsets.symmetric(vertical: 15),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    AppColor.orange,
                                    AppColor.orange,
                                    AppColor.orange
                                  ])),
                          child: Text(
                            "Серверга юбориш",
                            style: whiteText,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 15),
                    Expanded(
                      child: InkWell(
                        onTap: () => delete(),
                        child: Container(
                          // width: double.infinity,
                          padding: EdgeInsets.symmetric(vertical: 15),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    AppColor.red,
                                    AppColor.red,
                                    AppColor.red
                                  ])),
                          child: Text(
                            "Ўчириш",
                            style: whiteText,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            : null,
      ),
    );
  }

  sale() {
    print("FFF");
    List<Payment> list = [];
    for (int i = 0; i < selectedList.length; i++) {
      list.add(payments[selectedList[i]]);
    }
    _bloc.add(MultiplePayment(data: list));
    print("payments:${list}");
  }

  delete() {
    print("FFF");
    List<Payment> list = [];
    for (int i = 0; i < selectedList.length; i++) {
      list.add(payments[selectedList[i]]);
    }

     DialogUtils.showDeleteDialog(context, text: "Бу ҳужжатларни ўчиришни хоҳлайсизми?", toDelete: (){
   _bloc.add(DeletePayment(data: list));
     });
    
  }

  item(e) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 13, vertical: 6),
      padding: EdgeInsets.only(top: 15, bottom: 15, right: 10),
      decoration: BoxDecoration(
          color: AppColor.black.withOpacity(0.5),
          borderRadius: BorderRadius.circular(15),
          border: Border.all(
              color: e.send ? AppColor.brandColor2 : AppColor.transparent,
              width: e.send ? 2 : 0)),
      child: Row(
        children: [
          InkWell(
            onTap: () {
              print(selectedList.contains(payments.indexOf(e)));
              if (e.send) {
              } else
                setState(() {
                  if (selectedList.contains(payments.indexOf(e))) {
                    selectedList.remove(payments.indexOf(e));
                  } else {
                    selectedList.add(payments.indexOf(e));
                  }
                });
              print(selectedList);
            },
            child: Container(
              // color: AppColor.red,
              padding:
                  const EdgeInsets.symmetric(vertical: 15.0, horizontal: 12),
              child: Container(
                height: 15,
                width: 15,
                // margin: EdgeInsets.only(right: 10),
                decoration: BoxDecoration(
                    color: dotColor(e),
                    borderRadius: BorderRadius.circular(20),
                    // border: Border
                    border: Border.all(color: AppColor.white)),
              ),
            ),
          ),
          Expanded(
            child: InkWell(
              onTap: () {

                 if (e.send){
                Navigator.pushNamed(context, Routes.readPayment, arguments: {
                  "client": clients
                      .firstWhere((element) => element.mijozId == e.mijozId),
                  "data": e
                });
                 } else {
                    Navigator.pushNamed(context, Routes.editPayment, arguments: {
                  "client": clients
                      .firstWhere((element) => element.mijozId == e.mijozId),
                  "data": e
                }).then((value) async {
                              await getHive();
                            });
                 }
               
              },
              child: Column(
                children: [
                  TextRow(
                    text1: clients
                        .firstWhere((element) => element.mijozId == e.mijozId)
                        .mijozName,
                    text2: Services.currencyFormatter(e.kirimSummasi),
                  ),
                  TextRow(
                    text1: DateFormat('dd.MM.yyyy hh:mm')
                        .format(new DateTime.fromMillisecondsSinceEpoch(e.id)),
                    text2: currency
                        .firstWhere(
                            (element) => element.valyutaId == e.kassaValyutaId)
                        .valyutaNomi,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  dotColor(e) {
    if (e.send) {
      return AppColor.green;
    } else {
      if (selectedList.contains(payments.indexOf(e))) {
        return AppColor.brandColor2;
      } else
        return AppColor.transparent;
    }
  }
}
