import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/dialog/custom_dialogs.dart';
import 'package:basco/dialog/custom_loading_dialog.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/payment.dart';
import 'package:basco/models/user.dart';
import 'package:basco/models/user_client.dart';
import 'package:basco/screens/clientsScreens/cashFlow/bloc/payment_bloc.dart';
import 'package:basco/utils/services.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
class ReadPayment extends StatefulWidget {
  const ReadPayment({ Key? key }) : super(key: key);
  static const String routeName = 'read-payment';

  @override
  _ReadPaymentState createState() => _ReadPaymentState();
}

class _ReadPaymentState extends State<ReadPayment> {
  late final UserHive _hive = di.get<UserHive>();
  TextEditingController comment = TextEditingController();
  TextEditingController summa = TextEditingController();
  TextEditingController clientSum = TextEditingController();

  final _bloc = PaymentBloc();
  late dynamic args;
  Map<String, dynamic> data = {};
  List currencyList = [];

  dynamic currency;
  dynamic cashCurrency;
  dynamic userId;
  dynamic sinxTime = '';
  String clientName = '';
  dynamic selectedClient;
  dynamic cash;
  List cashs = [];

  @override
  void initState() {
    super.initState();
   
    summa.addListener(() {
      setState(() {});
    });
   
  }

  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    setState(() {
      data = accountData.data;
      cashs = data["user"] != null ? User.fromJson(data['user']).kassalar :[];
      sinxTime = data["clients"] != null
          ? (UserClient.fromJson(data['clients']).sinxronVaqti)
          : '';
      currencyList = accountData.data["user"] != null
          ? User.fromJson(accountData.data['user']).valyutalar
          : [];
    });
    print(cashs);
  }

  @override
  void didChangeDependencies() {
    getHive();

    setState(() {
      args = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
    });
    setState(() {
      clientName = args["client"] != null ? args["client"].mijozName : "";
      comment.text = args["data"] != null ?args['data'].izox:'';
      // cash = cashs.firstWhere((e)=> e.kassaId == args['data'].kassaId);
    });
    print(args['data'].kassaId);
   
   
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    String rate = '0';
    
    // clientSum.text =
    //     (double.parse(rate) * (double.tryParse(summa.text) ?? 0)).toStringAsFixed(3);

    cash = cashs.firstWhere((e)=> e.kassaId == args['data'].kassaId).kassa;
    cashCurrency = currencyList.firstWhere((e)=> e.valyutaId == args['data'].kassaValyutaId);
    currency = currencyList.firstWhere((e)=> e.valyutaId == args['data'].mijozValyutaId);
    summa.text =  args['data'].kirimSummasi;
    clientSum.text = args['data'].mijozSummasi;
    if (cashCurrency != null && currency != null) {
      rate =
          (double.parse(args['data'].mijozSummasi)/double.parse(args['data'].kirimSummasi)).toStringAsFixed(6);
    }
    String date = DateFormat("dd.MM.yyyy").format(DateFormat("yyyy-MM-dd hh:mm:ss").parse(args["data"].date));
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        body: BackgroundView(
          child: Column(
            children: [
              CustomAppBar(title: "Мижоздан пул кирими", rightMenu: false),
              Expanded(
                  child: SingleChildScrollView(
                child: BlocConsumer<PaymentBloc, PaymentState>(
                  listener: (context, state) {
                   CustomLoadingDialog.hide(context);
                    if (state is PaymentLoadingState) {
                      CustomLoadingDialog.show(context);
                    } else if (state is PaymentSuccessState) {
                      Navigator.pop(context);
                      DialogUtils.showLangDialog(context,
                          okBtnFunction: () {},
                          text: "Маълумотлар муваффиқиятли кўчирилди.");
                    } else if (state is PaymentErrorState) {
                      DialogUtils.showLangDialog(context,
                          okBtnFunction: () {}, text: state.message);
                      // showSnackBar(state.message);
                    }
                  },
                  builder: (context, state) {
                    return Column(
                      children: [
                      
                        Container(
                          margin:
                              EdgeInsets.symmetric(horizontal: 12, vertical: 5),
                          padding: EdgeInsets.symmetric(
                              horizontal: 19, vertical: 10),
                          decoration: BoxDecoration(
                              color: AppColor.black.withOpacity(0.5),
                              borderRadius: BorderRadius.circular(16)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              TextRow(
                                text1: "Номер",
                                text2: args["data"] != null? args["data"].id.toString().substring(5):"",
                                color: AppColor.brandColor2,
                              ),
                              divider(),
                              TextRow(
                                  text1: "Ҳужжат вақти", text2: date),
                              divider(),
                              TouchedRow(
                                text1: "Мижоз",
                                text2: clientName,
                                onPress: () async {
                                  // if (args["client"])
                                  //   print("false");
                                  // else {
                                  //   FocusScope.of(context).unfocus();
                                  //   dynamic selected =
                                  //       await Navigator.pushNamed(
                                  //           context, Routes.selectClient);

                                  //   if (selected != null) {
                                  //     setState(() {
                                  //       clientName = selected.mijozName;
                                  //       selectedClient = selected;
                                  //     });
                                  //   }
                                  // }
                                },
                                color: AppColor.brandColor,
                              ),
                              divider(),
                              TouchedRow(
                                text1: "Касса",
                                text2: cash != null ? cash : "",
                                onPress: () async {
                                  // FocusScope.of(context).unfocus();

                                  // dynamic cashData = await Navigator.pushNamed(
                                  //     context, Routes.cash);

                                  // if (cashData != null) {
                                  //   setState(() {
                                  //     cash = cashData;
                                  //   });
                                  // }
                                },
                              ),
                              divider(),
                              TouchedRow(
                                text1: "Касса валютаси",
                                text2: cashCurrency != null
                                    ? cashCurrency.valyutaNomi
                                    : "",
                                onPress: () async {
                                  // FocusScope.of(context).unfocus();

                                  // var currencyData = await Navigator.pushNamed(
                                  //     context, Routes.currency);

                                  // setState(() {
                                  //   cashCurrency = currencyData;
                                  //   currency = currencyData;
                                  // });
                                },
                              ),
                              divider(),
                              TextRow(text1:"Кирим суммаси" ,text2: Services.currencyFormatter(summa.text)),
                              // TextInputWithTitle(
                              //   text1: "Кирим суммаси",
                              //   value: summa,
                              //   shadow: false,
                              //   keyboardType: TextInputType.number,
                              //   validator: (value) {
                              //     return null;
                              //   },
                              // ),
                              divider(),
                              TextRow(text1: "Курс", text2: rate),
                              divider(),
                              TouchedRow(
                                text1: "Мижоз валютаси",
                                text2: currency != null
                                    ? currency.valyutaNomi
                                    : "",
                                onPress: () async {
                                  // FocusScope.of(context).unfocus();
                                  // var currencyData = await Navigator.pushNamed(
                                  //     context, Routes.currency);

                                  // setState(() {
                                  //   currency = currencyData;
                                  // });
                                },
                              ),
                              divider(),
                              TextRow(text1:"Мижоз суммаси" ,text2: Services.currencyFormatter(clientSum.text),),
                              // TextInputWithTitle(
                              //   text1: "Мижоз суммаси",
                              //   value: clientSum,
                              //   shadow: false,
                              //   keyboardType: TextInputType.number,
                              //   validator: (value) {
                              //     return null;
                              //   },
                              // ),
                              divider(),
                              Text("Изох",
                                  style: ligthTextStyle.copyWith(fontSize: 16)),
                              TextFormField(
                                controller: comment,
                                maxLines: 3,
                                readOnly: true,
                                cursorColor: AppColor.brandColor2,
                                style: whiteText,
                                decoration: InputDecoration(
                                  border: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          style: BorderStyle.none, width: 0)),
                                ),
                              ),
                             
                            ],
                          ),
                        )
                      ],
                    );
                  },
                ),
              ))
            ],
          ),
        ),
      ),
    );
  }

  onPress(routeName) {
    Navigator.pushNamed(context, routeName);
  }

  divider() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 4),
      child: Divider(color: AppColor.white),
    );
  }

  Touchable(title, ontap) {
    return Expanded(
      child: Material(
        color: AppColor.transparent,
        child: InkWell(
          onTap: () {
            ontap();
          },
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 5),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      AppColor.brandColor,
                      AppColor.brandColor2,
                      AppColor.brandColor
                    ]),
                boxShadow: [
                  BoxShadow(
                      color: AppColor.black.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 5)
                ],
                borderRadius: BorderRadius.circular(10)),
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: whiteText.copyWith(fontSize: 15),
            ),
          ),
        ),
      ),
    );
  }

  void sale(id) {
    data = {
      "user_id": userId,
      "mijoz_id":
         selectedClient!=null?selectedClient.mijozId: (args["data"] != null ? args["data"].mijozId : ""),
      "valyuta_id": currency?.valyutaId ?? "",
      "kassa_id": cash.kassaId,
      "kassa_valyuta_id": cashCurrency.valyutaId,
      "kirim_summasi": summa.text,
      "mijoz_valyuta_id": currency.valyutaId,
      "mijoz_summasi": clientSum.text,
      "sinxron_time": DateFormat("yyyyMMddhhmmss").format(sinxTime),
      "izox": comment.text,
      "send": false,
      "id": DateTime.now().millisecondsSinceEpoch,
    };
    Payment order = Payment.fromJson(data);
    print("TIME:${data["sinxron_time"]}");

    if ((currency != null) && (clientName.isNotEmpty)) {
      if (id == 0) {
        _bloc.add(Paid(data: order));
      } else if (id == 1) {
        _bloc.add(SavePayment(data: order));
      }
    } else {
      DialogUtils.showLangDialog(context,
          okBtnFunction: () {},
          text: "Мижоз,омбор,товар,валюта майдонларини  тўлдиринг.");
    }
  }

  double exchangeRate(valId1, valId2) {
    num a = currencyList.where((x) => x.valyutaId == valId1).single.valyutaKursi
        as num;
    num b = currencyList.where((x) => x.valyutaId == valId2).single.valyutaKursi
        as num;
    if (clientSum.text != '') {
      double k = (double.tryParse(clientSum.text) ?? 0) /
          (double.tryParse(summa.text) ?? 0);
      print("K:$k");
    }

    return (b / a);
  }
}