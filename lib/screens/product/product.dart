import 'package:basco/constants/colors.dart';
import 'package:basco/models/user_base.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
class ProductScreen extends StatefulWidget {
  const ProductScreen({ Key? key }) : super(key: key);
  static const String routeName = 'product-screen';

  @override
  _ProductScreenState createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
   late dynamic args;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    setState(() {
      args = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
    });
    print(args);
    super.didChangeDependencies();
  }
  @override
  Widget build(BuildContext context) {
     print("ST:${ args["storage"]}");
     List list =[];
     args["storage"].map((x)=> list =[...(x as Res).tovarlar]).toList();
     print("AA:$list");
     var seen = Set<String>();
     List uniquelist = list.where((element) => seen.add(element.tovarId)).toList();
    //  list.removeWhere((element) => element.tovarId == element.tovarId);
    // list.sort((a, b) => a.tovarId.compareTo(b.tovarId));

    print(uniquelist);
   
    //  var distinct = k.;
     return Scaffold(
      body: BackgroundView(
        child: Column(
          children: [
            CustomAppBar(
              title: "Товар номи",
              rightMenu: false,
            ),
             Expanded(
              child: MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child: ListView.builder(
                  itemBuilder: (BuildContext context, index) {
                    Tovarlar value = uniquelist[index];
                    return StoreItem(text1: value.tovarName,onPress:(){
                       Navigator.pop(context,value);
                    //   setState(() {
                    // selectedItem = index;
                    // });
                    },index: index,);

                  },
                  itemCount: uniquelist.length,
                ),
              )
            )
          ],
        ),
      ),
    );
  }
}