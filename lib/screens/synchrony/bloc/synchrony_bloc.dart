import 'package:basco/di/locator.dart';
import 'package:basco/repositories/services_repositories.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

part 'synchrony_event.dart';
part 'synchrony_state.dart';

class SynchronyBloc extends Bloc<SynchronyEvent, SynchronyState> {
  final ServicesRepository _api = di.get();

  SynchronyBloc() : super(SynchronyInitial()) {
    on<SynchronyEvent>((event, emit) async {
      if (event is Login) {
        await _login(event, emit);
      }
    });
  }

  Future<void> _login(
      SynchronyEvent event, Emitter<SynchronyState> emit) async {
        emit(SynchronyLoadingState());
        try {
         await _api.signIn(event.name, event.password);
         emit(SynchronySuccessState());
        } catch (e){
          emit(SynchronyErrorState(message: e.toString()));
          // print("ERROR:$e");
        }
      }
}
