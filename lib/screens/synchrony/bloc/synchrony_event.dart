part of 'synchrony_bloc.dart';

// @immutable
abstract class SynchronyEvent extends Equatable {
  const SynchronyEvent();
  @override
  List<Object> get props => [name,password];

  get name => null;
  get password => null;
}

class Login extends SynchronyEvent {
final String name,password;
const Login({required this.name,required this.password});
  @override
  List<Object> get props => [name,password];
}