part of 'synchrony_bloc.dart';

abstract class SynchronyState extends Equatable {
  const SynchronyState();
  
  @override
  List<Object> get props => [];
}


class SynchronyInitial extends SynchronyState {}


class SynchronyLoadingState extends SynchronyState {}

class SynchronySuccessState extends SynchronyState {}

class SynchronyErrorState extends SynchronyState {
final String message;

   SynchronyErrorState({required this.message});

  @override
  List<Object> get props => [message];
}