import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/dialog/custom_dialogs.dart';
import 'package:basco/dialog/custom_loading_dialog.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/screens/synchrony/bloc/synchrony_bloc.dart';
import 'package:basco/utils/function.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Synchrony extends StatefulWidget {
  const Synchrony({Key? key}) : super(key: key);
  static const String routeName = 'synchrony';

  @override
  _SynchronyState createState() => _SynchronyState();
}

class _SynchronyState extends State<Synchrony> {
  late final _bloc = SynchronyBloc();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _name = TextEditingController();
  TextEditingController _password = TextEditingController();
  late final UserHive _hive = di.get<UserHive>();

  @override
  void initState() {
    super.initState();
    getHive();
  }

  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    bool isFind =accountData.data["settings"]!=null;
    
    setState(() {
      _name.text = accountData.data["login"]!=null?accountData.data["login"]:"";
      _password.text = accountData.data["password"]!=null?accountData.data["password"]:"";

    });
    
    }
  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: BackgroundView(
          child: Column(
            children: [
              CustomAppBar(title: "Синхронлаш формаси", rightMenu: false),
              Expanded(
                child: SingleChildScrollView(
                  child: BlocConsumer<SynchronyBloc, SynchronyState>(
                    listener: (context, state) {
                      CustomLoadingDialog.hide(context);
                  if (state is SynchronyLoadingState) {
                    CustomLoadingDialog.show(context);
                  } else if (state is SynchronySuccessState) {
                     DialogUtils.showLangDialog(context, okBtnFunction: (){},text: "Маълумотлар муваффиқиятли кўчирилди.");
                  } else if (state is SynchronyErrorState) {
                    DialogUtils.showLangDialog(context, okBtnFunction: (){},text: state.message);
                    // showSnackBar(state.message);
                  }
                    },
                    builder: (context, state) {
                      return Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.symmetric(vertical: 28),
                              child: Text(
                                "Сервердаги логин паролни киритинг",
                                style: whiteText,
                              ),
                            ),
                            Input(
                                title: "Логин",
                                value: _name,
                                onChange: (value) => () {},
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter some text';
                                  }
                                  return null;
                                },
                                keyboardType: TextInputType.text),
                            Input(
                                title: "Парол",
                                value: _password,
                                onChange: (value) => () {},
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter some text';
                                  }
                                  return null;
                                },
                                keyboardType: TextInputType.text),
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 20),
                              child: Button(
                                  title: "Сервердан маьлумот олиш",
                                  onPress: () => onPress()),
                            )
                          ],
                        ),
                      );
                    },
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  onPress() {
    if (_formKey.currentState!.validate()) {
      print("do");
      _bloc.add(Login(name: _name.text, password: _password.text));
       FocusScope.of(context).unfocus();
    }
  }
}
