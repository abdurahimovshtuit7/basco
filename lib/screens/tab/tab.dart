import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/screens/mainScreen/main_screen.dart';
import 'package:basco/screens/productsScreen/products.dart';
import 'package:basco/screens/tab/main/main_tab.dart';
import 'package:basco/screens/tab/price/price.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TabScreen extends StatefulWidget {
  const TabScreen({Key? key}) : super(key: key);
  static const String routeName = 'tab-screen';

  @override
  _TabScreenState createState() => _TabScreenState();
}

class _TabScreenState extends State<TabScreen> {
  int _currentIndex = 0;
  late dynamic args;
  late List data = [];

  @override
  void didChangeDependencies() {
    setState(() {
      args = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
      data = args["data"];
    });

    super.didChangeDependencies();
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  tabBarItem(title, icon, index) {
    return BottomNavigationBarItem(
      icon: Container(
        padding: EdgeInsets.symmetric(vertical: 7),
        child: SvgPicture.asset(icon,
            height: 30,
            width: 30,
            color:
                _currentIndex == index ? AppColor.brandColor : AppColor.white),
      ),
      label: title,
      
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: BackgroundView(
        child: IndexedStack(
          index: _currentIndex,
          children: [
            MainTab(data: data),
            PriceScreen(data: data),   
          ],
        ),
      ),
      bottomNavigationBar: MediaQuery.removePadding(
        context: context,
        removeBottom: true,
        child: BottomNavigationBar(
          backgroundColor: AppColor.black.withOpacity(0.7),
          unselectedItemColor: AppColor.white,
          selectedItemColor: AppColor.brandColor,
          selectedFontSize: 14,
          unselectedFontSize: 14,
          type: BottomNavigationBarType.fixed,
          currentIndex: _currentIndex,
          onTap: onTabTapped,
          
          items: [
            tabBarItem("Основное", Images.home, 0),
            tabBarItem("Нархлар", Images.currency, 1)
          ],
        ),
      ),
      extendBodyBehindAppBar: true,
      extendBody: true,
    );
  }
}
