import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/orders.dart';
import 'package:basco/models/user_base.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:collection/collection.dart';

class MainTab extends StatefulWidget {
  final List data;
  const MainTab({Key? key, required this.data}) : super(key: key);

  @override
  _MainTabState createState() => _MainTabState();
}

class _MainTabState extends State<MainTab> {
  late final UserHive _hive = di.get<UserHive>();
  late dynamic dataAccount;
  List<StrixKodlar> barcodes = [];
  TextEditingController inputController = TextEditingController();
  bool isSearch = false;
  String text = '';
  List<Orders> trades = [];


  Future<void> getHive() async {
    
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    setState(() {
      dataAccount = accountData.data;
      barcodes = dataAccount["user_base"] != null
          ? UserBase.fromJson(dataAccount['user_base']).strixKodlar
          : [];
       trades = dataAccount["trade"] != null
          ? (List<Orders>.from(dataAccount["trade"].map((x) => Orders.fromJson(x))))
          : [];
    });
  }

 
  @override
  void didChangeDependencies() {
    getHive();

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var group = groupBy(widget.data, (x)=>(x as Tovarlar).xarakId).values.toList();
    print("barcodes:$barcodes");

        print("goup:${group}");


    return Scaffold(
      body: BackgroundView(
        child: Column(
          children: [
           
            SearchBar(
              title: widget.data[0].tovarName,
              ontap: () {
                setState(() {
                  isSearch = !isSearch;
                  text = '';
                });
              },
            ),
            isSearch ? search() : Container(),
            Expanded(
                child: MediaQuery.removePadding(
              context: context,
              removeTop: true,
              child: text == ""
                  ?ListView.builder(
                itemBuilder: (BuildContext context, index) {
                  dynamic value = group[index][0];
                  double count = 0;

                  group[index].map((x){
                   count += x.tovarSoni;
                  }).toList();
                  return Container(
                    margin: EdgeInsets.symmetric(vertical: 7, horizontal: 13),
                    padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                    decoration: BoxDecoration(
                        color: AppColor.black.withOpacity(0.5),
                        borderRadius: BorderRadius.circular(6)),
                    child: Column(
                      children: [
                        TextRow(
                          text1: value.xarakName != ''
                              ? value.xarakName
                              : "Стандарт",
                          text2: '',
                        ),
                        Divider(
                          color: AppColor.white,
                        ),
                        TextRow(
                          text1: barcodes
                                  .firstWhereOrNull((element) =>
                                      element.tovarId == value.tovarId &&
                                      element.xarakId == value.xarakId)
                                  ?.shtrixKod ??
                              "-------------",
                          text2: "${count-countProduct(value.tovarId, value.xarakId)} та",
                        ),
                      ],
                    ),
                  );
                },
                itemCount: group.length,
              ):ListView.builder(
                itemBuilder: (BuildContext context, index) {
                  dynamic value = group.where((row) => (row[0].xarakName==''?"Стандарт".toLowerCase().contains(text.toLowerCase()):row[0].xarakName
                                        .toLowerCase()
                                        .contains(text.toLowerCase())))
                                    .toList()[index][0];
                  double count = 0;

                  group.where((row) => (row[0].xarakName==''?"Стандарт".toLowerCase().contains(text.toLowerCase()):row[0].xarakName
                                        .toLowerCase()
                                        .contains(text.toLowerCase())))
                                    .toList()[index].map((x){
                   count += x.tovarSoni;
                  }).toList();
                  return Container(
                    margin: EdgeInsets.symmetric(vertical: 7, horizontal: 13),
                    padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                    decoration: BoxDecoration(
                        color: AppColor.black.withOpacity(0.5),
                        borderRadius: BorderRadius.circular(6)),
                    child: Column(
                      children: [
                        TextRow(
                          text1: value.xarakName != ''
                              ? value.xarakName
                              : "Стандарт",
                          text2: '',
                        ),
                        Divider(
                          color: AppColor.white,
                        ),
                        TextRow(
                          text1: barcodes
                                  .firstWhereOrNull((element) =>
                                      element.tovarId == value.tovarId &&
                                      element.xarakId == value.xarakId)
                                  ?.shtrixKod ??
                              "-------------",
                          text2: "${count - countProduct(value.tovarId, value.xarakId)} та",
                        ),
                      ],
                    ),
                  );
                },
                itemCount: group.where((row) => (row[0].xarakName==''?"Стандарт".toLowerCase().contains(text.toLowerCase()):row[0].xarakName
                                        .toLowerCase()
                                        .contains(text.toLowerCase())))
                                    .toList().length,
              ),
            ))
          ],
        ),
      ),
    );

  }
   Widget search() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 13),
      child: TextFormField(
        controller: inputController,
        onChanged: (value) => {
          setState(() {
            text = value;
          })
        },
        cursorColor: AppColor.brandColor,
        style: whiteText,
        decoration: InputDecoration(
          suffixIcon: IconButton(
            onPressed: () {
              inputController.clear();
              setState(() {
                text = "";
              });
            },
            icon: Icon(
              Icons.clear,
              color: AppColor.brandColor,
            ),
          ),
          hintStyle: TextStyle(color: AppColor.gray),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: AppColor.brandColor),
            borderRadius: BorderRadius.circular(25.0),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: AppColor.gray, width: 1),
            borderRadius: BorderRadius.circular(25.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
              width: 1,
            ),
            borderRadius: BorderRadius.circular(25.0),
          ),
          contentPadding: EdgeInsets.only(left: 20),
          filled: true,
          fillColor: AppColor.black.withOpacity(0.7),
          hintText: "Товарни қидириш",
        ),
      ),
    );
  }

  counter(omborId, tovarId) {
    dynamic newlist = groupBy(trades, (x) => (x as Orders).omborId)??[];
    dynamic omborList = newlist[omborId]??[];

    dynamic productOf = [];
    dynamic negativeProduct = [];
    // omborList.map((e) => productOf.addAll(e.ruyxat)).toList();

    for(int i =0;i<omborList.length;i++){
      if(omborList[i].vozvrat){
        negativeProduct.addAll(omborList[i].ruyxat);
      } else {
        productOf.addAll(omborList[i].ruyxat);
      }
    }
 
    double c = 0;
    for (int i = 0; i < productOf.length; i++) {
      if (productOf[i].tovarId == tovarId) {
        c += double.tryParse(productOf[i].soni) ?? 0;
      }
    }
    for(int i=0;i<negativeProduct.length;i++){
        if (negativeProduct[i].tovarId == tovarId) {
             c -= double.tryParse(negativeProduct[i].soni) ?? 0;
          }
    }

    return c;
  }

   double countProduct(tovarId, xarakId) {
      List productOf = [];
      List negativeProduct = [];
      //  trades.map((e) => e.ruyxat).toList();
    for(int i=0;i<trades.length;i++){
      if(!trades[i].vozvrat){
        productOf.addAll(trades[i].ruyxat);
      } else negativeProduct.addAll(trades[i].ruyxat);

    }
   //groupBy(trades, (x) => (x as Orders).omborId)??[];
    // dynamic omborList = newlist[omborId]??[];
    // dynamic tovarlar =  groupBy(newlist[omborId],(e)=>(e as Orders).);
    print("roycat:$productOf");
    print("returnList:$negativeProduct");

    // dynamic productOf = [];
    // dynamic negativeProduct = [];
    // // omborList.map((e) => productOf.addAll(e.ruyxat)).toList();
    // for(int i =0;i<omborList.length;i++){
    //   if(!omborList[i].vozvrat){
    //     productOf.addAll(omborList[i].ruyxat);
    //   }
    // }
    // for(int i =0;i<omborList.length;i++){
    //   if(omborList[i].vozvrat){
    //     negativeProduct.addAll(omborList[i].ruyxat);
    //   }
    // }
 
    print(productOf);
    double c = 0.0;
    for (int i = 0; i < productOf.length; i++) {
      if ((productOf[i].tovarId == tovarId) &&
          (productOf[i].xarakId == xarakId)) {
        c += double.tryParse(productOf[i].soni.toString()) ?? 0;
      }
    }
    for(int i=0;i<negativeProduct.length;i++){
        if ((negativeProduct[i].tovarId == tovarId) &&
          (negativeProduct[i].xarakId == xarakId)) {
             c -= double.tryParse(negativeProduct[i].soni.toString()) ?? 0;
          }
    }

    return c;
  }
}
