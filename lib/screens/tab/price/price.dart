import 'package:basco/constants/colors.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/user_base.dart';
import 'package:basco/utils/services.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';

class PriceScreen extends StatefulWidget {
  final dynamic data;
  const PriceScreen({Key? key, required this.data}) : super(key: key);

  @override
  _PriceScreenState createState() => _PriceScreenState();
}

class _PriceScreenState extends State<PriceScreen> {
  late final UserHive _hive = di.get<UserHive>();
  late dynamic dataAccount;
  List<SotishNarxlari> sellingPrice = [];

  Future<void> getHive() async {
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    setState(() {
      dataAccount = accountData.data;
      sellingPrice = dataAccount["user_base"] != null
          ? UserBase.fromJson(dataAccount['user_base']).sotishNarxlari
          : [];
    });
    // print("barcodes:$barcodes");
  }

  @override
  void didChangeDependencies() {
    getHive();

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    List<SotishNarxlari> list = [];
    for (int i = 0; i < widget.data.length; i++) {
      list = sellingPrice
          .where((element) =>
              element.tovarId == widget.data[i].tovarId )
          .toList();
    }
    // print("LIST:${list.length}");
    // print("PRICE:${sellingPrice.length}");
    // // sellingPrice.where((e)=>e.tovarId==)
    // print("ff");

    return Scaffold(
        body: BackgroundView(
      child: Column(
        children: [
          CustomAppBar(
            title: "Нархлар",
            rightMenu: false,
          ),
          Expanded(
              child: SingleChildScrollView(
            child: Container(
              // margin: EdgeInsets.symmetric(vertical: 15, horizontal: 12),
              // padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
              decoration: BoxDecoration(
                  // color: AppColor.black.withOpacity(0.5),
                  borderRadius: BorderRadius.circular(10)),
              child: Column(
                children: [
                  ...list.map((e) {
                    return e.narxTuriName!=''?Container(
                    margin: EdgeInsets.symmetric(vertical: 7, horizontal: 13),
                    padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                    decoration: BoxDecoration(
                        color: AppColor.black.withOpacity(0.5),
                        borderRadius: BorderRadius.circular(6)),
                    child: Column(
                      children: [
                        TextRow(
                          text1: e.xarakName != ''
                              ? e.xarakName
                              : "Стандарт",
                          text2: '',
                        ),
                        Divider(
                          color: AppColor.white,
                        ),
                        TextRow(
                          text1: e.narxTuriName ==''?"Стандарт":e.narxTuriName,
                          text2: "${Services.currencyFormatter(
                                    e.sumNarxi.toString())} сум" ,
                        ),
                      ],
                    ),
                  ):Container();
                        // Column(
                        //     children: [
                        //       TextRow(
                        //         text1: e.narxTuriName,
                        //         text2: Services.currencyFormatter(
                        //             e.sumNarxi.toString()),
                        //       ),
                        //       Divider(
                        //         color: AppColor.white,
                        //       )
                        //     ],
                        //   )
                        // : Container();
                  }).toList(),
                  SizedBox(height: 70,)
                ],
              ),
            ),
          ))
        ],
      ),
    ));
  }
}
