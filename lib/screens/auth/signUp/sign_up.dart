import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/account.dart';
import 'package:basco/routes.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);
  static const String routeName = 'sign-up';

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  late final _name = TextEditingController();
  late final _code = TextEditingController();
  late final _password = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late final UserHive _hive = di.get<UserHive>();
  List<AccountData> data = [];

  @override
  void initState() {
    super.initState();
    getHive();
  }

  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    setState(() {
      data = baza;
    });
  }

  Future<void> setData() async {
    List<AccountData> accountData = [...data];
    setState(() {});
    if (_formKey.currentState!.validate()) {
      accountData.add(AccountData(
          name: _name.text,
          parol: _code.text,
          id: (accountData.length + 1).toString(),
          data: {}));
      await _hive.saveAccount(accountData);

      Navigator.pushNamedAndRemoveUntil(
          context, Routes.accounts, (route) => false);
      // Navigator.pushReplacementNamed(context, Routes.accounts);
    }
    // for (int i = 0; i < data.length; i++) {
    //   print("User$i ${data[i].name}");
    // }
  }

  @override
  Widget build(BuildContext context) {
    // print(data);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: BackgroundView(
        child: Column(
          children: [
            CustomAppBar(title: "Рўйхатдан ўтиш", rightMenu: false),
            Expanded(
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 20),
                        child: Text(
                          "Логин ва паролни киритинг",
                          style: whiteText,
                        ),
                      ),
                      Input(
                          title: "Логин:",
                          value: _name,
                          onChange: (value) => () {},
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Илтимос,Логинни киритинг';
                            }
                            if (data
                                .where((element) => element.name == value)
                                .isNotEmpty) {
                              return 'Бу фойдаланувчи базада  бор,илтимос бошқа логин танланг';
                            }
                            return null;
                          },
                          keyboardType: TextInputType.text),
                      Input(
                          title: "Парол:",
                          value: _code,
                          onChange: (value) => () {},
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Илтимос,паролни киритинг';
                            }
                            return null;
                          },
                        
                  
                          keyboardType: TextInputType.text,
                          
                          
                          ),
                      Input(
                          title: "Паролни тасдиқланг:",
                          value: _password,
                          onChange: (value) => () {},
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Илтимос,паролни киритинг';
                            }
                            if (value != _code.text) {
                              return 'Парол тўғри келмади';
                            }
                            return null;
                          },
                          keyboardType: TextInputType.text),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Button(
                            title: "Рўйхатдан ўтиш", onPress: () => setData()),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
