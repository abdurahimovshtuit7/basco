import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/hive/user_hive.dart';
import 'package:basco/models/account.dart';
import 'package:basco/routes.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../di/locator.dart';

import 'package:get_it/get_it.dart';

class Accounts extends StatefulWidget {
  const Accounts({Key? key}) : super(key: key);
  static const String routeName = 'accounts';
  @override
  _AccountsState createState() => _AccountsState();
}

class _AccountsState extends State<Accounts> {
  // GetIt di = GetIt.I;

  late final UserHive _hive = di.get<UserHive>();
  late List<AccountData> data =[];

  @override
  void initState() {
    super.initState();
    getHive();
  }

  Future<void> getHive() async {
    print("HIVE: $_hive");

    final List<AccountData> baza = await _hive.getAccount();
    print(baza);
    setState(() {
      data = baza;
    });
  }

  @override
  Widget build(BuildContext context) {
    double statusBarHeight = MediaQuery.of(context).size.height;
    print(data);
    return Scaffold(
      body: BackgroundView(
        child: Column(
          children: [
            AccountAppBar(
              title: "Аккаунтлар",
              addButton: () => onPress(Routes.signUp,data),
              toolsButton: () {},
            ),
            data != null
                ? Expanded(
                    child: MediaQuery.removePadding(
                      context: context,
                      removeTop: true,
                      child: ListView.builder(
                      
                      itemBuilder: (BuildContext context, index) {
                        return accountItem(data: data[index]);
                      },
                      itemCount: data?.length,
                  ),
                    ))
                : Container(
                    child: Text("null"),
                  )
          ],
        ),
      ),
    );
  }

  onPress(routeName,data) {
    Navigator.pushNamed(context, routeName,arguments: {"data":data});
  }

  Widget accountItem({
    required AccountData? data,
  }) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      width: double.infinity,
      decoration: BoxDecoration(
          color: AppColor.black.withOpacity(0.5),
          borderRadius: BorderRadius.circular(10)),
      child: Material(
        borderRadius: BorderRadius.circular(10),
        color: AppColor.transparent,
        child: InkWell(
          onTap: () => onPress(Routes.login,data),
          onLongPress: ()=>_showMyDialog(data!.id),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
            child: Text(
              data!.name.toString(),
              style: whiteText,
            ),
          ),
        ),
      ),
    );
  }



  Future<void> _showMyDialog(String index) async {
    showCupertinoDialog<void>(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        // title: const Text('Alert'),
        content: const Text('Аккаунтни ўчирасизми??',style: TextStyle(fontSize: 18),),
        actions: <CupertinoDialogAction>[
          CupertinoDialogAction(
            child: const Text('Йўқ'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          CupertinoDialogAction(
            child: const Text('Ҳа'),
            isDestructiveAction: true,
            onPressed: () async {
              data.removeWhere((x)=>x.id == index);
              await _hive.saveAccount(data);
              // list.removeAt(index);
              setState(() {

              });
              Navigator.pop(context);
              // Do something destructive.
            },
          )
        ],
      ),
    );
   
  }

}
