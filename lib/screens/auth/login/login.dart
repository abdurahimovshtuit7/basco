import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/dialog/custom_loading_dialog.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/account.dart';
import 'package:basco/routes.dart';
import 'package:basco/screens/auth/login/bloc/login_bloc.dart';
import 'package:basco/utils/function.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);
  static const String routeName = 'login';

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  late final _bloc = LoginBloc();
  late final _password = TextEditingController();
  late dynamic args;

  @override
  void didChangeDependencies() {
    setState(() {
      args = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
    });
    print(args);
    super.didChangeDependencies();
  }
 
  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: BackgroundView(
          child: Column(
            children: [
              CustomAppBar(title: "Регистрация", rightMenu: false),
              Expanded(
                child: SingleChildScrollView(
                  child: BlocConsumer<LoginBloc, LoginState>(
                    listener: (context, state) {
                      CustomLoadingDialog.hide(context);
                  if (state is LoginLoadingState) {
                    CustomLoadingDialog.show(context);
                  } else if (state is LoginSuccessState) {
                    Navigator.pushNamed(context, Routes.main);
                  } else if (state is LoginErrorState) {
                    showSnackBar(state.message);
                  }
                    },
                    builder: (context, state) {
                      return Column(
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 28),
                            child: Text(
                              "Паролни киритинг",
                              style: whiteText,
                            ),
                          ),
                          Input(
                              title: "Парол",
                              value: _password,
                              onChange: (value) => () {},
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Please enter some text';
                                }
                                return null;
                              },
                              keyboardType: TextInputType.text),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal:20.0),
                            child: Button(
                                title: "Дастурга кириш",
                                onPress: () => onPress()),
                          ),
                        ],
                      );
                    },
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  onPress() async {
    _bloc.add(LoginButton(input:_password.text,password: args["data"]!.parol,name:args["data"]!.name));
    // if (_password.text == args["data"]!.parol) {
    //   await SharedPref().save("activeUser", args["data"]!.name);
    //   Navigator.pushNamed(context, routeName);
    // }

    FocusScope.of(context).unfocus();
  }
}
