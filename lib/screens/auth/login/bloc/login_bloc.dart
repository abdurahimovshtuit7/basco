import 'package:basco/utils/shared_pref.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginInitial()) {
    on<LoginEvent>((event, emit) async {
      if (event is LoginButton) {
        await _login(event, emit);
      }
    });
  }

  Future<void> _login(LoginEvent event, Emitter<LoginState> emit) async {
    emit(LoginLoadingState());

    if (event.input == event.password) {
      await SharedPref().save("activeUser", event.name);
      emit(LoginSuccessState());
    } else {
      emit(LoginErrorState(message: 'Паролни хато киритдингиз'));
    }

    // print("ERROR:$e");
  }
}
