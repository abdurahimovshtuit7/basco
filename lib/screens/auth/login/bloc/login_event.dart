part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
  @override
  List<Object> get props => [input,password,name];

  get input => null;
  get password => null;
  get name => null;

}

class LoginButton extends LoginEvent {
final String input,password,name;
const LoginButton({required this.input,required this.password,required this.name});
  @override
  List<Object> get props => [input,password,name];
}