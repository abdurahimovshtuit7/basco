import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/dialog/custom_dialogs.dart';
import 'package:basco/dialog/custom_loading_dialog.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/orders.dart';
import 'package:basco/models/user.dart';
import 'package:basco/models/user_client.dart';
import 'package:basco/routes.dart';
import 'package:basco/screens/clientsScreens/sale/bloc/sale_bloc.dart';
import 'package:basco/screens/clientsScreens/sale/card.dart';
import 'package:basco/utils/services.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';

class Sale extends StatefulWidget {
  const Sale({Key? key}) : super(key: key);
  static const String routeName = "sale";

  @override
  _SaleState createState() => _SaleState();
}

class _SaleState extends State<Sale> {
  TextEditingController comment = TextEditingController();
  late final _lastDate = TextEditingController(
      text: DateFormat("dd.MM.yyyy").format(DateTime.now()));

  final _bloc = SaleBloc();
  bool tabChange = true;
  bool _expanded = false;
  dynamic currency;
  dynamic selectedStore;
  late final UserHive _hive = di.get<UserHive>();
  Map<String, dynamic> data = {};
  List storage = [];
  dynamic userId;
  late dynamic args;
  dynamic productItem;
  List productItems = [];
  dynamic sinxTime = '';
  String clientName = '';
  dynamic selectedClient;
  bool isTemplateSelected = false;
  String deviceId = '';
  List clients = [];

  // double total = 0;
  // double count = 0;

  @override
  void initState() {
    super.initState();
    getHive();
  }

  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");
    String id = await SharedPref().read("deviceID")??'';

    final accountData = baza.firstWhere((x) => x.name == name);
    setState(() {
      data = accountData.data;
      storage =
          data["user"] != null ? User.fromJson(data['user']).omborlar : [];
      userId = data["user"] != null ? User.fromJson(data['user']).userId : '';
      sinxTime = data["clients"] != null
          ? (UserClient.fromJson(data['clients']).sinxronVaqti)
          : '';
      deviceId = id;
      clients  = data["clients"] != null?UserClient.fromJson(data['clients']).mijozlar :[];
    });
  }

  @override
  void didChangeDependencies() {
    setState(() {
      args = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
    });
    setState(() {
      clientName = args["data"] != null ? args["data"].mijozName : "";
    });

    print(args["data"]);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    print((selectedStore));
    double total = 0;
    double count = 0;
    for (int i = 0; i < productItems.length; i++) {
      total += double.parse(productItems[i]["totalAmount"]);
      count += productItems[i]["count"].toDouble();
      //  print("AAA:${productItems[i]["count"]}");
    }

    //  print("AAA:$a");
    // setState(() {
    //    total = a;
    //    count = b;
    // });
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        body: BackgroundView(
          child: Column(
            children: [
              CustomAppBar(title: "Savdo", rightMenu: false),
              Expanded(
                  child: SingleChildScrollView(
                child: BlocConsumer<SaleBloc, SaleState>(
                  listener: (context, state) {
                    CustomLoadingDialog.hide(context);
                    if (state is SaleLoadingState) {
                      CustomLoadingDialog.show(context);
                    } else if (state is SaleSuccessState) {
                      Navigator.pop(context);
                      DialogUtils.showLangDialog(context,
                          okBtnFunction: () {},
                          text: "Маълумотлар муваффиқиятли кўчирилди.");
                    } else if (state is SaleErrorState) {
                      DialogUtils.showLangDialog(context,
                          okBtnFunction: () {}, text: state.message);
                      // showSnackBar(state.message);
                    }
                  },
                  builder: (context, state) {
                    return Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 8),
                          child: Row(
                            children: [
                              Touchable("Серверга юбориш", () => {sale(0)}),
                              Touchable("Сақлаш ва чиқиш", () => {sale(1)}),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 13),
                          padding: EdgeInsets.symmetric(
                              vertical: 15, horizontal: 20),
                          decoration: BoxDecoration(
                              color: AppColor.black.withOpacity(0.5),
                              borderRadius: BorderRadius.circular(16)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                  onTap: () {
                                    setState(() {
                                      tabChange = true;
                                    });
                                  },
                                  child: Container(
                                      padding: EdgeInsets.only(
                                        bottom:
                                            5, // Space between underline and text
                                      ),
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom: BorderSide(
                                        color: tabChange
                                            ? AppColor.brandColor2
                                            : AppColor.transparent,
                                        width: 1.0, // Underline thickness
                                      ))),
                                      child: Text(
                                        "Реквизитлар",
                                        style: TextStyle(
                                            color: tabChange
                                                ? AppColor.white
                                                : AppColor.brandColor2
                                                    .withOpacity(0.8)),
                                      ))),
                              SizedBox(width: 20),
                              InkWell(
                                  onTap: () {
                                    setState(() {
                                      tabChange = false;
                                    });
                                  },
                                  child: Container(
                                    padding: EdgeInsets.only(
                                      bottom:
                                          5, // Space between underline and text
                                    ),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                      color: tabChange
                                          ? AppColor.transparent
                                          : AppColor.brandColor2,
                                      width: 1.0, // Underline thickness
                                    ))),
                                    child: Text(
                                      "Товарлар",
                                      style: TextStyle(
                                          color: tabChange
                                              ? AppColor.brandColor2
                                                  .withOpacity(0.8)
                                              : AppColor.white),
                                    ),
                                  )),
                            ],
                          ),
                        ),
                        tabChange
                            ? Requisites(
                                name: clientName,
                                // client: args["client"],
                                clientsNavigation: () async {
                                  if (args["client"])
                                    print("false");
                                  else {
                                    if (productItems.length == 0){
                                       dynamic selected =
                                        await Navigator.pushNamed(
                                            context, Routes.selectClient);
                                    if (selected != null)
                                      setState(() {
                                        clientName = selected.mijozName;
                                        selectedClient = selected;
                                      });
                                    }
                                   
                                  }
                                },
                                storageNavigation: () async {
                                  if (productItems.length == 0) {
                                    var store = await Navigator.pushNamed(
                                        context, Routes.storage,
                                        arguments: {"storage": storage});
                                    setState(() {
                                      selectedStore = store;
                                    });
                                  }
                                },
                                currencyNavigation: () async {
                                  if (productItems.length == 0) {
                                    var currencyData =
                                        await Navigator.pushNamed(
                                            context, Routes.currency);
                                    setState(() {
                                      currency = currencyData;
                                    });
                                  }
                                },
                                currency: currency,
                                store: selectedStore,
                                comment: comment,
                                count: Services.currencyFormatter(
                                    count.toString()),
                                total: Services.currencyFormatter(
                                    total.toString()),
                                itemLength: productItems.length,
                                send: false,
                                sinxTime: sinxTime,
                                date: _lastDate,
                                readOnly: false,
                                isTemplateSelected: isTemplateSelected,
                                templateChanged: (value) => {
                                  setState(() {
                                    isTemplateSelected = value;
                                  })
                                },
                                toDate: (time) {
                                  setState(() {
                                    _lastDate.text =
                                        DateFormat("dd.MM.yyyy").format(time);
                                  });
                                },
                              )
                            : Container(
                                margin: EdgeInsets.symmetric(horizontal: 12),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 15),
                                decoration: BoxDecoration(
                                    color: AppColor.black.withOpacity(0.5),
                                    borderRadius: BorderRadius.circular(16)),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Row(
                                            children: [
                                              Material(
                                                color: AppColor.transparent,
                                                child: InkWell(
                                                  onTap: () {
                                                    if (selectedStore == null) {
                                                      DialogUtils.showLangDialog(
                                                          context,
                                                          okBtnFunction: () {},
                                                          text:
                                                              "Омбор танланмаган!");
                                                    } else if (currency ==
                                                        null) {
                                                      DialogUtils.showLangDialog(
                                                          context,
                                                          okBtnFunction: () {},
                                                          text:
                                                              "Валютани танланг!");
                                                    } else if ((args["client"] ? args["data"].mijozId : selectedClient!=null?selectedClient.mijozId:"").isEmpty) {
                                                      DialogUtils.showLangDialog(
                                                          context,
                                                          okBtnFunction: () {},
                                                          text:
                                                              "Мижозни танланг!");
                                                    } else
                                                      onPress2(
                                                          Routes.addProduct,
                                                          arguments:<String,dynamic> {
                                                            "store":
                                                                selectedStore
                                                                    .omborId,
                                                            "currency":
                                                                currency,
                                                            "productItems":
                                                                productItems,
                                                            "products":
                                                                productItems,
                                                            "id":clients.firstWhere((element) => element.mijozId == (args["client"] ? args["data"].mijozId : selectedClient!=null?selectedClient.mijozId:"")).narxTuriId
                                                            
                                                          });
                                                  },
                                                  child: Container(
                                                    padding: EdgeInsets.all(8),
                                                    decoration: BoxDecoration(
                                                        color: AppColor.black
                                                            .withOpacity(0.7),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(6)),
                                                    child: SvgPicture.asset(
                                                        Images.plus),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(width: 24),
                                              Material(
                                                color: AppColor.transparent,
                                                child: InkWell(
                                                  onTap: () {
                                                    if (selectedStore == null) {
                                                      DialogUtils.showLangDialog(
                                                          context,
                                                          okBtnFunction: () {},
                                                          text:
                                                              "Омбор танланмаган!");
                                                    } else if (currency ==
                                                        null) {
                                                      DialogUtils.showLangDialog(
                                                          context,
                                                          okBtnFunction: () {},
                                                          text:
                                                              "Валютани танланг!");
                                                    } else
                                                      onPress(Routes.barCode,
                                                          arguments: {
                                                            "store":
                                                                selectedStore
                                                                    .omborId,
                                                            "currency":
                                                                currency,
                                                            "products":
                                                                productItems
                                                          });
                                                  },
                                                  child: Container(
                                                    padding: EdgeInsets.all(5),
                                                    decoration: BoxDecoration(
                                                        color: AppColor.black
                                                            .withOpacity(0.7),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(6)),
                                                    child: SvgPicture.asset(
                                                        Images.barCode),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        _dropDownButton()
                                        // Material(
                                        //   color: AppColor.transparent,
                                        //   child:
                                        //   InkWell(
                                        //     onTap: () {

                                        //     },
                                        //     child: Container(
                                        //       padding: EdgeInsets.symmetric(
                                        //           horizontal: 10, vertical: 5),
                                        //       child: Icon(Icons.delete_forever,color: AppColor.white,size: 35,),
                                        //     ),
                                        //   ),
                                        // )
                                      ],
                                    ),
                                    SizedBox(height: 4),
                                    ...productItems.map((item) {
                                      return Card3(
                                          item: item,
                                          edit: () async {
                                            dynamic p =
                                                await Navigator.pushNamed(
                                                    context, Routes.editProduct,
                                                    arguments: {
                                                  "store":
                                                      selectedStore.omborId,
                                                  "currency": currency,
                                                  "productItems": productItems,
                                                  "item": item,
                                                  "products": productItems,
                                                  "id":clients.firstWhere((element) => element.mijozId == (args["client"] ? args["data"].mijozId : selectedClient!=null?selectedClient.mijozId:"")).narxTuriId


                                                });
                                            if (p != null) {
                                              setState(() {
                                                productItems[productItems
                                                    .indexOf(item)] = p;
                                              });
                                            }

                                            // print("KK");
                                          });
                                    }).toList(),
                                    SizedBox(
                                      height: 7,
                                    ),
                                    TextRow(
                                        text1: "Жами товарлар суммаси",
                                        text2: Services.currencyFormatter(
                                            total.toString())),
                                    Divider(
                                      color: AppColor.white,
                                    ),
                                    TextRow(
                                        text1: "Жами товарлар сони",
                                        text2: Services.currencyFormatter(
                                            count.toString()))
                                  ],
                                ),
                              ),
                      ],
                    );
                  },
                ),
              ))
            ],
          ),
        ),
      ),
    );
  }

  void onPress(routeName, {arguments}) async {
    dynamic item =
        await Navigator.of(context).pushNamed(routeName, arguments: arguments);

    if (await item != null) {
      print("ITEM:$item");
      setState(() {
        productItems.add(item);
      });
    } else {
      // DialogUtils.showLangDialog(context,
      //     okBtnFunction: () {}, text: "Штрих кодли товар топилмади");
    }
  }

  void onPress2(routeName, {arguments}) async {
    dynamic item =
        await Navigator.of(context).pushNamed(routeName, arguments: arguments);
    // print("item:${item["character"].xarakName}");
    // print("pr:${item["product"].tovarName}");

    if (item != null) {
      setState(() {
        productItems.add(item);
      });
    }
  }

  void sale(id) {
    List newList = [];
    print("DATAS:$selectedClient");
        dynamic listItem = productItems.map((e) {
      return {  
        "tovar_id": e["product"].tovarId,
        "xarak_id": e["character"].xarakId,
        "soni": e["count"].toString(),
        "narxi": e["price"].toString()
      };
    });

    print(listItem);

    newList.addAll(listItem);

    if ((selectedStore != null) &&
        (currency != null) &&
        (clientName.isNotEmpty) &&
        (newList.isNotEmpty)) {

    double total = 0;
    // double count = 0;
    for (int i = 0; i < productItems.length; i++) {
      total += double.parse(productItems[i]["totalAmount"]);
      // count += productItems[i]["count"].toDouble();
      //  print("AAA:${productItems[i]["count"]}");
    }

    Map<String, dynamic> datas;
    datas = {
      "user_id": userId,
      "mijoz_id":
          args["client"] ? args["data"].mijozId : selectedClient.mijozId,
      "ombor_id": selectedStore?.omborId ?? "",
      "valyuta_id": currency?.valyutaId ?? "",
      "sinxron_time": DateFormat("yyyyMMddhhmmss").format(sinxTime),
      "izox": comment.text,
      "ruyxat": newList,
      "send": false,
      "id": DateTime.now().millisecondsSinceEpoch,
      "date": DateFormat("dd.MM.yyyy").parse(_lastDate.text).toString(),
      "device_id":deviceId,
      "vozvrat": isTemplateSelected,
      "total": total.toString()
    };
        Orders order = Orders.fromJson(datas);

      if (id == 0) {
        _bloc.add(Sold(data: order));
      } else if (id == 1) {
        _bloc.add(Save(data: order));
      }
    } else {
      if (selectedStore == null) {
        DialogUtils.showLangDialog(context,
            okBtnFunction: () {},
            text: "Омборни танланг.");
      }
      if(selectedClient ==null){
         DialogUtils.showLangDialog(context,
            okBtnFunction: () {},
            text: "Мижозни танланг.");
      }
      if (currency == null) {
         DialogUtils.showLangDialog(context,
            okBtnFunction: () {},
            text: "Валютани танланг.");
      }
      // if (clientName.isNotEmpty) {
      //    DialogUtils.showLangDialog(context,
      //       okBtnFunction: () {},
      //       text: "Мижозни танланг.");
      // }
      if (newList.isEmpty) {
         DialogUtils.showLangDialog(context,
            okBtnFunction: () {},
            text: "Товар танланг.");
      }
      
    }
  }

  productNameRow(
      {required String name,
      required String detail,
      required Function onPress,
      required bool expanded}) {
    return ExpansionPanel(
        headerBuilder: (context, isExpanded) {
          return Material(
            color: AppColor.transparent,
            child: InkWell(
              onTap: () => onPress(),
              child: Container(
                // margin: EdgeInsets.symmetric(vertical: 5),
                padding: EdgeInsets.symmetric(horizontal: 17, vertical: 15),
                decoration: BoxDecoration(
                    // color: AppColor.black.withOpacity(0.7),
                    // borderRadius: BorderRadius.circular(10)
                    ),
                child: Row(
                  children: [
                    Expanded(
                        child: Text(
                      name,
                      style: whiteText,
                    )),
                    Text(
                      detail,
                      style: whiteText.copyWith(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    SizedBox(width: 10),
                    SvgPicture.asset(Images.rigthArrow)
                  ],
                ),
              ),
            ),
          );
        },
        body: ListTile(
          iconColor: AppColor.green,
          title:
              Text('Description text', style: TextStyle(color: Colors.white)),
          tileColor: AppColor.black.withOpacity(0.2),
        ),
        isExpanded: _expanded,
        canTapOnHeader: false,
        backgroundColor: AppColor.black.withOpacity(0.7));
  }

  Touchable(title, ontap) {
    return Expanded(
      child: Material(
        color: AppColor.transparent,
        child: InkWell(
          onTap: () {
            ontap();
          },
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 5),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      AppColor.brandColor,
                      AppColor.brandColor2,
                      AppColor.brandColor
                    ]),
                boxShadow: [
                  BoxShadow(
                      color: AppColor.black.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 5)
                ],
                borderRadius: BorderRadius.circular(10)),
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: whiteText.copyWith(fontSize: 15),
            ),
          ),
        ),
      ),
    );
  }

  Widget _dropDownButton() {
    return PopupMenuButton(
        icon: const Icon(
          Icons.delete_forever,
          color: AppColor.white,
        ),
        iconSize: 35,
        color: AppColor.white,
        // tooltip: "dsd",
        itemBuilder: (BuildContext context) => <PopupMenuEntry>[
              PopupMenuItem(
                onTap: () {
                  setState(() {
                    productItems = [];
                  });
                },
                child: Text('Ўчириш'),
              ),
              PopupMenuItem(
                onTap: () {},
                child: Text('Бекор қилиш'),
              ),
            ]);
  }
}
