part of 'sale_bloc.dart';

abstract class SaleEvent extends Equatable {
  const SaleEvent();

  @override
  List<Object> get props => [data];
    get data => null;

}
class Sold extends SaleEvent {
final Orders data;
const Sold({required this.data});
  @override
  List<Object> get props => [data];
}

class Save extends SaleEvent {
final Orders data;
const Save({required this.data});
  @override
  List<Object> get props => [data];
}

class ChangeTrade extends SaleEvent {
final Orders data;
const ChangeTrade({required this.data});
  @override
  List<Object> get props => [data];
}


class SingleTrade extends SaleEvent {
final Orders data;
const SingleTrade({required this.data});
  @override
  List<Object> get props => [data];
}
