part of 'sale_bloc.dart';

abstract class SaleState extends Equatable {
  const SaleState();
  
  @override
  List<Object> get props => [];
}

class SaleInitial extends SaleState {}

class SaleLoadingState extends SaleState {}

class SaleSuccessState extends SaleState {}

class SaleErrorState extends SaleState {
final String message;

   SaleErrorState({required this.message});

  @override
  List<Object> get props => [message];
}