import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/account.dart';
import 'package:basco/models/orders.dart';
import 'package:basco/models/user.dart';
import 'package:basco/repositories/services_repositories.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'sale_event.dart';
part 'sale_state.dart';

class SaleBloc extends Bloc<SaleEvent, SaleState> {
  final ServicesRepository _api = di.get();
  final UserHive _hive = di.get<UserHive>();

  

  SaleBloc() : super(SaleInitial()) {
    on<SaleEvent>((event, emit) async {
      if (event is Sold) {
        await _sold(event, emit);
      } 

      if(event is Save){
        await _save(event,emit);
      }
       if(event is ChangeTrade){
        await _change(event, emit);
      }
      if(event is SingleTrade){
        await _single(event, emit);
      }
    });
  }

  Future<void> _sold(SaleEvent event, Emitter<SaleState> emit) async {
    emit(SaleLoadingState());
    try {
       String name = await SharedPref().read("activeUser");
       List<AccountData> baza = await _hive.getAccount();

        Map<String, dynamic> map = {
          "trade": [],
        };
        await _api.order(event.data);

        print("rrr: ${event.data.runtimeType}");

        event.data.setSend = true;
       
        print("ORDER:${event.data}");
        // List a = [];
        // a = [...a,event.data];

        // print("a:${a[0].send}");
       
        baza.firstWhere((x) => x.name == name).data["trade"] !=null?baza.firstWhere((x) => x.name == name).data["trade"]: baza.firstWhere((x) => x.name == name).data.addAll(map);
        
        baza.firstWhere((x) => x.name == name).data["trade"].add(event.data);
        
      
        await _hive.saveAccount(baza);
    
      emit(SaleSuccessState());
    } catch (e) {
      emit(SaleErrorState(message: e.toString()));
      // print("ERROR:$e");
    }
  }

   Future<void> _save(SaleEvent event, Emitter<SaleState> emit) async {
    emit(SaleLoadingState());
    try {
       String name = await SharedPref().read("activeUser");
       List<AccountData> baza = await _hive.getAccount();

        Map<String, dynamic> map = {
          "trade": [],
        };
        print("DDDDDDD");
        baza.firstWhere((x) => x.name == name).data["trade"] !=null?baza.firstWhere((x) => x.name == name).data["trade"]: baza.firstWhere((x) => x.name == name).data.addAll(map);
       print("DDDDDDD3:${baza.firstWhere((x) => x.name == name).data["trade"]}");
        baza.firstWhere((x) => x.name == name).data["trade"] =[...baza.firstWhere((x) => x.name == name).data["trade"],event.data];
        // baza.firstWhere((x) => x.name == name).data.addAll(map);
        // baza.firstWhere((x) => x.name == name).data["trade"].add(event.data);
         print("DDDDDDD2");
        await _hive.saveAccount(baza);
      // await Future.delayed(const Duration(milliseconds: 1500), () {});
      //  await _api.signIn(event.name, event.password);
      emit(SaleSuccessState());
    } catch (e) {
      emit(SaleErrorState(message: e.toString()));
      // print("ERROR:$e");
    }
  }
     Future<void> _change(SaleEvent event, Emitter<SaleState> emit) async {
    emit(SaleLoadingState());
    try {
       String name = await SharedPref().read("activeUser");
       List<AccountData> baza = await _hive.getAccount();
       print("EVENT:${event.data.id}");
       
        for(int i=0;i<baza.firstWhere((x) => x.name == name).data["trade"].length;i++){
          if(baza.firstWhere((x) => x.name == name).data["trade"][i]['id'] == (event.data as Orders).id){
            baza.firstWhere((x) => x.name == name).data["trade"][i] = event.data;
          }
        }

        await _hive.saveAccount(baza);

      emit(SaleSuccessState());
    } catch (e) {
      emit(SaleErrorState(message: e.toString()));
     
    }
  }

   Future<void> _single(SaleEvent event, Emitter<SaleState> emit) async {
    emit(SaleLoadingState());
    try {
       String name = await SharedPref().read("activeUser");
       List<AccountData> baza = await _hive.getAccount();

        await _api.order(event.data);
        event.data.setSend = true;
       
        // print("ORDER:${event.data}");
        // baza.firstWhere((x) => x.name == name).data["Sale"] !=null?baza.firstWhere((x) => x.name == name).data["Sale"]: baza.firstWhere((x) => x.name == name).data.addAll(map);       
        // baza.firstWhere((x) => x.name == name).data["Sale"].add(event.data);
         for(int i=0;i<baza.firstWhere((x) => x.name == name).data["trade"].length;i++){
          if(baza.firstWhere((x) => x.name == name).data["trade"][i]['id'] == (event.data as Orders).id){
            baza.firstWhere((x) => x.name == name).data["trade"][i] = event.data;
          }
        }
      
        await _hive.saveAccount(baza);
    
      emit(SaleSuccessState());
    } catch (e) {
      emit(SaleErrorState(message: e.toString()));
      // print("ERROR:$e");
    }
  }
}
