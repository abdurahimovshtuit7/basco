import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/user.dart';
import 'package:basco/routes.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';

class Currency extends StatefulWidget {
  const Currency({Key? key}) : super(key: key);
  static const String routeName = 'currency';

  @override
  _CurrencyState createState() => _CurrencyState();
}

class _CurrencyState extends State<Currency> {
  late int selectedItem = -1;
   late final UserHive _hive = di.get<UserHive>();
  Map<String, dynamic> data = {};
  List currency = [];

  @override
  void initState() {
    super.initState();
    getHive();
  }

  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
      setState(() {
        data = accountData.data;
        currency = data["user"]!=null?User.fromJson(data['user']).valyutalar:[];
      });
    
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BackgroundView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            CustomAppBar(title: "Валюталар"),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10, horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  
                  Text(
                    "Пул бирлиги",
                    style: TextStyle(
                        color: AppColor.white,
                        fontSize: 18,
                        fontFamily: AppFont.MontserratBold,
                        fontWeight: FontWeight.w600),
                  ),
                 
                ],
              ),
            ),
            Expanded(
              child: MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child: ListView.builder(
                  itemBuilder: (BuildContext context, index) {
                    var value = currency[index];
                    return CheckedItem(text1: value.valyutaNomi,text2: "00000001",onPress:(){
                      Navigator.pop(context,value);
                    },index:index+1);

                  },
                  itemCount: currency.length,
                ),
              )
            )
          ],
        ),
      ),
    );
  }
}
