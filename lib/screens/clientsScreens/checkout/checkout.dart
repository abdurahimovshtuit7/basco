import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
class Checkout extends StatefulWidget {
  const Checkout({ Key? key }) : super(key: key);
  static const String routeName = 'ckeckout';

  @override
  _CheckoutState createState() => _CheckoutState();
}

class _CheckoutState extends State<Checkout> {
  late int selectedItem = -1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BackgroundView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            CustomAppBar(title: "Кассалар"),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10, horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  
                  Text(
                    "Код",
                    style: TextStyle(
                        color: AppColor.white,
                        fontSize: 18,
                        fontFamily: AppFont.MontserratBold,
                        fontWeight: FontWeight.w600),
                  ),
                  Text(
                    "Мижоз",
                    style: TextStyle(
                        color: AppColor.white,
                        fontSize: 18,
                        fontFamily: AppFont.MontserratBold,
                        fontWeight: FontWeight.w600),
                  ),
                ],
              ),
            ),
            Expanded(
              child: MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child: ListView.builder(
                  itemBuilder: (BuildContext context, index) {
                    return CheckedItem(text1: "Хамкорбанк касса",text2: "00000001",onPress:(){setState(() {
                    selectedItem = index;
                    });},index: index+1,);

                  },
                  itemCount: 9,
                ),
              )
            )
          ],
        ),
      ),
    );
  }
}