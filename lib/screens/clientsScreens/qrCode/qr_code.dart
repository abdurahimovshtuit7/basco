import 'dart:io';
import 'dart:ffi';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/dialog/custom_dialogs.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/user.dart';
import 'package:basco/models/user_base.dart';
import 'package:basco/routes.dart';
import 'package:basco/utils/services.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:collection/src/iterable_extensions.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QRCodeScreen extends StatefulWidget {
  const QRCodeScreen({Key? key}) : super(key: key);
  static const String routeName = 'bar_code';

  @override
  _QRCodeScreenState createState() => _QRCodeScreenState();
}

class _QRCodeScreenState extends State<QRCodeScreen> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  late final UserHive _hive = di.get<UserHive>();
  Barcode? result;
  QRViewController? controller;
  late dynamic args;
  late dynamic data = '';
  late dynamic dataAccount;
  late List products = [];
  late List sellingPrice = [];
  late List shtrixCode = [];
  String dollarId = '';
  List currency = [];
 dynamic datas;
  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    } else if (Platform.isIOS) {
      controller!.resumeCamera();
    }
  }

  @override
  void didChangeDependencies() {
    setState(() {
      args = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
      data = args["store"];
    });
    getHive();

    super.didChangeDependencies();
  }

  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    setState(() {
      dataAccount = accountData.data;
      products = dataAccount["user_base"] != null
          ? UserBase.fromJson(dataAccount['user_base'])
              .response
              .firstWhere((element) => element.omborId == data)
              .tovarlar
          : [];
      sellingPrice = dataAccount["user_base"] != null
          ? UserBase.fromJson(dataAccount['user_base']).sotishNarxlari
          : [];

      shtrixCode = dataAccount["user_base"] != null
          ? UserBase.fromJson(dataAccount['user_base']).strixKodlar
          : [];

      dollarId = dataAccount["user"] != null
          ? User.fromJson(dataAccount["user"]).dollarId
          : "";
      currency = dataAccount["user"] != null
          ? User.fromJson(dataAccount['user']).valyutalar
          : [];
    });
  }

  @override
  Widget build(BuildContext context) {
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 280.0
        : 300.0;
    print(result?.code);

         dynamic pr = shtrixCode.firstWhereOrNull(
          (element) => element.shtrixKod == (result?.code ?? '1'));
      dynamic product = pr != null
          ? sellingPrice.firstWhere((element) =>
              element.tovarId == pr.tovarId && element.xarakId == pr.xarakId)
          : null;
      dynamic tovar = pr != null
          ? products.firstWhere((element) =>
              element.tovarId == pr.tovarId && element.xarakId == pr.xarakId)
          : null;
      if (result != null) {
        // Future.delayed(Duration(seconds: 2), () {
       
        if (product != null) {
          dynamic total;
          double narx = product.dollarNarxi;
          double priceCurrency = Services.converterWithoutAsync(
              dollarId, narx, args["currency"].valyutaId, currency);
          final n = pr?.soni ?? 0;
          final price = priceCurrency;

          total = (price * n).toStringAsFixed(3);
          print("TOTAL:${total}");
          
          setState(() {
            datas = {
            "product": tovar,
            "count": n,
            "totalAmount": total,
            "character": tovar,
            "price": price.toString()
          };
          });
          // Navigator.of(context).pop(datas);
          // Navigator.of(context).popAndPushNamed(Routes.sale, result: datas);
        } else {
          // Navigator.of(context).pop();
          // Navigator.of(context).popAndPushNamed(Routes.sale);
        }
      //   WidgetsBinding.instance?.addPostFrameCallback((_) {
      //     // Navigator.of(context).pop(datas);
      //     // });
      // //     Navigator.of(context).pushNamedAndRemoveUntil(Routes.sale, (Route<dynamic> route) => false,
      // // arguments: datas);  
      //   });
      }
    return Scaffold(
      backgroundColor: AppColor.black,
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 6,
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
              formatsAllowed: [],
              overlay: QrScannerOverlayShape(
                  borderColor: Colors.red,
                  borderRadius: 10,
                  borderLength: 30,
                  borderWidth: 10,
                  cutOutSize: scanArea),
            ),
          ),
          Expanded(
            flex: 2,
            child:
            datas!=null?
             Center(
              child:SingleChildScrollView(
                child: Container(
                                  padding: EdgeInsets.symmetric(horizontal: 20),

                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                    SizedBox(height: 10,),
                    TextRow(text1: "Tovar nomi",text2: datas["product"].tovarName),
                    TextRow(text1: "Tovar xarakteristikasi",text2: datas["character"].xarakName),
                    TextRow(text1: "Tovar narxi",text2: datas["price"].toString()),

                     Button(onPress: (){
                       Navigator.of(context).pop(datas);
                     }, title :"OK",margin: 10,)

                  ],),
                ),
              ),
            ):(result!=null)?Center(
              child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                    Text("Штрих кодли товар топилмади",style: whiteText,),
                    
                     Button(onPress: (){
                       Navigator.of(context).pop();
                     }, title :"OK",margin: 10,)

                  ],),
                ),
            ):
              Center(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Text("Штрих кодни сканерланг",style: whiteText,textAlign: TextAlign.center,),
                ),
              ),
            ),
          
        ],
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) async {
      setState(() {
        result = scanData;
      });
 
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
