import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/models/user_base.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';

class ProductName extends StatefulWidget {
  const ProductName({Key? key}) : super(key: key);
  static const String routeName = 'product-name';

  @override
  _ProductNameState createState() => _ProductNameState();
}

class _ProductNameState extends State<ProductName> {
  late dynamic args;
  TextEditingController inputController = TextEditingController();
  bool isSearch = false;
  String text = '';


  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    setState(() {
      args = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
    });
    print(args);
    super.didChangeDependencies();
  }
  @override
  Widget build(BuildContext context) {
    var seen = Set<String>();
     List uniquelist =  args["products"].where((element) => seen.add(element.tovarId)).toList();
    return Scaffold(
      body: BackgroundView(
        child: Column(
          children: [
           
              SearchBar(
              title: "Товар номи",
              ontap: () {
                setState(() {
                  isSearch = !isSearch;
                  text = '';
                });
              },
            ),
            isSearch ? search() : Container(),
             Expanded(
              child: text == ""
                  ? MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child: ListView.builder(
                  itemBuilder: (BuildContext context, index) {
                    Tovarlar value = uniquelist[index];
                    return StoreItem(text1: value.tovarName,onPress:(){
                       Navigator.pop(context,value);
                    //   setState(() {
                    // selectedItem = index;
                    // });
                    },index: index,);

                  },
                  itemCount: uniquelist.length,
                ),
              ):MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child: ListView.builder(
                  itemBuilder: (BuildContext context, index) {
                    Tovarlar value = uniquelist.where((row) => (row.tovarName
                                        .toLowerCase()
                                        .contains(text.toLowerCase())))
                                    .toList()[index];
                    return StoreItem(text1: value.tovarName,onPress:(){
                       Navigator.pop(context,value);
                    //   setState(() {
                    // selectedItem = index;
                    // });
                    },index: index,);

                  },
                  itemCount: uniquelist.where((row) => (row.tovarName
                                        .toLowerCase()
                                        .contains(text.toLowerCase())))
                                    .toList().length,
                ),
              )
            )
          ],
        ),
      ),
    );
  }

  divider() {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Divider(color: AppColor.white));
  }
   Widget search() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 13),
      child: TextFormField(
        controller: inputController,
        onChanged: (value) => {
          setState(() {
            text = value;
          })
        },
        cursorColor: AppColor.brandColor,
        style: whiteText,
        decoration: InputDecoration(
          suffixIcon: IconButton(
            onPressed: () {
              inputController.clear();
              setState(() {
                text = "";
              });
            },
            icon: Icon(
              Icons.clear,
              color: AppColor.brandColor,
            ),
          ),
          hintStyle: TextStyle(color: AppColor.gray),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: AppColor.brandColor),
            borderRadius: BorderRadius.circular(25.0),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: AppColor.gray, width: 1),
            borderRadius: BorderRadius.circular(25.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
              width: 1,
            ),
            borderRadius: BorderRadius.circular(25.0),
          ),
          contentPadding: EdgeInsets.only(left: 20),
          filled: true,
          fillColor: AppColor.black.withOpacity(0.7),
          hintText: "Товарни қидириш",
        ),
      ),
    );
  }
}
