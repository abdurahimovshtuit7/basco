import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/account.dart';
import 'package:basco/models/payment.dart';
import 'package:basco/repositories/services_repositories.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'payment_event.dart';
part 'payment_state.dart';

class PaymentBloc extends Bloc<PaymentEvent, PaymentState> {
  final ServicesRepository _api = di.get();
  final UserHive _hive = di.get<UserHive>();

  PaymentBloc() : super(PaymentInitial()) {
    on<PaymentEvent>((event, emit) async {
      if (event is Paid) {
        await _sold(event, emit);
      } 
      if(event is SavePayment){
        await _save(event,emit);
      }
      if(event is ChangePayment){
        await _change(event, emit);
      }
      if(event is SinglePayment){
        await _single(event, emit);
      }
    });
  }

  Future<void> _sold(PaymentEvent event, Emitter<PaymentState> emit) async {
    emit(PaymentLoadingState());
    try {
       String name = await SharedPref().read("activeUser");
       List<AccountData> baza = await _hive.getAccount();

        Map<String, dynamic> map = {
          "payment": [],
        };
        await _api.payment(event.data);

        event.data.setSend = true;
       
        print("ORDER:${event.data}");

       
        baza.firstWhere((x) => x.name == name).data["payment"] !=null?baza.firstWhere((x) => x.name == name).data["payment"]: baza.firstWhere((x) => x.name == name).data.addAll(map);
        
        baza.firstWhere((x) => x.name == name).data["payment"].add(event.data);
        
      
        await _hive.saveAccount(baza);
    
      emit(PaymentSuccessState());
    } catch (e) {
      emit(PaymentErrorState(message: e.toString()));
      // print("ERROR:$e");
    }
  }

   Future<void> _save(PaymentEvent event, Emitter<PaymentState> emit) async {
    emit(PaymentLoadingState());
    try {
       String name = await SharedPref().read("activeUser");
       List<AccountData> baza = await _hive.getAccount();

        Map<String, dynamic> map = {
          "payment": [],
        };
        print("DDDDDDD");
        baza.firstWhere((x) => x.name == name).data["payment"] !=null?baza.firstWhere((x) => x.name == name).data["payment"]: baza.firstWhere((x) => x.name == name).data.addAll(map);
       print("DDDDDDD3:${baza.firstWhere((x) => x.name == name).data["payment"]}");
        baza.firstWhere((x) => x.name == name).data["payment"] =[...baza.firstWhere((x) => x.name == name).data["payment"],event.data];
        // baza.firstWhere((x) => x.name == name).data.addAll(map);
        // baza.firstWhere((x) => x.name == name).data["trade"].add(event.data);
        await _hive.saveAccount(baza);

      emit(PaymentSuccessState());
    } catch (e) {
      emit(PaymentErrorState(message: e.toString()));
     
    }
  }
   Future<void> _change(PaymentEvent event, Emitter<PaymentState> emit) async {
    emit(PaymentLoadingState());
    try {
       String name = await SharedPref().read("activeUser");
       List<AccountData> baza = await _hive.getAccount();
       print("EVENT:${event.data.id}");
       
        for(int i=0;i<baza.firstWhere((x) => x.name == name).data["payment"].length;i++){
          if(baza.firstWhere((x) => x.name == name).data["payment"][i]['id'] == (event.data as Payment).id){
            baza.firstWhere((x) => x.name == name).data["payment"][i] = event.data;
          }
        }

        await _hive.saveAccount(baza);

      emit(PaymentSuccessState());
    } catch (e) {
      emit(PaymentErrorState(message: e.toString()));
     
    }
  }

   Future<void> _single(PaymentEvent event, Emitter<PaymentState> emit) async {
    emit(PaymentLoadingState());
    try {
       String name = await SharedPref().read("activeUser");
       List<AccountData> baza = await _hive.getAccount();

        await _api.payment(event.data);
        event.data.setSend = true;
       
        // print("ORDER:${event.data}");
        // baza.firstWhere((x) => x.name == name).data["payment"] !=null?baza.firstWhere((x) => x.name == name).data["payment"]: baza.firstWhere((x) => x.name == name).data.addAll(map);       
        // baza.firstWhere((x) => x.name == name).data["payment"].add(event.data);
         for(int i=0;i<baza.firstWhere((x) => x.name == name).data["payment"].length;i++){
          if(baza.firstWhere((x) => x.name == name).data["payment"][i]['id'] == (event.data as Payment).id){
            baza.firstWhere((x) => x.name == name).data["payment"][i] = event.data;
          }
        }
      
        await _hive.saveAccount(baza);
    
      emit(PaymentSuccessState());
    } catch (e) {
      emit(PaymentErrorState(message: e.toString()));
      // print("ERROR:$e");
    }
  }
}
