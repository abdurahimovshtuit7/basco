part of 'payment_bloc.dart';

abstract class PaymentEvent extends Equatable {
const PaymentEvent();

  @override
  List<Object> get props => [data];
    get data => null;

}
class Paid extends PaymentEvent {
@override
  final Payment data;
const Paid({required this.data});
  @override
  List<Object> get props => [data];
}

class SavePayment extends PaymentEvent {
@override
  final dynamic data;
const SavePayment({required this.data});
  @override
  List<Object> get props => [data];
}

class ChangePayment extends PaymentEvent {
@override
  final dynamic data;
const ChangePayment({required this.data});
  @override
  List<Object> get props => [data];
}


class SinglePayment extends PaymentEvent {
@override
  final Payment data;
const SinglePayment({required this.data});
  @override
  List<Object> get props => [data];
}
