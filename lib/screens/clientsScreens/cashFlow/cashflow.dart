import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/dialog/custom_dialogs.dart';
import 'package:basco/dialog/custom_loading_dialog.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/payment.dart';
import 'package:basco/models/user.dart';
import 'package:basco/models/user_client.dart';
import 'package:basco/routes.dart';
import 'package:basco/screens/clientsScreens/cashFlow/bloc/payment_bloc.dart';
import 'package:basco/screens/clientsScreens/sale/bloc/sale_bloc.dart';
import 'package:basco/utils/services.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';

class CashFlow extends StatefulWidget {
  const CashFlow({Key? key}) : super(key: key);
  static const String routeName = 'cashflow';

  @override
  _CashFlowState createState() => _CashFlowState();
}


class _CashFlowState extends State<CashFlow> {
  late final UserHive _hive = di.get<UserHive>();
  TextEditingController comment = TextEditingController();
  TextEditingController summa = TextEditingController();
  TextEditingController rateController = TextEditingController();

  TextEditingController clientSum = TextEditingController();
  late final _lastDate = TextEditingController(
      text: DateFormat("dd.MM.yyyy").format(DateTime.now()));



  final _bloc = PaymentBloc();
  late dynamic args;
  Map<String, dynamic> data = {};
  List currencyList = [];

  dynamic currency;
  dynamic cashCurrency;
  dynamic userId;
  dynamic sinxTime = '';
  String clientName = '';
  dynamic selectedClient;
  dynamic cash;

  @override
  void initState() {
    super.initState();

    summa.addListener(() {
      setState(() {});
    });
    rateController.addListener(() {
      setState(() {});
    });

    getHive();
  }

  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    setState(() {
      data = accountData.data;
      userId = data["user"] != null ? User.fromJson(data['user']).userId : '';
      sinxTime = data["clients"] != null
          ? (UserClient.fromJson(data['clients']).sinxronVaqti)
          : '';
      currencyList = accountData.data["user"] != null
          ? User.fromJson(accountData.data['user']).valyutalar
          : [];
    });
  }

  @override
  void didChangeDependencies() {
    setState(() {
      args = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
    });
    setState(() {
      clientName = args["data"] != null ? args["data"].mijozName : "";
    });

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    String rate = '0';
    rate = rateController.text;
    print("RATE:$rate");
    // print(NumberFormat().parse(summa.text));
    // clientSum.text = Services.currencyFormatter(
    //     ((double.parse(rate.isNotEmpty ? rate.replaceAll(",", '') : "0")) *
    //            double.parse(summa.text.isNotEmpty ? summa.text.replaceAll(",", '') : "0"))
    //         .toString());
      
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        body: BackgroundView(
          child: Column(
            children: [
              CustomAppBar(title: "Мижоздан пул кирими", rightMenu: false),
              Expanded(
                  child: SingleChildScrollView(
                child: BlocConsumer<PaymentBloc, PaymentState>(
                  listener: (context, state) {
                    CustomLoadingDialog.hide(context);
                    if (state is PaymentLoadingState) {
                      CustomLoadingDialog.show(context);
                    } else if (state is PaymentSuccessState) {
                      Navigator.pop(context);
                      DialogUtils.showLangDialog(context,
                          okBtnFunction: () {},
                          text: "Маълумотлар муваффиқиятли кўчирилди.");
                    } else if (state is PaymentErrorState) {
                      DialogUtils.showLangDialog(context,
                          okBtnFunction: () {}, text: state.message);
                      // showSnackBar(state.message);
                    }
                  },
                  builder: (context, state) {
                    return Column(
                      children: [
                        Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: 8, vertical: 10),
                          child: Row(
                            children: [
                              Touchable("Серверга юбориш", () => sale(0)),
                              Touchable("Сақлаш ва чиқиш", () => sale(1)),
                            ],
                          ),
                        ),
                        Container(
                          margin:
                              EdgeInsets.symmetric(horizontal: 12, vertical: 5),
                          padding: EdgeInsets.symmetric(
                              horizontal: 19, vertical: 10),
                          decoration: BoxDecoration(
                              color: AppColor.black.withOpacity(0.5),
                              borderRadius: BorderRadius.circular(16)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              // TextRow(
                              //   text1: "Номер",
                              //   text2: "",
                              //   color: AppColor.brandColor,
                              // ),
                              // divider(),
                              Row(
                                children: [
                                  Text(
                                    "Ҳужжат вақти",
                                    style:
                                        ligthTextStyle.copyWith(fontSize: 16),
                                  ),
                                  Flexible(
                                    child: CustomDateTime(
                                        label: 'гача:',
                                        value: _lastDate,
                                        dateTimePicker: _toDate,
                                        minimunDate: sinxTime,
                                        length: 0),
                                  ),
                                ],
                              ),
                              divider(),
                              TouchedRow(
                                text1: "Мижоз",
                                text2: clientName,
                                onPress: () async {
                                  if (args["client"])
                                    print("false");
                                  else {
                                    FocusScope.of(context).unfocus();
                                    dynamic selected =
                                        await Navigator.pushNamed(
                                            context, Routes.selectClient);

                                    if (selected != null) {
                                      setState(() {
                                        clientName = selected.mijozName;
                                        selectedClient = selected;
                                      });
                                    }
                                  }
                                },
                                color: AppColor.brandColor,
                              ),
                              divider(),
                              TouchedRow(
                                text1: "Касса",
                                text2: cash != null ? cash.kassa : "",
                                onPress: () async {
                                  FocusScope.of(context).unfocus();

                                  dynamic cashData = await Navigator.pushNamed(
                                      context, Routes.cash);

                                  if (cashData != null) {
                                    setState(() {
                                      cash = cashData;
                                    });
                                  }
                                },
                              ),
                              divider(),
                              TouchedRow(
                                text1: "Касса валютаси",
                                text2: cashCurrency != null
                                    ? cashCurrency.valyutaNomi
                                    : "",
                                onPress: () async {
                                  FocusScope.of(context).unfocus();

                                  var currencyData = await Navigator.pushNamed(
                                      context, Routes.currency);
                                  String rate = '0';
                                  setState(() {
                                    cashCurrency = currencyData;
                                    currency = currencyData;
                                  });

                                  if (cashCurrency != null &&
                                      currency != null) {
                                    rate = exchangeRate(cashCurrency.valyutaId,
                                            currency.valyutaId)
                                        .toStringAsFixed(6);
                                  }
                                  rateController.text = rate;
                                  clientSum.text = Services.currencyFormatter(
        ((double.parse(rate.isNotEmpty ? rate.replaceAll(",", '') : "0")) *
               double.parse(summa.text.isNotEmpty ? summa.text.replaceAll(",", '') : "0"))
            .toString());
                                },
                              ),
                              divider(),
                              TextInputWithTitle(
                                text1: "Кирим суммаси",
                                value: summa,
                                shadow: false,
                                keyboardType: TextInputType.number,
                                validator: (value) {
                                  return null;
                                },
                                onSubmit: (){
                                   clientSum.text = Services.currencyFormatter(
        ((double.parse(rate.isNotEmpty ? rate.replaceAll(",", '') : "0")) *
               double.parse(summa.text.isNotEmpty ? summa.text.replaceAll(",", '') : "0"))
            .toString());
                                },
                              ),
                              divider(),
                              ((rate == "1.0000") ||
                                      (rate == "1.00") ||
                                      ((currency != null
                                              ? currency.valyutaId
                                              : 0) ==
                                          (cashCurrency != null
                                              ? cashCurrency.valyutaId
                                              : 1)))
                                  ? TextRow(text1: "Курс", text2: rate)
                                  : TextInputWithTitle(
                                      text1: "Курс",
                                      value: rateController,
                                      shadow: false,
                                      keyboardType: TextInputType.number,
                                      validator: (value) {
                                        return null;
                                      },
                                      mantissa: 6,
                                      onSubmit: (){
                                        clientSum.text = Services.currencyFormatter(
        ((double.parse(rate.isNotEmpty ? rate.replaceAll(",", '') : "0")) *
               double.parse(summa.text.isNotEmpty ? summa.text.replaceAll(",", '') : "0"))
            .toString());
                                      },
                                    ),
                             
                              divider(),
                              TouchedRow(
                                text1: "Мижоз валютаси",
                                text2: currency != null
                                    ? currency.valyutaNomi
                                    : "",
                                onPress: () async {
                                  FocusScope.of(context).unfocus();
                                  var currencyData = await Navigator.pushNamed(
                                      context, Routes.currency);
                                  String rate = "0";
                                  setState(() {
                                    currency = currencyData;
                                  });
                                  if (cashCurrency != null &&
                                      currency != null) {
                                    rate = exchangeRate(cashCurrency.valyutaId,
                                            currency.valyutaId)
                                        .toStringAsFixed(6);
                                  }
                                  rateController.text = rate;
                                  clientSum.text = Services.currencyFormatter(
        ((double.parse(rate.isNotEmpty ? rate.replaceAll(",", '') : "0")) *
               double.parse(summa.text.isNotEmpty ? summa.text.replaceAll(",", '') : "0"))
            .toString());
                                },
                              ),
                              divider(),
                              TextInputWithTitle(
                                text1: "Мижоз суммаси",
                                value: clientSum,
                                shadow: false,
                                keyboardType: TextInputType.number,
                                validator: (value) {
                                  return null;
                                },
                                onSubmit: (){
                                  rateController.text = (double.parse(clientSum.text.isNotEmpty?clientSum.text.replaceAll(",", ''):"0")/double.parse(summa.text.isNotEmpty ? summa.text.replaceAll(",", '') : "1")).toStringAsFixed(6);
                                },
                              ),
                              divider(),
                              Text("Изох",
                                  style: ligthTextStyle.copyWith(fontSize: 16)),
                              TextFormField(
                                controller: comment,
                                maxLines: 3,
                                cursorColor: AppColor.brandColor2,
                                style: whiteText,
                                decoration: InputDecoration(
                                  border: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          style: BorderStyle.none, width: 0)),
                                ),
                              ),
                        
                            ],
                          ),
                        )
                      ],
                    );
                  },
                ),
              ))
            ],
          ),
        ),
      ),
    );
  }

  onPress(routeName) {
    Navigator.pushNamed(context, routeName);
  }

  divider() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 4),
      child: Divider(color: AppColor.white),
    );
  }

  Touchable(title, ontap) {
    return Expanded(
      child: Material(
        color: AppColor.transparent,
        child: InkWell(
          onTap: () {
            ontap();
          },
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 5),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      AppColor.brandColor,
                      AppColor.brandColor2,
                      AppColor.brandColor
                    ]),
                boxShadow: [
                  BoxShadow(
                      color: AppColor.black.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 5)
                ],
                borderRadius: BorderRadius.circular(10)),
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: whiteText.copyWith(fontSize: 15),
            ),
          ),
        ),
      ),
    );
  }

  void sale(id) {
    if ((currency != null) &&
        (clientName.isNotEmpty) &&
        (cash != null) &&
        (cashCurrency != null) &&
        (summa.text.isNotEmpty) &&
        (clientSum.text.isNotEmpty)) {
      data = {
        "user_id": userId,
        "mijoz_id": selectedClient != null
            ? selectedClient.mijozId
            : (args["data"] != null ? args["data"].mijozId : ""),
        "valyuta_id": currency?.valyutaId ?? "",
        "kassa_id": cash.kassaId,
        "kassa_valyuta_id": cashCurrency.valyutaId,
        "kirim_summasi":
            NumberFormat("###,##0.00#", 'en_US').parse(summa.text).toString(),
        "mijoz_valyuta_id": currency.valyutaId,
        "mijoz_summasi": NumberFormat("###,##0.00#", 'en_US')
            .parse(clientSum.text)
            .toString(),
        "sinxron_time": DateFormat("yyyyMMddhhmmss").format(sinxTime),
        "izox": comment.text,
        "send": false,
        "id": DateTime.now().millisecondsSinceEpoch,
        "date": DateFormat("dd.MM.yyyy").parse(_lastDate.text).toString(),
      };
      Payment order = Payment.fromJson(data);
      print("TIME:${data["sinxron_time"]}");
      if (id == 0) {
        _bloc.add(Paid(data: order));
      } else if (id == 1) {
        _bloc.add(SavePayment(data: order));
      }
    } else {
      DialogUtils.showLangDialog(context,
          okBtnFunction: () {},
          text: "Мижоз,валюта,кирим суммаси,касса майдонларини  тўлдиринг.");
    }
  }

  double exchangeRate(valId1, valId2) {
    num a = currencyList.where((x) => x.valyutaId == valId1).single.valyutaKursi
        as num;
    num b = currencyList.where((x) => x.valyutaId == valId2).single.valyutaKursi
        as num;
    if (clientSum.text != '') {
      double k = (double.tryParse(clientSum.text) ?? 0) /
          (double.tryParse(summa.text) ?? 0);
      print("K:$k");
    }
    if (a == 0) {
      return 0;
    } else
      return (b / a);
  }

  void _toDate(time) {
    setState(() {
      _lastDate.text = DateFormat("dd.MM.yyyy").format(time);
    });
  }
}
