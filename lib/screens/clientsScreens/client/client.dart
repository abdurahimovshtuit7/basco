import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/dialog/custom_dialogs.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/currency.dart';
import 'package:basco/models/location.dart';
import 'package:basco/models/orders.dart';
import 'package:basco/models/payment.dart';
import 'package:basco/models/user.dart';
import 'package:basco/models/user_client.dart';
import 'package:basco/utils/services.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:collection/collection.dart';

import '/../../routes.dart';

class Client extends StatefulWidget {
  const Client({Key? key}) : super(key: key);
  static const String routeName = 'client';
  @override
  _ClientState createState() => _ClientState();
}

class _ClientState extends State<Client> {
  late dynamic args;
  late dynamic clients = [];
  late final UserHive _hive = di.get<UserHive>();
  Map<String, dynamic> data = {};
  List<Payment> payments = [];
  List<Orders> trades = [];
  List currency = [];
  String userId = '';
  List location = [];

  @override
  void initState() {
    super.initState();
    getHive();
  }

  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    setState(() {
      data = accountData.data;

      payments = data["payment"] != null
          ? (List<Payment>.from(
              data["payment"].map((x) => Payment.fromJson(x))))
          : [];
      trades = data["trade"] != null
          ? (List<Orders>.from(data["trade"].map((x) => Orders.fromJson(x))))
          : [];
      currency =
          data["user"] != null ? User.fromJson(data['user']).valyutalar : [];

      userId = data["user"] != null ? User.fromJson(data['user']).userId : '';
      location = data["location"] != null
          ? (List<ClientLocation>.from(
              data["location"].map((x) => ClientLocation.fromJson(x))))
          : [];
    });
  }

  @override
  void didChangeDependencies() {
    setState(() {
      args = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
      clients = args["data"] != null ? UserClient.fromJson(args["data"]) : [];
    });

    // print("args:${args['item']}");
    super.didChangeDependencies();
  }

  Future<void> _makePhoneCall(String url) async {
    if (await canLaunch('tel:$url')) {
      await launch('tel:$url');
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    // print("LOCATION:${(location[0] as ClientLocation).longtitude}");//69.28310036659241

    final item = args["item"];
    final lat = location.firstWhereOrNull((e) => e.mijozId == item.mijozId) !=
            null
        ? location.firstWhereOrNull((e) => e.mijozId == item.mijozId).latitude
        : '';
    final long = location.firstWhereOrNull((e) => e.mijozId == item.mijozId) !=
            null
        ? location.firstWhereOrNull((e) => e.mijozId == item.mijozId).longtitude
        : '';
    final List billingHistory = clients.mijozOstatkalari
        .where((x) => x.mijozId == item.mijozId)
        .toList();
    final List clientTrades =
        trades.where((element) => element.mijozId == item.mijozId).toList();
    final List clientPayment =
        payments.where((element) => element.mijozId == item.mijozId).toList();

    List list = [];
    List listById = [];
    var seen = Set<String>();

    for (int k = 0; k < clientTrades.length; k++) {
      bool isExist = false;
     
      for (int i = 0; i < billingHistory.length; i++) {
        if (clientTrades[k].valyutaId == billingHistory[i].valyutaId) {
           isExist = true;
           break;
        } 
       
      }
       if(!isExist){
           listById.add(ClientCurrency(
              clientId: clientTrades[k].mijozId,
              valyutaId: clientTrades[k].valyutaId));
        }
    }
    for (int k = 0; k < clientPayment.length; k++) {
      bool isExist = false;
       for (int i = 0; i < billingHistory.length; i++) {
        if (clientPayment[k].mijozValyutaId == billingHistory[i].valyutaId) {
          isExist = true;
           break;
         
        }
        }
        if(!isExist){
           listById.add(ClientCurrency(
              clientId: clientPayment[k].mijozId,
              valyutaId: clientPayment[k].mijozValyutaId));
        }
      }

    // for (int i = 0; i < billingHistory.length; i++) {
      //       if (clientTrades.every((item) => billingHistory.contains(item))) {
      //   return true;
      // } else {
      //   return false;
      // }
      // for (int k = 0; k < clientTrades.length; k++) {
      //   if (clientTrades[k].valyutaId != billingHistory[i].valyutaId) {
      //     listById.add(ClientCurrency(
      //         clientId: clientTrades[k].mijozId,
      //         valyutaId: clientTrades[k].valyutaId));
      //   }
      // }
      // for (int k = 0; k < clientPayment.length; k++) {
      //   if (clientPayment[k].mijozValyutaId != billingHistory[i].valyutaId) {
      //     listById.add(ClientCurrency(
      //         clientId: clientPayment[k].mijozId,
      //         valyutaId: clientPayment[k].mijozValyutaId));
      //   }
      // }
      //  listById.addAll(a.map((e) => ClientCurrency(clientId: e.mijozId,
      //           valyutaId: e.valyutaId)));
    // }
  
    list = listById.where((element) => seen.add(element.valyutaId)).toList();
    print("ksdkd:${list.length}");
    for (int i = 0; i < list.length; i++) {
      print("BillingHistory:$billingHistory");
      String name = item.mijozName;//billingHistory[0].mijozName;
      String valyuta = currency
          .firstWhere((element) => element.valyutaId == list[i].valyutaId)
          .valyutaNomi;
      billingHistory.add(MijozOstatkalari(
          mijozId: list[i].clientId,
          mijozName: name,
          valyuta: valyuta,
          valyutaId: list[i].valyutaId,
          haqqimiz: 0,
          qarzimiz: 0));
    }

    final List lastOperations = clients.oxirgiOperatsiyalar
        .where((x) => x.mijozId == item.mijozId)
        .toList();
    // print("ITEM:${billingHistory}");

    return Scaffold(
      body: BackgroundView(
        child: Column(
          children: [
            CustomAppBar(title: item.mijozName,rightMenu: false),
            Expanded(
                child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 13, horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        rowButton(
                            text: "Савдо",
                            onPress: () => navigationPressed(Routes.sale,
                                data: {"data": item, "client": true})),
                        SizedBox(width: 9),
                        rowButton(
                            text: "Пул кирими",
                            onPress: () => navigationPressed(Routes.cashFlow,
                                data: {"data": item, "client": true})),
                        SizedBox(width: 9),
                        rowButton(
                            text: "АКТ сверка",
                            onPress: () => navigationPressed(Routes.aktScreen,
                                data: {"data": item})),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    decoration:
                        BoxDecoration(color: AppColor.black.withOpacity(0.5)),
                    child: Column(
                      children: [
                        TextRow(text1: "ISM:", text2: item.mijozName),
                        Divider(color: AppColor.white),
                        Material(
                          color: AppColor.transparent,
                          child: InkWell(
                            onTap: () => _makePhoneCall(item.mijozPhone),
                            child: TextRow(
                                text1: "Телефон раками :",
                                text2: item.mijozPhone,
                                color: AppColor.brandColor2),
                          ),
                        ),
                        Divider(color: AppColor.white),
                        Material(
                          color: AppColor.transparent,
                          child: InkWell(
                            onTap: () => _makePhoneCall(item.mijozPhone),
                            child: TextRow(
                                text1: "Кушимча раками :",
                                text2: item.mijozPhone,
                                color: AppColor.brandColor2),
                          ),
                        ),
                        Divider(color: AppColor.white),
                        Material(
                          color: AppColor.transparent,
                          child: InkWell(
                            onTap: () => DialogUtils.showLocationDialog(context,
                                text: "Мижоз манзили", toMap: () {
                              Navigator.pushNamed(context, Routes.mapScreen,
                                  arguments: {
                                    "name": item.mijozName,
                                    "lat": item.mijozLat == ''
                                        ? lat
                                        : item.mijozLat,
                                    "long": item.mijozLong == ''
                                        ? long
                                        : item.mijozLong
                                  });
                            }, selectLocation: () {
                              Navigator.pushNamed(
                                  context, Routes.selectLocation, arguments: {
                                "data": item.mijozId,
                                "user": userId
                              });
                            }),
                            child: TextRow(
                                text1: "Манзили :",
                                text2: item.mijozAddress,
                                color: AppColor.brandColor2),
                          ),
                        ),
                        Divider(color: AppColor.white),
                        TextRow(text1: "Нарх тури :", text2: item.narxTuri),
                      ],
                    ),
                  ),
                  SizedBox(height: 18),
                  Container(
                    color: AppColor.black.withOpacity(0.5),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              "Хисоб-китоблар колдиги",
                              style: ligthTextStyle,
                            ),
                          ),
                          buildTableTop("Валюта", "Карзимиз", "Хаккимиз"),
                          Container(
                            height: billingHistory.length * 50, //230,
                            child: MediaQuery.removePadding(
                              context: context,
                              removeTop: true,
                              child: ListView(
                                physics: const NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                children: [
                                  Table(
                                    defaultVerticalAlignment:
                                        TableCellVerticalAlignment.middle,
                                    columnWidths: {
                                      0: FlexColumnWidth(1),
                                      1: FlexColumnWidth(3),
                                      2: FlexColumnWidth(4),
                                      3: FlexColumnWidth(4),
                                    },
                                    children: billingHistory
                                        .asMap()
                                        .entries
                                        .map((element) {
                                      var index = element.key;
                                      var value = element.value;
                                      double fee = 0;
                                      double loan = 0;

                                      if (value.haqqimiz == 0 &&
                                          value.qarzimiz == 0) {
                                        if (storageLoan(item.mijozId,
                                                    value.valyutaId) -
                                                storageOrders(item.mijozId,
                                                    value.valyutaId) >
                                            0) {
                                          loan = storageLoan(item.mijozId,
                                                  value.valyutaId) -
                                              storageOrders(item.mijozId,
                                                  value.valyutaId);
                                        } else {
                                          fee = storageOrders(item.mijozId,
                                                  value.valyutaId) -
                                              storageLoan(item.mijozId,
                                                  value.valyutaId);
                                        }
                                      } else {
                                        if (value.haqqimiz == 0) {
                                          loan = value.qarzimiz +
                                              storageLoan(item.mijozId,
                                                  value.valyutaId);
                                        } else {
                                          if ((value.haqqimiz -
                                                  storageLoan(item.mijozId,
                                                      value.valyutaId)) >
                                              0) {
                                            fee = value.haqqimiz -
                                                storageLoan(item.mijozId,
                                                    value.valyutaId);
                                            loan = 0;
                                          } else {
                                            loan = (value.haqqimiz -
                                                    storageLoan(item.mijozId,
                                                        value.valyutaId))
                                                .abs();
                                            fee = 0;
                                          }
                                        }
                                        if (loan == 0) {
                                          fee = value.haqqimiz +
                                              storageOrders(item.mijozId,
                                                  value.valyutaId);
                                        } else {
                                          if ((loan -
                                                  storageOrders(item.mijozId,
                                                      value.valyutaId)) >
                                              0) {
                                            loan = loan -
                                                storageOrders(item.mijozId,
                                                    value.valyutaId);
                                            fee = 0;
                                          } else {
                                            fee = fee +
                                                (loan -
                                                        storageOrders(
                                                            item.mijozId,
                                                            value.valyutaId))
                                                    .abs();
                                            loan = 0;
                                          }
                                        }
                                      }

                                      return TableRow(
                                          decoration: BoxDecoration(
                                              color: index % 2 == 0
                                                  ? AppColor.transparent
                                                  : AppColor.transparent,
                                              border: Border(
                                                bottom: BorderSide(
                                                  color: AppColor.brandColor,
                                                  width: 1,
                                                ),
                                              )),
                                          children: [
                                            buildItem((index + 1).toString()),
                                            buildItem(value.valyuta),
                                            buildItem(
                                                Services.currencyFormatter(
                                                    loan.toString()),
                                                align: TextAlign.end),
                                            buildItem(
                                                Services.currencyFormatter(
                                                    fee.toString()),
                                                align: TextAlign.end),
                                          ]);
                                    }).toList(),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 18),
                  Container(
                    color: AppColor.black.withOpacity(0.5),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              "Охирги операциялар",
                              style: ligthTextStyle,
                            ),
                          ),
                          buildTableTop("Сана", "Изох", "Сумма"),
                          Container(
                            height: lastOperations.length * 70,
                            child: MediaQuery.removePadding(
                              context: context,
                              removeTop: true,
                              child: ListView(
                                physics: const NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                children: [
                                  Table(
                                    defaultVerticalAlignment:
                                        TableCellVerticalAlignment.middle,
                                    columnWidths: {
                                      0: FlexColumnWidth(1),
                                      1: FlexColumnWidth(3),
                                      2: FlexColumnWidth(6),
                                      3: FlexColumnWidth(4),
                                    },
                                    children: lastOperations
                                        .asMap()
                                        .entries
                                        .map((element) {
                                      var index = element.key;
                                      var value = element.value;
                                      return TableRow(
                                          decoration: BoxDecoration(
                                              color: index % 2 == 0
                                                  ? AppColor.transparent
                                                  : AppColor.transparent,
                                              border: Border(
                                                bottom: BorderSide(
                                                  color: AppColor.brandColor,
                                                  width: 1,
                                                ),
                                              )),
                                          children: [
                                            buildItem((index + 1).toString()),
                                            buildItem(
                                                DateFormat("yyyy.MM.dd hh:mm")
                                                    .format(value.sana)),
                                            buildItem(value.izoh.toString()),
                                            buildItem(
                                                "${Services.currencyFormatter(value.summa.toString())} ${value.valyuta}",
                                                align: TextAlign.end,
                                                color:value.summa.toString()[0]=='-'? AppColor.red:AppColor.white
                                                ),
                                          ]);
                                    }).toList(),
                                  ),
                                  SizedBox(height: 25)
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
               
               
                ],
              ),
            )),
          ],
        ),
      ),
    );
  }

  void navigationPressed(String routeName, {data}) {
    Navigator.pushNamed(context, routeName, arguments: data);
  }

  Widget buildItemLabel(text) {
    return Padding(
      padding: const EdgeInsets.only(left: 5, top: 15, bottom: 15),
      child: Text(
        text,
        style: TextStyle(
            fontSize: 15,
            color: AppColor.black,
            fontFamily: AppFont.MontserratBold,
            fontWeight: FontWeight.w600),
        // textAlign: TextAlign.center,
      ),
    );
  }

  Widget buildItem(text, {color = AppColor.white, align = TextAlign.start}) {
    return Padding(
      padding: const EdgeInsets.only(left: 5, top: 15, bottom: 15),
      child: SelectableText(
        text,
        style:
            TextStyle(color: color, fontSize: 12, fontWeight: FontWeight.w600),
        textAlign: align,
      ),
    );
  }

  Widget buildTableTop(title1, title2, title3) {
    return Table(
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        columnWidths: {
          0: FlexColumnWidth(1),
          1: FlexColumnWidth(4),
          2: FlexColumnWidth(4),
          3: FlexColumnWidth(4),
        },
        children: [
          TableRow(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  AppColor.brandColor,
                  AppColor.brandColor2,
                  AppColor.brandColor
                ],
              )),
              children: [
                buildItemLabel("N"),
                buildItemLabel(title1),
                buildItemLabel(title2),
                buildItemLabel(title3),
              ]),
        ]);
  }

  Widget rowButton({required String text, required Function onPress}) {
    return Expanded(
      child: InkWell(
        onTap: () => onPress(),
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 13),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: AppColor.black.withOpacity(0.7),
              boxShadow: [
                BoxShadow(
                    color: AppColor.black.withOpacity(0.5),
                    spreadRadius: 2,
                    blurRadius: 3)
              ]),
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 16,
                color: AppColor.white,
                fontFamily: AppFont.MontserratBold,
                fontWeight: FontWeight.w600,
                shadows: [
                  Shadow(
                    offset: Offset(1.0, 1.0),
                    blurRadius: 10.0,
                    color: AppColor.shadow.withOpacity(0.1),
                  ),
                  Shadow(
                    offset: Offset(1.0, 1.0),
                    blurRadius: 10.0,
                    color: AppColor.shadow.withOpacity(0.1),
                  ),
                ]),
          ),
        ),
      ),
    );
  }

  storageLoan(mijozId, valyuta) {
    print(mijozId);
    print(valyuta);
    double sum = 0;
    print(payments.map((e) => e.mijozId));
    print(payments.map((e) => e.mijozValyutaId));
    for (int i = 0; i < payments.length; i++) {
      if (payments[i].mijozId == mijozId &&
          payments[i].mijozValyutaId == valyuta) {
        sum += double.tryParse(payments[i].mijozSummasi) ?? 0;
      }
    }
    //  sum += double.tryParse(payments.where((e) => (e.mijozId == mijozId )&&(e.mijozValyutaId == valyuta)).single.mijozSummasi)??0;
    print(sum);
    return sum;
  }

  storageOrders(mijozId, valyuta) {
    dynamic newlist = groupBy(trades, (x) => (x as Orders).mijozId) ?? [];
    dynamic clientOrderList = newlist[mijozId] ?? [];
    dynamic currencyList =
        groupBy(clientOrderList, (x) => (x as Orders).valyutaId) ?? [];
    dynamic currency = currencyList[valyuta] ?? [];
    double c = 0;
    for (int i = 0; i < currency.length; i++) {
      c += double.tryParse(currency[i].total) ?? 0;
    }
    return c;
  }
  // checkClientId(mijozId,)
}
