import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';

class ClientCurrency extends StatefulWidget {
  const ClientCurrency({ Key? key }) : super(key: key);
    static const String routeName = 'client-currency';


  @override
  _ClientCurrencyState createState() => _ClientCurrencyState();
}

class _ClientCurrencyState extends State<ClientCurrency> {
  late int selectedItem = -1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BackgroundView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            CustomAppBar(title: "Мижоз валютаси"),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10, horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  
                  Text(
                    "Пул бирлиги",
                    style: TextStyle(
                        color: AppColor.white,
                        fontSize: 18,
                        fontFamily: AppFont.MontserratBold,
                        fontWeight: FontWeight.w600),
                  ),
                  Text(
                    "Код",
                    style: TextStyle(
                        color: AppColor.white,
                        fontSize: 18,
                        fontFamily: AppFont.MontserratBold,
                        fontWeight: FontWeight.w600),
                  ),
                ],
              ),
            ),
            Expanded(
              child: MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child: ListView.builder(
                  itemBuilder: (BuildContext context, index) {
                    return CheckedItem(text1: "000000001",text2: "Юан",onPress:(){setState(() {
                    selectedItem = index;
                    });},index: index+1,);

                  },
                  itemCount: 9,
                ),
              )
            )
          ],
        ),
      ),
    );
  }
}