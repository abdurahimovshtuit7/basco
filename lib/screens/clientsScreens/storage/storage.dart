import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
class Storage extends StatefulWidget {
  const Storage({ Key? key }) : super(key: key);
  static const String routeName = 'storage';

  @override
  _StorageState createState() => _StorageState();
}

class _StorageState extends State<Storage> {
  late int selectedItem = -1;
  late dynamic args;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    setState(() {
      args = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
    });
    print(args);
    super.didChangeDependencies();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BackgroundView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            CustomAppBar(title: "Омборлар"),
            // Container(
            //   margin: EdgeInsets.symmetric(vertical: 10, horizontal: 35),
            //   child: 
            //       Text(
            //         "Омбор номи",
            //         textAlign: TextAlign.center,
            //         style: TextStyle(
            //             color: AppColor.white,
            //             fontSize: 18,
            //             fontFamily: AppFont.MontserratBold,
            //             fontWeight: FontWeight.w600),
            //       ),
             
            // ),
            SizedBox(height: 10),
            Expanded(
              child: MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child: ListView.builder(
                  itemBuilder: (BuildContext context, index) {
                    dynamic value = args["storage"][index];
                    return StoreItem(text1: value.ombor,onPress:(){
                       Navigator.pop(context,value);
                    //   setState(() {
                    // selectedItem = index;
                    // });
                    },index: index,);

                  },
                  itemCount: args["storage"].length,
                ),
              )
            )
          ],
        ),
      ),
    );
  }
}