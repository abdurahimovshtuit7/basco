import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/models/revise_model.dart';
import 'package:basco/utils/services.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

import 'package:intl/intl.dart';

class ActList extends StatefulWidget {
  const ActList({Key? key}) : super(key: key);
  static const String routeName = 'act-list';

  @override
  _ActListState createState() => _ActListState();
}

class _ActListState extends State<ActList> {
  late dynamic args;
  late dynamic data;

  @override
  void didChangeDependencies() {
    setState(() {
      args = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
      data = args["data"] as ReviseModel;
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BackgroundView(
        child: Column(
          children: [
            CustomAppBar(title: data.mijozName, rightMenu: false),
            SizedBox(height: 12),
            Text("${DateFormat('dd.MM.yyy').format(data.data1)} - ${DateFormat('dd.MM.yyy').format(data.data2)}",style: ligthTextStyle.copyWith(fontSize: 18),),
            Expanded(
                child: MediaQuery.removePadding(
              context: context,
              removeTop: true,
              child: ListView.builder(
                itemBuilder: (BuildContext context, index) {
                  var value = data.malumotlar[index];
                  return item(value);
                },
                itemCount: data.malumotlar.length,
              ),
            ))
          ],
        ),
      ),
    );
  }

  item(value) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
      padding: EdgeInsets.symmetric(vertical: 15, horizontal: 13),
      decoration: BoxDecoration(
          color: AppColor.black.withOpacity(0.5),
          borderRadius: BorderRadius.circular(12)),
      child: Column(
        children: [
          TextRow(
            text1: "Валюта Номи",
            text2: value.valyuta,
            color: AppColor.white,
          ),
          diveder(),
          TextRow(
            text1: "Бош колдик",
            text2: Services.currencyFormatter(value.boshQoldiq.toString()),
            color: (Services.currencyFormatter(value.boshQoldiq.toString())[0]=='-')?AppColor.red:AppColor.white,
          ),
          diveder(),
            TextRow(
            text1: "Мижоздан кирим",
            text2: Services.currencyFormatter(value.bizgaKirim.toString()),
            color: (Services.currencyFormatter(value.bizgaKirim.toString())[0]=='-')?AppColor.red:AppColor.white,
          ),
          diveder(),
           TextRow(
            text1: "Мижозга чиким",
            text2: Services.currencyFormatter(value.bizdanChiqim.toString()),
            color:(Services.currencyFormatter(value.bizdanChiqim.toString())[0]=='-')?AppColor.red:AppColor.white,
          ),
          diveder(),
          TextRow(
            text1: "Охирги колдик",
            text2: Services.currencyFormatter(value.oxirgiQoldiq.toString()),
            color: (Services.currencyFormatter(value.oxirgiQoldiq.toString())[0]=='-')?AppColor.red:AppColor.white,
          ),
          diveder(),
         
          expansionList(value.malumotlar,value.valyuta)
        ],
      ),
    );
  }

  diveder() {
    return Divider(
      color: AppColor.white,
      thickness: 0.5,
    );
  }

  expansionList(list,valyuta) {
    return ExpandableNotifier(
        child: ScrollOnExpand(
      child: Card(
        clipBehavior: Clip.antiAlias,
        color: AppColor.transparent,
        elevation: 0,
        // shape: RoundedRectangleBorder(
        //   borderRadius: BorderRadius.circular(8.0),
        // ),
        child: Column(
          children: <Widget>[
            ExpandablePanel(
              theme: const ExpandableThemeData(
                  headerAlignment: ExpandablePanelHeaderAlignment.center,
                  tapBodyToExpand: false,
                  tapBodyToCollapse: false,
                  tapHeaderToExpand: true,
                  hasIcon: false,
                  useInkWell: true),
              header: Row(
                children: [
                  Expanded(
                      child: Text(
                    "Давр ичида айданма расшифровкаси",
                    style: ligthTextStyle.copyWith(fontSize: 15),
                  )),
                  ExpandableIcon(
                    theme: const ExpandableThemeData(
                      expandIcon: Icons.keyboard_arrow_down_rounded,
                      collapseIcon: Icons.keyboard_arrow_up_rounded,
                      iconColor: Colors.white,
                      iconSize: 30.0,
                      iconRotationAngle: math.pi,
                      hasIcon: false,
                    ),
                  ),
                ],
              ),
              collapsed: Container(),
              expanded: buildList(list,valyuta),
            ),
          ],
        ),
      ),
    ));
  }

  buildList(list,valyuta) {
    return Container(
      width: double.infinity,
      child: SingleChildScrollView(
        child: Column(
          children: [...list.asMap().entries.map((element) {
            dynamic key = element.key;
            dynamic value = element.value;
            return expansionListItem(value,valyuta);
          }),]
        ),
      ),
    );
  }

  expansionListItem(value,valyuta){
     return Padding(
       padding: const EdgeInsets.symmetric(vertical:8.0),
       child: ExpandableNotifier(
          child: ScrollOnExpand(
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 5),
          decoration: BoxDecoration(
          color: AppColor.black.withOpacity(0.3),
          borderRadius: BorderRadius.circular(10),
        ),
          child: Column(
            children: <Widget>[
              ExpandablePanel(
                theme: const ExpandableThemeData(
                    headerAlignment: ExpandablePanelHeaderAlignment.center,
                    tapBodyToExpand: false,
                    tapBodyToCollapse: false,
                    tapHeaderToExpand: true,
                    hasIcon: false,
                    useInkWell: true),
                header: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                    AppColor.yellow2.withOpacity(0.9),
                    AppColor.yellow,
                    AppColor.yellow2.withOpacity(0.9)
                  ])),
                  padding: EdgeInsets.only(left:10),
                  child: Row(
                    children: [
                      Expanded(
                          child: Text(
                        value.izox,
                        maxLines: 1,
                        style: blackColor.copyWith( overflow: TextOverflow.ellipsis),
                         
                      )),
                      Expanded(
                        child: Container(child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                         Text("${Services.currencyFormatter(value.summa.toString())} $valyuta",style: blackColor.copyWith(fontSize: 12,color: Services.currencyFormatter(value.summa.toString())[0]=='-'?AppColor.red:AppColor.black),),
                         Text(DateFormat("dd.MM.yyyy").format(value.data),style:blackColor.copyWith(fontSize: 12) ,),
                        ],),),
                      ),
                      ExpandableIcon(
                        theme: const ExpandableThemeData(
                          expandIcon: Icons.keyboard_arrow_down_rounded,
                          collapseIcon: Icons.keyboard_arrow_up_rounded,
                          iconColor: Colors.white,
                          iconSize: 30.0,
                          iconRotationAngle: math.pi,
                          hasIcon: false,
                        ),
                      ),
                    ],
                  ),
                ),
                collapsed: Container(),
                expanded: Container(
                  padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
                  child: Text(value.izox,style: whiteText.copyWith(fontSize: 14),),),
              ),
            ],
          ),
        ),
    )),
     );
  }
}
