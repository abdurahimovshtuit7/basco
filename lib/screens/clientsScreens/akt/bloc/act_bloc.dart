import 'package:basco/di/locator.dart';
import 'package:basco/models/revise_model.dart';
import 'package:basco/repositories/services_repositories.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'act_event.dart';
part 'act_state.dart';

class ActBloc extends Bloc<ActEvent, ActState> {
  final ServicesRepository _api = di.get();

  ActBloc() : super(ActInitial()) {
    on<ActEvent>((event, emit) async {
      if (event is Revise) {
        await _login(event, emit);
      }
    });
  }

  Future<void> _login(
      ActEvent event, Emitter<ActState> emit) async {
        emit(ActLoadingState());
        try {
         dynamic res = await _api.revise(event.fromDate,event.toDate,event.userId,event.clientId);
         emit(ActSuccessState(reviseData: res));
        } catch (e){
          emit(ActErrorState(message: e.toString()));
          // print("ERROR:$e");
        }
      }
}
