part of 'act_bloc.dart';

abstract class ActState extends Equatable {
  const ActState();
  
  @override
  List<Object> get props => [];
}

class ActInitial extends ActState {}

class ActLoadingState extends ActState {}

class ActSuccessState extends ActState {
  final dynamic reviseData;
  const ActSuccessState({required this.reviseData});
   @override
  List<Object> get props => [reviseData];
}

class ActErrorState extends ActState {
final String message;

   ActErrorState({required this.message});

  @override
  List<Object> get props => [message];
}