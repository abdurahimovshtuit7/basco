part of 'act_bloc.dart';

abstract class ActEvent extends Equatable {
  const ActEvent();

  @override
  List<Object> get props => [fromDate,toDate,clientId,userId];

  get fromDate => null;
  get toDate => null;
  get clientId => null;
  get userId => null;
}



class Revise extends ActEvent {
final String fromDate,toDate,clientId,userId;
const Revise({required this.fromDate,required this.toDate,required this.clientId,required this.userId});
  @override
  List<Object> get props => [fromDate,toDate,clientId,userId];
}