import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/screens/clientsScreens/clients/clients.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';

class Panel extends StatefulWidget {
  final List clients;
  final TextEditingController value;
  final Function onChange;
  final Function onSelect;

  Panel({Key? key,required this.clients, required this.value,required this.onChange,required this.onSelect}) : super(key: key);

  @override
  State<Panel> createState() => _PanelState();
}

class _PanelState extends State<Panel> {
  ExpandableController _controller =
      ExpandableController(initialExpanded: false);

  @override
  Widget build(BuildContext context) {
    print("object:${widget.clients}");
    _toggleExpandables(int index) {
      setState(() {
        _controller.value = !_controller.value;

        // _getController(index).value = true;
      });
    }

    buildItem(String label,item) {
      return Material(
        color: AppColor.transparent,
        child: InkWell(
          onTap: (){
            widget.onSelect(item);
            _toggleExpandables(0);
          },
          child: Container(
            width: double.infinity,
            // color: AppColor.red,
            padding: const EdgeInsets.all(10.0),
            child: Text(
              label,
              style: whiteText,
            ),
          ),
        ),
      );
    }

    buildList() {
      return Container(
        height: 300,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              for (var i in widget.clients)
                buildItem("${i.mijozName}",i),
            ],
          ),
        ),
      );
    }

    return ExpandableNotifier(
        // controller: _controller,
        child: ScrollOnExpand(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical:8.0),
            child: Card(
              clipBehavior: Clip.antiAlias,
              color: AppColor.black.withOpacity(0.3),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: Column(
                children: <Widget>[
                  ExpandablePanel(
                    controller: _controller,
                    theme: const ExpandableThemeData(
                        headerAlignment: ExpandablePanelHeaderAlignment.center,
                        tapBodyToExpand: false,
                        tapBodyToCollapse: false,
                        tapHeaderToExpand: false,
                        hasIcon: false,
                        useInkWell: true),
                    header: Container(
                      decoration: BoxDecoration(
                          color: AppColor.black.withOpacity(0.4),
                          borderRadius: BorderRadius.circular(8)),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10.0, vertical: 3),
                        child: Row(
                          children: [
                            SvgPicture.asset(
                              Images.account,
                              height: 20,
                            ),
                            SizedBox(width: 8),
                            Flexible(
                                child: TextFormField(
                                
                              onTap: () {
                                _toggleExpandables(0);
                              },
                              onChanged: (value)=>{widget.onChange(value)},
                              controller: widget.value,
                              style: whiteText,
                              
                              cursorColor: AppColor.brandColor2,
                              decoration: InputDecoration(
                                hintText: "Мижозлар",
                                hintStyle: whiteText.copyWith(color: AppColor.gray),
                                border: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        style: BorderStyle.none, width: 0)),
                              ),
                            )),
                            SizedBox(width: 8),
                            Material(
                              color: AppColor.transparent,
                              child: InkWell(
                                onTap: () {
                                  print("object");
                                  _toggleExpandables(0);
                                },
                                child: ExpandableIcon(
                                  theme: const ExpandableThemeData(
                                    expandIcon: Icons.chevron_right,
                                    collapseIcon: Icons.keyboard_arrow_down_rounded,
                                    iconColor: Colors.white,
                                    iconSize: 30.0,
                                    iconRotationAngle: math.pi / 2,
                                    // iconPadding: EdgeInsets.only(right: 5),
                                    hasIcon: false,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    collapsed: Container(),
                    expanded: buildList(),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
