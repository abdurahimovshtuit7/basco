import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/dialog/custom_loading_dialog.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/user.dart';
import 'package:basco/models/user_client.dart';
import 'package:basco/routes.dart';
import 'package:basco/screens/clientsScreens/akt/bloc/act_bloc.dart';
import 'package:basco/screens/clientsScreens/akt/panel.dart';
import 'package:basco/utils/function.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';

class AktScreen extends StatefulWidget {
  const AktScreen({Key? key}) : super(key: key);
  static const String routeName = 'akt';

  @override
  _AktScreenState createState() => _AktScreenState();
}

class _AktScreenState extends State<AktScreen> {
  late final _bloc = ActBloc();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late final _firstDate =
      TextEditingController(text: DateFormat.yMMMd().format(DateTime.now()));
  late final _lastDate =
      TextEditingController(text: DateFormat.yMMMd().format(DateTime.now()));
  late final _search = TextEditingController();
  String text = '';
  late final UserHive _hive = di.get<UserHive>();
  Map<String, dynamic> data = {};

  String selectedClientId = '';
  String userId = '';
  dynamic client;
  dynamic args;
  @override
  void initState() {
    super.initState();
    getHive();
  }

  Future<void> getHive() async {
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    setState(() {
      data = accountData.data;
      userId = data["user"] != null ? (User.fromJson(data["user"]).userId) : '';
    });
  }

  @override
  void didChangeDependencies() {
    setState(() {
      args = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
      // client = args["data"] != null ?  : {};
      _search.text = args["data"] != null ? args["data"].mijozName : '';
      selectedClientId = args["data"] != null ? args["data"].mijozId : "";
    });

    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final clientsList = data["clients"] != null
        ? (UserClient.fromJson(data["clients"]).mijozlar)
        : [];
    List clients = clientsList
        .where((element) =>
            (element.mijozName.toLowerCase()).contains(text.toLowerCase()))
        .toList();

    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        body: BackgroundView(
          child: Column(
            children: [
              CustomAppBar(
                title: "Акт сверка",
                rightMenu: false,
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: BlocConsumer<ActBloc, ActState>(
                    listener: (context, state) {
                      CustomLoadingDialog.hide(context);
                      if (state is ActLoadingState) {
                        CustomLoadingDialog.show(context);
                      } else if (state is ActSuccessState) {
                        Navigator.pushNamed(context, Routes.actList,
                            arguments: {"data": state.reviseData});
                      } else if (state is ActErrorState) {
                        showSnackBar(state.message);
                      }
                    },
                    builder: (context, state) {
                      return Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.symmetric(
                              horizontal: 13,
                            ),
                            child: Form(
                                key: _formKey,
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    Panel(
                                      clients: clients,
                                      value: _search,
                                      onChange: (value) => {
                                        setState(() {
                                          text = value;
                                        })
                                      },
                                      onSelect: (item) => {
                                        setState(() {
                                          _search.text = item.mijozName;
                                          selectedClientId = item.mijozId;
                                        })
                                      },
                                    ),

                                    Row(children: [
                                      Flexible(
                                        child: DateTimeInput(
                                          label: 'дан:',
                                          value: _firstDate,
                                          dateTimePicker: _fromDate,
                                        ),
                                      ),
                                      SizedBox(width: 5),
                                      Flexible(
                                        child: DateTimeInput(
                                          label: 'гача:',
                                          value: _lastDate,
                                          dateTimePicker: _toDate,
                                          minimunDate: DateFormat.yMMMd()
                                              .parse(_firstDate.text),
                                        ),
                                      ),
                                    ]),
                                    // SizedBox(height: 10),
                                    Button(
                                        title: "Акт сверкасини олиш",
                                        onPress: () {
                                          //  print(selectedClientId);
                                          //  print(userId);
                                          //  print(dateFormatter(_firstDate.text));
                                          _bloc.add(Revise(
                                              fromDate: dateFormatter(
                                                  _firstDate.text, 0),
                                              toDate: dateFormatter(
                                                  _lastDate.text, 1),
                                              clientId: selectedClientId,
                                              userId: userId));

                                          // DateFormat("yyMMddHHmm").format(_firstDate.text)
                                        })
                                  ],
                                )),
                          ),
                        ],
                      );
                    },
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _fromDate(time) {
    setState(() {
      _firstDate.text = DateFormat.yMMMd().format(time);
    });

    if (DateFormat.yMMMd()
        .parse(_firstDate.text)
        .isAfter(DateFormat.yMMMd().parse(_lastDate.text))) {
      print("DATTTTTTE:");
      setState(() {
        _lastDate.text = DateFormat.yMMMd().format(time);
      });
    }
  }

  void _toDate(time) {
    setState(() {
      _lastDate.text = DateFormat.yMMMd().format(time);
    });
  }

  String dateFormatter(time, id) {
    if (id == 0) {
      var dateTime1 = DateFormat.yMMMd()
          .parse(time);
          
      return DateFormat("yyyyMMdd"+"000000").format(dateTime1);
    } else {
      var dateTime2 = DateFormat.yMMMd().parse(time);
     
      return DateFormat("yyyyMMdd"+"235900").format(dateTime2);
    }
  }
}
