import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/orders.dart';
import 'package:basco/models/user_base.dart';
import 'package:basco/models/user_client.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:collection/collection.dart';

class Character extends StatefulWidget {
  const Character({Key? key}) : super(key: key);
  static const String routeName = "character";

  @override
  _CharacterState createState() => _CharacterState();
}

class _CharacterState extends State<Character> {
  late final UserHive _hive = di.get<UserHive>();
  Map<String, dynamic> data = {};
  dynamic args;
  dynamic character;
  String omborId = '';

  List selectedList = [];
  List clients = [];
  List products = [];
  List<Orders> trades = [];

  @override
  void initState() {
    super.initState();
    getHive();
  }

  Future<void> getHive() async {
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);

    setState(() {
      data = accountData.data;
      trades = data["trade"] != null
          ? (List<Orders>.from(data["trade"].map((x) => Orders.fromJson(x))))
          : [];
    });
  }

  @override
  void didChangeDependencies() {
    setState(() {
      args = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
      character = args["data"];
      omborId = args["omborId"] ?? '';
      products = args["products"]??[];
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {

    print("PROD:$products");
    return Scaffold(
      body: BackgroundView(
        child: Column(
          children: [
            CustomAppBar(title: "Характеристикаси", rightMenu: false),
            Expanded(
              child: MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child: ListView.builder(
                  itemBuilder: (BuildContext context, index) {
                    var value = character[index];
                    Tovarlar res = Tovarlar(
                      tovarId: value.tovarId,
                      tovarName: value.tovarName,
                      xarakId: value.xarakId,
                      xarakName: value.xarakName,
                      tovarSoni: value.tovarSoni -
                          count(omborId, value.tovarId, value.xarakId)-countProduct(value.tovarId, value.xarakId),
                      summaSum: value.summaSum,
                      summaVal: value.summaVal,
                      tannarxSum: value.tannarxSum,
                      tannarxVal: value.tannarxVal,
                      omborId: value.omborId,
                    );

                    return InkWell(
                      onTap: () {
                        Navigator.pop(context, res);
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                        margin:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 12),
                        decoration: BoxDecoration(
                            color: AppColor.black.withOpacity(0.5),
                            borderRadius: BorderRadius.circular(10)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              value.xarakName.isEmpty
                                  ? "Стандарт"
                                  : value.xarakName,
                              style: whiteText,
                            ),
                            Text(
                              (value.tovarSoni -
                                      count(omborId, value.tovarId,
                                          value.xarakId)-countProduct(value.tovarId, value.xarakId))
                                  .toString(),
                              style: whiteText,
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                  itemCount: character.length,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
  double countProduct(tovarId, xarakId){
    double sum =0.0;
    if(products!=[]){
    sum += products!=[]?(products.firstWhereOrNull((e) => e["character"].xarakId == xarakId && e["product"].tovarId == tovarId)!=null?products.firstWhereOrNull((e) => e["character"].xarakId == xarakId && e["product"].tovarId == tovarId)["count"]:0):0;

    }
    return sum;
  }
  double count(omborId, tovarId, xarakId) {
    dynamic newlist = groupBy(trades, (x) => (x as Orders).omborId)??[];
    dynamic omborList = newlist[omborId]??[];
    // dynamic tovarlar =  groupBy(newlist[omborId],(e)=>(e as Orders).);
    print(omborList);
    dynamic productOf = [];
    dynamic negativeProduct = [];
    // omborList.map((e) => productOf.addAll(e.ruyxat)).toList();
    for(int i =0;i<omborList.length;i++){
      if(!omborList[i].vozvrat){
        productOf.addAll(omborList[i].ruyxat);
      }
    }
    for(int i =0;i<omborList.length;i++){
      if(omborList[i].vozvrat){
        negativeProduct.addAll(omborList[i].ruyxat);
      }
    }
 
    print(productOf);
    double c = 0.0;
    for (int i = 0; i < productOf.length; i++) {
      if ((productOf[i].tovarId == tovarId) &&
          (productOf[i].xarakId == xarakId)) {
        c += double.tryParse(productOf[i].soni.toString()) ?? 0;
      }
    }
    for(int i=0;i<negativeProduct.length;i++){
        if ((negativeProduct[i].tovarId == tovarId) &&
          (negativeProduct[i].xarakId == xarakId)) {
             c -= double.tryParse(negativeProduct[i].soni.toString()) ?? 0;
          }
    }

    return c;
  }
}
