import 'package:basco/constants/colors.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/dialog/custom_dialogs.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/user.dart';
import 'package:basco/models/user_base.dart';
import 'package:basco/routes.dart';
import 'package:basco/utils/services.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
import "package:collection/collection.dart";
import 'package:intl/intl.dart';

class EditProduct extends StatefulWidget {
  const EditProduct({Key? key}) : super(key: key);
  static const String routeName = 'edit-product';

  @override
  _EditProductState createState() => _EditProductState();
}

class _EditProductState extends State<EditProduct> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late final summa = TextEditingController();
  late final counter = TextEditingController(text: "0");
  late final UserHive _hive = di.get<UserHive>();

  late dynamic args;
  late dynamic data = '';
  late dynamic dataAccount;
  late List products = [];
  late List sellingPrice = [];

  dynamic product;
  dynamic character;
  String dollarId = '';
  String sumId = '';
  bool changeSum = false;
  double lowPrice = 0;
  double highPrice = 0;

  @override
  void initState() {
    super.initState();
    counter.addListener(() {
      setState(() {});
    });
    summa.addListener(() {
      setState(() {});
    });
  }

  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    setState(() {
      dataAccount = accountData.data;
      products = dataAccount["user_base"] != null
          ? UserBase.fromJson(dataAccount['user_base'])
              .response
              .firstWhere((element) => element.omborId == data)
              .tovarlar
          : [];
      sellingPrice = dataAccount["user_base"] != null
          ? UserBase.fromJson(dataAccount['user_base']).sotishNarxlari
          : [];

      dollarId = dataAccount["user"] != null
          ? User.fromJson(dataAccount["user"]).dollarId
          : "";
      sumId = dataAccount["user"] != null
          ? User.fromJson(dataAccount["user"]).sumId
          : "";
      changeSum = dataAccount["user"] != null
          ? User.fromJson(dataAccount["user"]).narxUzgartirish
          : false;
    });
  }

  @override
  void didChangeDependencies() {
    setState(() {
      args = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
      data = args["store"];
      character = args["item"]["character"];
      counter.text = args["item"]["count"].toString();
      summa.text = args["item"]["price"].toString();
      product = args["item"]["product"];
      // productName = args["item"].tovarName;
    });
    getHive();
    print("NAME:${args["item"]}");

    super.didChangeDependencies();
  }

  @override
  void deactivate() {
    FocusScope.of(context).unfocus();
  }

  @override
  Widget build(BuildContext context) {
    String productName = product != null ? product.tovarName : "";
    dynamic newlist = groupBy(products, (x) => (x as Tovarlar).tovarId);

    dynamic total;
    final n = double.tryParse(counter.text) ?? 0;
    final price = NumberFormat("###,##0.00#", 'en_US')
        .parse(summa.text.isNotEmpty ? summa.text : "0");

    total = (price * n).toStringAsFixed(3);

    return WillPopScope(
      onWillPop: () async {
        // Do something here
        Navigator.pop(context, true);

        return true;
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: BackgroundView(
          child: Column(
            children: [
              BubbleAppBar(
                  title: "Саклаш",
                  onpress: () {
                    double count = double.tryParse(counter.text) ?? 0;
                    if (_formKey.currentState!.validate()) {
                      if (count > 0) {
                        data = {
                          "product": product,
                          "count": count,
                          "totalAmount": total,
                          "character": character,
                          "price": NumberFormat("###,##0.00#", 'en_US')
                              .parse(summa.text)
                        };
                        Navigator.pop(context, data);
                      } else
                        DialogUtils.showLangDialog(context,
                            okBtnFunction: () {},
                            text: "Товар сони 0 га тенг бўлмайди");
                    }
                  }),
              Expanded(
                child: Form(
                  key: _formKey,
                  child: SingleChildScrollView(
                    child: Container(
                      margin:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 13),
                      padding:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        color: AppColor.black.withOpacity(0.5),
                      ),
                      child: Column(
                        children: [
                          TouchedRow(
                            text1: "Товар номи",
                            text2: productName,
                            onPress: () async {
                              var selectedProduct = await Navigator.pushNamed(
                                  context, Routes.productName,
                                  arguments: {"products": products});
                              setState(() {
                                product = (selectedProduct as Tovarlar);
                                character = null;
                                counter.text = "0";
                              });
                            },
                            shadow: false,
                          ),
                          productName.isNotEmpty
                              ? Column(
                                  children: [
                                    divider(),
                                    TouchedRow(
                                      text1: "Характеристикаси",
                                      text2: character != null
                                          ? character.xarakName.isEmpty
                                              ? "Стандарт"
                                              : character.xarakName
                                          : "",
                                      onPress: () async {
                                        dynamic characterData =
                                            await Navigator.pushNamed(
                                                context, Routes.character,
                                                arguments: {
                                              "data": newlist[product.tovarId],
                                              "omborId": data
                                            });
                                        var productId = await product != null
                                            ? product.tovarId
                                            : "";

                                        var characterId =
                                            await characterData != null
                                                ? characterData.xarakId
                                                : "";
                                        double price = await sellingPrice
                                                .firstWhereOrNull((element) =>
                                                    element.tovarId ==
                                                        productId &&
                                                    element.xarakId ==
                                                        characterId &&
                                                    element.narxTuriId ==
                                                        args["id"])
                                                ?.dollarNarxi ??
                                            0;
                                        double sumPrice = await sellingPrice
                                                .firstWhereOrNull((element) =>
                                                    element.tovarId ==
                                                        productId &&
                                                    element.xarakId ==
                                                        characterId &&
                                                    element.narxTuriId ==
                                                        args["id"])
                                                ?.sumNarxi ??
                                            0;
                                        double low = await sellingPrice
                                                .firstWhereOrNull((element) =>
                                                    element.tovarId ==
                                                        productId &&
                                                    element.xarakId ==
                                                        characterId &&
                                                    element.narxTuriId ==
                                                        args["id"])
                                                ?.quyiDollarNarxi ??
                                            0;
                                        double lowPriceSum = await sellingPrice
                                                .firstWhereOrNull((element) =>
                                                    element.tovarId ==
                                                        productId &&
                                                    element.xarakId ==
                                                        characterId &&
                                                    element.narxTuriId ==
                                                        args["id"])
                                                ?.quyiSumNarxi ??
                                            0;
                                        double high = await sellingPrice
                                                .firstWhereOrNull((element) =>
                                                    element.tovarId ==
                                                        productId &&
                                                    element.xarakId ==
                                                        characterId &&
                                                    element.narxTuriId ==
                                                        args["id"])
                                                ?.yuqoriDollarNarxi ??
                                            0;
                                        double highPriceSum = await sellingPrice
                                                .firstWhereOrNull((element) =>
                                                    element.tovarId ==
                                                        productId &&
                                                    element.xarakId ==
                                                        characterId &&
                                                    element.narxTuriId ==
                                                        args["id"])
                                                ?.yuqoriSumNarxi ??
                                            0;

                                        print("PRICE:$price");

                                        double priceCurrency =
                                            args["currency"].valyutaId == sumId
                                                ? sumPrice
                                                : await Services.converter(
                                                    dollarId,
                                                    price,
                                                    args["currency"].valyutaId);
                                        double lowPriceCurrency =
                                            args["currency"].valyutaId == sumId
                                                ? lowPriceSum
                                                : await Services.converter(
                                                    dollarId,
                                                    low,
                                                    args["currency"].valyutaId);
                                        double highPriceCurrency =
                                            args["currency"].valyutaId == sumId
                                                ? highPriceSum
                                                : await Services.converter(
                                                    dollarId,
                                                    high,
                                                    args["currency"].valyutaId);

                                        setState(() {
                                          character = characterData;
                                          counter.text = "0";
                                          lowPrice = lowPriceCurrency;
                                          highPrice = highPriceCurrency;
                                          summa.text = priceCurrency.toString();
                                        });
                                        print("PRICE:$price");
                                      },
                                      shadow: false,
                                    ),
                                  ],
                                )
                              : Container(),
                          divider(),
                          GainedItem(
                              text1: "Сони",
                              value: counter,
                              shadow: false,
                              change: (String val) {},
                              limit: character != null
                                  ? character.tovarSoni.toDouble()
                                  : 0,
                              keyboardType: TextInputType.number),
                          divider(),
                          changeSum
                              ? TextInputWithTitle(
                                  text1: "Нархи",
                                  value: summa,
                                  shadow: false,
                                  keyboardType: TextInputType.number,
                                  validator: (value) {
                                    if (changeSum) {
                                      if (double.parse(
                                              value.replaceAll(",", '')) >
                                          double.parse(
                                              highPrice.toStringAsFixed(3))&&double.parse(highPrice.toStringAsFixed(3))!=0.0) {
                                        return "Сиз ${highPrice} дан катта қийматни киритдингиз";
                                      }
                                      if (double.parse(
                                              value.replaceAll(",", '')) <
                                          double.parse(
                                              lowPrice.toStringAsFixed(3))&&double.parse(lowPrice.toStringAsFixed(3))!=0.0) {
                                        return "Сиз ${lowPrice} дан кичик қийматни киритдингиз";
                                      }
                                    }
                                    return null;
                                  },
                                  onSubmit: () {},
                                )
                              : TextRow(
                                  text1: "Нархи",
                                  text2: Services.currencyFormatter(summa.text),
                                  shadow: false),
                          divider(),
                          TextRow(
                              text1: "Жами суммаси",
                              text2: total,
                              shadow: false),
                          divider(),
                          TextRow(
                              text1: "Остатка сони",
                              text2: character != null
                                  ? character.tovarSoni.toString()
                                  : "",
                              shadow: false),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  divider() {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 5),
        child: Divider(color: AppColor.white));
  }
}
