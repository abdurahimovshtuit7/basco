import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/user_base.dart';
import 'package:basco/routes.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/search_bar.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
import "package:collection/collection.dart";

class ProductsSceen extends StatefulWidget {
  const ProductsSceen({Key? key}) : super(key: key);

  static const String routeName = 'products-screen';

  @override
  _ProductsSceenState createState() => _ProductsSceenState();
}

class _ProductsSceenState extends State<ProductsSceen> {
  late final UserHive _hive = di.get<UserHive>();
  TextEditingController inputController = TextEditingController();

  bool isSearch = false;
  String text = '';
  Map<String, dynamic> data = {};
  late dynamic products = [];
  late List newlist = [];

  @override
  void initState() {
    super.initState();
    getHive();
  }

  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    setState(() {
      data = accountData.data;
      data['user_base'] != null
          ? UserBase.fromJson(data["user_base"])
              .response
              .map((e) => products.addAll(e.tovarlar))
              .toList()
          : [];

      newlist =
          groupBy(products, (x) => (x as Tovarlar).tovarId).values.toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    // Map newMap = groupBy(products, (x) => (x as Tovarlar).tovarId) as Map;
    // List newlist = newMap.values.toList();
    print(products);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: BackgroundView(
        child: Column(
          children: [
            SearchBar(
              title: "Товарлар",
              ontap: () {
                setState(() {
                  isSearch = !isSearch;
                  text = '';
                });
              },
            ),
            isSearch ? search() : Container(),
            Expanded(
                child: text == ""
                  ? MediaQuery.removePadding(
              context: context,
              removeTop: true,
              child: ListView.builder(
                itemBuilder: (BuildContext context, index) {
                  var value = newlist[index];
                  return StoreItem(
                      text1: value[0].tovarName,
                      onPress: () {
                        Navigator.pushNamed(context, Routes.tabScreen,
                            arguments: {
                              "data": value,
                            });
                      },
                      index: index);
                },
                itemCount: newlist.length,
              ),
            ):MediaQuery.removePadding(
              context: context,
              removeTop: true,
              child: ListView.builder(
                itemBuilder: (BuildContext context, index) {
                  var value = newlist.where((row) => (row[0].tovarName
                                        .toLowerCase()
                                        .contains(text.toLowerCase())))
                                    .toList()[index];
                  return StoreItem(
                      text1: value[0].tovarName,
                      onPress: () {
                        Navigator.pushNamed(context, Routes.tabScreen,
                            arguments: {
                              "data": value,
                            });
                      },
                      index: index);
                },
                itemCount: newlist.where((row) => (row[0].tovarName
                                        .toLowerCase()
                                        .contains(text.toLowerCase())))
                                    .toList().length,
              ),
            )
            )
          ],
        ),
      ),
    );
  }

   Widget search() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 13),
      child: TextFormField(
        controller: inputController,
        onChanged: (value) => {
          setState(() {
            text = value;
          })
        },
        cursorColor: AppColor.brandColor,
        style: whiteText,
        decoration: InputDecoration(
          suffixIcon: IconButton(
            onPressed: () {
              inputController.clear();
              setState(() {
                text = "";
              });
            },
            icon: Icon(
              Icons.clear,
              color: AppColor.brandColor,
            ),
          ),
          hintStyle: TextStyle(color: AppColor.gray),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: AppColor.brandColor),
            borderRadius: BorderRadius.circular(25.0),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: AppColor.gray, width: 1),
            borderRadius: BorderRadius.circular(25.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
              width: 1,
            ),
            borderRadius: BorderRadius.circular(25.0),
          ),
          contentPadding: EdgeInsets.only(left: 20),
          filled: true,
          fillColor: AppColor.black.withOpacity(0.7),
          hintText: "Товарни қидириш",
        ),
      ),
    );
  }
}

