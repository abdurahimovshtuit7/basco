part of 'trade_bloc.dart';

abstract class TradeState extends Equatable {
  const TradeState();
  
  @override
  List<Object> get props => [];
}

class TradeInitial extends TradeState {}


class TradeLoadingState extends TradeState {}

class TradeSuccessState extends TradeState {
  final int id;
  const TradeSuccessState({required this.id});
  @override
  List<Object> get props => [id];

}

class TradeErrorState extends TradeState {
final String message;

const TradeErrorState({required this.message});

  @override
  List<Object> get props => [message];
}