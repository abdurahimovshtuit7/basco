part of 'trade_bloc.dart';

abstract class TradeEvent extends Equatable {
  const TradeEvent();

  @override
  List<Object> get props => [data];
    get data => null;

}
class MultipleSale extends TradeEvent {
final List data;
const MultipleSale({required this.data});
  @override
  List<Object> get props => [data];
}

class DeleteTrade extends TradeEvent {
final List data;
const DeleteTrade({required this.data});
  @override
  List<Object> get props => [data];
}