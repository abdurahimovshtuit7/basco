import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/account.dart';
import 'package:basco/models/orders.dart';
import 'package:basco/repositories/services_repositories.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'trade_event.dart';
part 'trade_state.dart';

class TradeBloc extends Bloc<TradeEvent, TradeState> {
  final ServicesRepository _api = di.get();
  final UserHive _hive = di.get<UserHive>();
  
  TradeBloc() : super(TradeInitial()) {
    on<TradeEvent>((event, emit) async {
      if (event is MultipleSale) {
        await _sold(event, emit);
      }
      if (event is DeleteTrade) {
        await _delete(event, emit);
      } 
    });
  }


  Future<void> _sold(TradeEvent event, Emitter<TradeState> emit) async {
    emit(TradeLoadingState());
    try {
       String name = await SharedPref().read("activeUser");
       List<AccountData> baza = await _hive.getAccount();

        await _api.orders(event.data);

        event.data.map((e){
          e.setSend = true;
        });

         List<Orders> trades = baza.firstWhere((x) => x.name == name).data["trade"] != null
          ? (List<Orders>.from(baza.firstWhere((x) => x.name == name).data["trade"].map((x) => Orders.fromJson(x))))
          : [];
          
        for(int i=0;i<event.data.length;i++){
         trades.firstWhere((s)=> s.id == event.data[i].id).setSend = true; 
        }
        
        // trades.map((e){
        //   if(event.data.firstWhere((s)=>s.id == e.id)){
        //     e.setSend = true;
        //     // return e;
        //   }
        //   print('E:${e.send}');
        //   // return e;
        //  });
        // print(event.data.map((e)=>e.id).toList());
        // print(trades.map((e)=>e.id).toList());
        // print("changed:${trades}");
        // print(baza.firstWhere((x) => x.name == name).data["trade"][2].send);
        // event.data.setSend = true;
        // List a = [];
        // a = [...a,event.data];
        // baza.firstWhere((x) => x.name == name).data["trade"] !=null?baza.firstWhere((x) => x.name == name).data["trade"]: baza.firstWhere((x) => x.name == name).data.addAll(map);
        baza.firstWhere((x) => x.name == name).data["trade"]=trades;
    
        await _hive.saveAccount(baza);
    
      emit(TradeSuccessState(id: 0));
    } catch (e) {
      emit(TradeErrorState(message: e.toString()));
  
    }
  }


    Future<void> _delete(TradeEvent event, Emitter<TradeState> emit) async {
    emit(TradeLoadingState());
    try {
       String name = await SharedPref().read("activeUser");
       List<AccountData> baza = await _hive.getAccount();

        // await _api.orders(event.data);

        // event.data.map((e){
        //   e.setSend = true;
        // });

         List<Orders> trades = baza.firstWhere((x) => x.name == name).data["trade"] != null
          ? (List<Orders>.from(baza.firstWhere((x) => x.name == name).data["trade"].map((x) => Orders.fromJson(x))))
          : [];
          
        for(int i=0;i<event.data.length;i++){
          trades.removeWhere((x)=>x.id==event.data[i].id);
        }
        
      
        baza.firstWhere((x) => x.name == name).data["trade"]=trades;
    
        await _hive.saveAccount(baza);
    
      emit(TradeSuccessState(id:1));
    } catch (e) {
      emit(TradeErrorState(message: e.toString()));
  
    }
  }
}
