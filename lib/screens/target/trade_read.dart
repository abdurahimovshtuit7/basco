import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/dialog/custom_dialogs.dart';
import 'package:basco/dialog/custom_loading_dialog.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/orders.dart';
import 'package:basco/models/user.dart';
import 'package:basco/models/user_base.dart';
import 'package:basco/models/user_client.dart';
import 'package:basco/routes.dart';
import 'package:basco/screens/clientsScreens/sale/bloc/sale_bloc.dart';
import 'package:basco/screens/clientsScreens/sale/card.dart';
import 'package:basco/screens/target/expanded.dart';
import 'package:basco/utils/services.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';

class TradeRead extends StatefulWidget {
  const TradeRead({Key? key}) : super(key: key);
  static const String routeName = "trade-read";

  @override
  _TradeReadState createState() => _TradeReadState();
}

class _TradeReadState extends State<TradeRead> {
  TextEditingController comment = TextEditingController();
  TextEditingController _lastDate =
      TextEditingController(text: DateFormat("dd.MM.yyyy").format(DateTime.now()));
  final _bloc = SaleBloc();
  bool tabChange = true;
  bool _expanded = false;
  dynamic currency;
  dynamic selectedStore;
  late final UserHive _hive = di.get<UserHive>();
  Map<String, dynamic> data = {};
  List<Res> storage = [];
  dynamic userId;
  late dynamic args;
  dynamic productItem;
  List productItems = [];
  dynamic sinxTime = '';
  String clientName = '';
  dynamic selectedClient;

  @override
  void initState() {
    super.initState();
    getHive();
  }

  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    setState(() {
      data = accountData.data;
      storage =
          data["user_base"] != null ? UserBase.fromJson(data['user_base']).response : [];
      userId = data["user"] != null ? User.fromJson(data['user']).userId : '';
      sinxTime = data["clients"] != null
          ? (UserClient.fromJson(data['clients']).sinxronVaqti)
          : '';
      currency = data["user"] !=null ? User.fromJson(data["user"]).valyutalar:[];
    });
  }

  @override
  void didChangeDependencies() {
    setState(() {
      args = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
    });
    setState(() {
      clientName =  args["client"].mijozName;
      productItems = args["data"].ruyxat;
    });

    print("DATA:${args}");
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    print("Store:${storage.firstWhere((element) => element.omborId == args["data"].omborId)}");
    double total = 0;
    double count = 0;
    for (int i = 0; i < productItems.length; i++) {
      total += double.parse(productItems[i].narxi) * double.parse(productItems[i].soni);
      count += double.parse(productItems[i].soni);
      //  print("AAA:${productItems[i]["count"]}");
    }
   comment.text =  args["data"].izox;
   _lastDate.text = DateFormat("dd.MM.yyyy").format(DateFormat("yyyy-MM-dd hh:mm:ss").parse(args["data"].date));
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        body: BackgroundView(
          child: Column(
            children: [
              CustomAppBar(title: "Savdo", rightMenu: false),
              Expanded(
                  child: SingleChildScrollView(
                child: BlocConsumer<SaleBloc, SaleState>(
                  listener: (context, state) {
                    CustomLoadingDialog.hide(context);
                    if (state is SaleLoadingState) {
                      CustomLoadingDialog.show(context);
                    } else if (state is SaleSuccessState) {
                      Navigator.pop(context);
                      DialogUtils.showLangDialog(context,
                          okBtnFunction: () {},
                          text: "Маълумотлар муваффиқиятли кўчирилди.");
                    } else if (state is SaleErrorState) {
                      DialogUtils.showLangDialog(context,
                          okBtnFunction: () {}, text: state.message);
                      // showSnackBar(state.message);
                    }
                  },
                  builder: (context, state) {
                    return Column(
                      children: [
                      
                        Container(
                          margin: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 13),
                          padding: EdgeInsets.symmetric(
                              vertical: 15, horizontal: 20),
                          decoration: BoxDecoration(
                              color: AppColor.black.withOpacity(0.5),
                              borderRadius: BorderRadius.circular(16)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                  onTap: () {
                                    setState(() {
                                      tabChange = true;
                                    });
                                  },
                                  child: Container(
                                      padding: EdgeInsets.only(
                                        bottom:
                                            5, // Space between underline and text
                                      ),
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom: BorderSide(
                                        color: tabChange
                                            ? AppColor.brandColor2
                                            : AppColor.transparent,
                                        width: 1.0, // Underline thickness
                                      ))),
                                      child: Text(
                                        "Реквизитлар",
                                        style: TextStyle(
                                            color: tabChange
                                                ? AppColor.white
                                                : AppColor.brandColor2
                                                    .withOpacity(0.8)),
                                      ))),
                              SizedBox(width: 20),
                              InkWell(
                                  onTap: () {
                                    setState(() {
                                      tabChange = false;
                                    });
                                  },
                                  child: Container(
                                    padding: EdgeInsets.only(
                                      bottom:
                                          5, // Space between underline and text
                                    ),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                      color: tabChange
                                          ? AppColor.transparent
                                          : AppColor.brandColor2,
                                      width: 1.0, // Underline thickness
                                    ))),
                                    child: Text(
                                      "Товарлар",
                                      style: TextStyle(
                                          color: tabChange
                                              ? AppColor.brandColor2
                                                  .withOpacity(0.8)
                                              : AppColor.white),
                                    ),
                                  )),
                            ],
                          ),
                        ),
                        tabChange
                            ? Requisites(
                                name: clientName,
                            
                                clientsNavigation: () async {
                                 
                                },
                                storageNavigation: () async {
                                 
                                },
                                currencyNavigation: () async {
                                 
                                },
                                currency: currency.firstWhere((e)=>e.valyutaId == args["data"].valyutaId),
                                store: storage.firstWhere((element) => element.omborId == args["data"].omborId),
                                comment: comment,
                                count: Services.currencyFormatter(
                                    count.toString()),
                                total: Services.currencyFormatter(
                                    total.toString()),
                                itemLength:productItems.length,
                                send:args["data"].send, 
                                sinxTime: DateTime.now(),
                                date: _lastDate,
                                readOnly:args["data"].vozvrat,
                                isTemplateSelected: args["data"].vozvrat,
                                templateChanged: (){},
                                toDate: (time) {
                                  // setState(() {
                                  //   _lastDate.text =
                                  //       DateFormat("dd.MM.yyyy").format(time);
                                  // });
                                },
                                )

                            : Container(
                                margin: EdgeInsets.symmetric(horizontal: 12),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 15),
                                decoration: BoxDecoration(
                                    color: AppColor.black.withOpacity(0.5),
                                    borderRadius: BorderRadius.circular(16)),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Row(
                                            children: [
                                              Material(
                                                color: AppColor.transparent,
                                                child: InkWell(
                                                  onTap: () {
                                                  
                                                  },
                                                  child: Container(
                                                    padding: EdgeInsets.all(8),
                                                    decoration: BoxDecoration(
                                                        color: AppColor.black
                                                            .withOpacity(0.7),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(6)),
                                                    child: SvgPicture.asset(
                                                        Images.plus),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(width: 24),
                                              Material(
                                                color: AppColor.transparent,
                                                child: InkWell(
                                                  onTap: () {} ,
                                                  child: Container(
                                                    padding: EdgeInsets.all(5),
                                                    decoration: BoxDecoration(
                                                        color: AppColor.black
                                                            .withOpacity(0.7),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(6)),
                                                    child: SvgPicture.asset(
                                                        Images.barCode),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Material(
                                          color: AppColor.transparent,
                                          child: InkWell(
                                            onTap: () {},
                                            child: Container(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 10, vertical: 5),
                                              child: SvgPicture.asset(
                                                  Images.rigthMenu),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 4),
                                    ...productItems.map((item) {
                                      return Expand(
                                          item: item,
                                          ombor: storage.firstWhere((element) => element.omborId == args["data"].omborId),
                                          edit: () async {
                                            // dynamic p =
                                            //     await Navigator.pushNamed(
                                            //         context, Routes.editProduct,
                                            //         arguments: {
                                            //       "store":
                                            //           selectedStore.omborId,
                                            //       "currency": currency,
                                            //       "productItems": productItems,
                                            //       "item": item
                                            //     });

                                            //     setState(() {
                                            //       productItems[productItems.indexOf(item)] = p;
                                            //     });

                                            // print("KK");
                                          });
                                    }).toList(),
                                    SizedBox(
                                      height: 7,
                                    ),
                                    TextRow(
                                        text1: "Жами товарлар суммаси",
                                        text2: Services.currencyFormatter(
                                            total.toString())),
                                    Divider(
                                      color: AppColor.white,
                                    ),
                                    TextRow(
                                        text1: "Жами товарлар сони",
                                        text2: Services.currencyFormatter(
                                            count.toString()))
                                  ],
                                ),
                              ),
                      ],
                    );
                  },
                ),
              ))
            ],
          ),
        ),
      ),
    );
  }

  void onPress(routeName, {arguments}) {
    Navigator.of(context).pushNamed(routeName, arguments: arguments);
  }

  void sale(id) {
    print("SALE:${currency.valyutaId}");

    List newList = [];
    dynamic listItem = productItems.map((e) {
      return {
        "tovar_id": e["product"].tovarId,
        "xarak_id": e["character"].xarakId,
        "soni": e["count"].toString(),
        "narxi": e["price"].toString()
      };
    });

    print(listItem);

    newList.addAll(listItem);

    data = {
      "user_id": userId,
      "mijoz_id": args["client"]?args["data"].mijozId:selectedClient.mijozId,
      "ombor_id": selectedStore.omborId,
      "valyuta_id": currency.valyutaId,
      "sinxron_time": DateFormat("yyyyMMddhhmmss").format(sinxTime),
      "izox": comment.text,
      "ruyxat": newList,
      "send": false
    };
    Orders order = Orders.fromJson(data);
    print("TIME:${data["sinxron_time"]}");
    if (id == 0) {
      _bloc.add(Sold(data: order));
    } else if (id == 1) {
      _bloc.add(Save(data: order));
    }
  }

  void onPress2(routeName, {arguments}) async {
    dynamic item =
        await Navigator.of(context).pushNamed(routeName, arguments: arguments);
    // print("item:${item["character"].xarakName}");
    // print("pr:${item["product"].tovarName}");

    if (item != null) {
      setState(() {
        productItems.add(item);
      });
    }
  }

  productNameRow(
      {required String name,
      required String detail,
      required Function onPress,
      required bool expanded}) {
    return ExpansionPanel(
        headerBuilder: (context, isExpanded) {
          return Material(
            color: AppColor.transparent,
            child: InkWell(
              onTap: () => onPress(),
              child: Container(
                // margin: EdgeInsets.symmetric(vertical: 5),
                padding: EdgeInsets.symmetric(horizontal: 17, vertical: 15),
                decoration: BoxDecoration(
                    // color: AppColor.black.withOpacity(0.7),
                    // borderRadius: BorderRadius.circular(10)
                    ),
                child: Row(
                  children: [
                    Expanded(
                        child: Text(
                      name,
                      style: whiteText,
                    )),
                    Text(
                      detail,
                      style: whiteText.copyWith(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    SizedBox(width: 10),
                    SvgPicture.asset(Images.rigthArrow)
                  ],
                ),
              ),
            ),
          );
        },
        body: ListTile(
          iconColor: AppColor.green,
          title:
              Text('Description text', style: TextStyle(color: Colors.white)),
          tileColor: AppColor.black.withOpacity(0.2),
        ),
        isExpanded: _expanded,
        canTapOnHeader: false,
        backgroundColor: AppColor.black.withOpacity(0.7));
  }

 
}
