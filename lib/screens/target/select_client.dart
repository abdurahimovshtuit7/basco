import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/account.dart';
import 'package:basco/models/user_client.dart';
import 'package:basco/routes.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';

class SelectClient extends StatefulWidget {
  const SelectClient({Key? key}) : super(key: key);
  static const String routeName = 'select-clients';
  @override
  _SelectClientState createState() => _SelectClientState();
}

class _SelectClientState extends State<SelectClient> {
  late final UserHive _hive = di.get<UserHive>();
  TextEditingController inputController = TextEditingController();
  Map<String, dynamic> data = {};
  bool isSearch = false;
  String text = '';
  List clients = [];

  @override
  void initState() {
    super.initState();
    getHive();
  }

  Future<void> getHive() async {
    // print(_hive);
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    setState(() {
      data = accountData.data;
      clients = data["clients"] != null
          ? (UserClient.fromJson(data["clients"]).mijozlar)
          : [];
    });
  }

  menuPressed() {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    // final clients = data["clients"]!=null?(UserClient.fromJson(data["clients"]).mijozlar):[];
    print(clients);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: BackgroundView(
        child: Column(
          children: [
            CustomHeader(
              menuPressed: () => menuPressed(),
              rightButton: () {
                setState(() {
                  isSearch = !isSearch;
                  text = '';
                });
              },
              icon: false,
              leftIcon: true,
            ),
            isSearch ? search() : Container(),
            Expanded(
              child: text == ""
                  ? MediaQuery.removePadding(
                      context: context,
                      removeTop: true,
                      child: clients.length != 0
                          ? ListView.builder(
                              itemBuilder: (BuildContext ctx, index) {
                                var client = clients[index];
                                return Item(
                                  name: client.mijozName,
                                  phone: client.mijozPhone,
                                  address: client.mijozAddress,
                                  onPress: () =>
                                      onPress(client),
                                  image:
                                      "https://p.kindpng.com/picc/s/78-785975_icon-profile-bio-avatar-person-symbol-chat-icon.png",
                                );
                              },
                              itemCount: clients.length,
                            )
                          : Container(),
                    )
                  : MediaQuery.removePadding(
                      context: context,
                      removeTop: true,
                      child: clients.length != 0
                          ? ListView.builder(
                              itemBuilder: (BuildContext ctx, index) {
                                var client = clients
                                    .where((row) => (row.mijozName
                                        .toLowerCase()
                                        .contains(text.toLowerCase())))
                                    .toList()[index];
                                return Item(
                                  name: client.mijozName,
                                  phone: client.mijozPhone,
                                  address: client.mijozAddress,
                                  onPress: () =>
                                      onPress(client),
                                  image:
                                      "https://p.kindpng.com/picc/s/78-785975_icon-profile-bio-avatar-person-symbol-chat-icon.png",
                                );
                              },
                              itemCount: clients
                                  .where((row) => (row.mijozName
                                      .toLowerCase()
                                      .contains(text.toLowerCase())))
                                  .toList()
                                  .length,
                            )
                          : Container(),
                    ),
            )
          ],
        ),
      ),
    );
  }

  Widget search() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 13),
      child: TextFormField(
        controller: inputController,
        onChanged: (value) => {
          setState(() {
            text = value;
          })
        },
        cursorColor: AppColor.brandColor,
        style: whiteText,
        decoration: InputDecoration(
          suffixIcon: IconButton(
            onPressed: () {
              inputController.clear();
              setState(() {
                text = "";
              });
            },
            icon: Icon(
              Icons.clear,
              color: AppColor.brandColor,
            ),
          ),
          hintStyle: TextStyle(color: AppColor.gray),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: AppColor.brandColor),
            borderRadius: BorderRadius.circular(25.0),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: AppColor.gray, width: 1),
            borderRadius: BorderRadius.circular(25.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
              width: 1,
            ),
            borderRadius: BorderRadius.circular(25.0),
          ),
          contentPadding: EdgeInsets.only(left: 20),
          filled: true,
          fillColor: AppColor.black.withOpacity(0.7),
          hintText: "Мижозни қидириш",
        ),
      ),
    );
  }

  void onPress(item) {
   Navigator.pop(context,item);
  }
}
