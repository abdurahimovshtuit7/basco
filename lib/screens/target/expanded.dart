import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/utils/services.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';

class Expand extends StatefulWidget {
  final dynamic item;
  final dynamic ombor;
  final Function edit;
 
  @override
  State<Expand> createState() => _ExpandState();

  const Expand({ Key? key,required this.item,required this.ombor,required this.edit}) : super(key: key);

}

class _ExpandState extends State<Expand> {
  ExpandableController _controller =
      ExpandableController(initialExpanded: false);

  @override
  Widget build(BuildContext context) {
    _toggleExpandables(int index) {
      setState(() {
        _controller.value = !_controller.value;

        // _getController(index).value = true;
      });
    }

    buildItem(String label) {
      return Padding(
        padding: const EdgeInsets.all(10.0),
        child: Text(
          label,
          style: whiteText,
        ),
      );
    }

    buildList() {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
        child: Column(
          children: <Widget>[
             TextRow(text1: "Характеристика",text2:  widget.ombor.tovarlar.firstWhere((e)=>e.xarakId== widget.item.xarakId).xarakName,),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: Divider(color: AppColor.white,),
            ),
            TextRow(text1: "Cумма",text2: Services.currencyFormatter((double.parse(widget.item.narxi) * double.parse(widget.item.soni)).toString()),),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: Divider(color: AppColor.white,),
            ),
            TextRow(text1: "Сони",text2: widget.item.soni.toString(),),
          ],
        ),
      );
    }

    return ExpandableNotifier(
        // controller: _controller,
        child: Padding(
      padding: const EdgeInsets.all(0),
      child: ScrollOnExpand(
        child: Card(
          clipBehavior: Clip.antiAlias,
          color: AppColor.transparent,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Column(
            children: <Widget>[
              ExpandablePanel(
                controller: _controller,
                theme: const ExpandableThemeData(
                    headerAlignment: ExpandablePanelHeaderAlignment.center,
                    tapBodyToExpand: false,
                    tapBodyToCollapse: false,
                    tapHeaderToExpand: false,
                    hasIcon: false,
                    useInkWell: true),
                header: Container(
                  decoration: BoxDecoration(
                      color: AppColor.black.withOpacity(0.7),
                      borderRadius: BorderRadius.circular(10)),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                           widget.ombor.tovarlar.firstWhere((e)=>e.tovarId== widget.item.tovarId).tovarName,
                            style: whiteText,
                          ),
                        ),
                        Material(
                            color: AppColor.transparent,
                            child: InkWell(
                              onTap: () =>
                                widget.edit()
                              ,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    bottom: 8.0, top: 8.0, left: 8.0),
                                child: SvgPicture.asset(Images.rigthArrow),
                              ),
                            )),
                        Material(
                          color: AppColor.transparent,
                          child: InkWell(
                            onTap: () {
                              print("object");
                              _toggleExpandables(0);
                            },
                            child: ExpandableIcon(
                              theme: const ExpandableThemeData(
                                expandIcon: Icons.chevron_right,
                                collapseIcon: Icons.keyboard_arrow_down_rounded,
                                iconColor: Colors.white,
                                iconSize: 30.0,
                                iconRotationAngle: math.pi / 2,
                                // iconPadding: EdgeInsets.only(right: 5),
                                hasIcon: false,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                collapsed: Container(),
                expanded: buildList(),
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
