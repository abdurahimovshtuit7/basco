import 'package:basco/screens/clientsScreens/akt/act_list.dart';
import 'package:basco/screens/clientsScreens/akt/akt.dart';
import 'package:basco/screens/clientsScreens/cashFlow/cash.dart';
import 'package:basco/screens/clientsScreens/character/character.dart';
import 'package:basco/screens/clientsScreens/editProduct/editProduct.dart';
import 'package:basco/screens/loan/loan.dart';
import 'package:basco/screens/loan/loan_description.dart';
import 'package:basco/screens/map/map.dart';
import 'package:basco/screens/map/select_location.dart';
import 'package:basco/screens/payments/edit_payment.dart';
import 'package:basco/screens/payments/payments.dart';
import 'package:basco/screens/payments/read_payment.dart';
import 'package:basco/screens/product/product.dart';
import 'package:basco/screens/productsScreen/products.dart';
import 'package:basco/screens/settings/settings.dart';
import 'package:basco/screens/storage/storage_balance.dart';
import 'package:basco/screens/tab/tab.dart';
import 'package:basco/screens/target/select_client.dart';
import 'package:basco/screens/target/target.dart';
import 'package:basco/screens/target/trade_read.dart';
import 'package:flutter/material.dart';

import 'screens/auth/accounts/accounts.dart';
import 'screens/auth/signUp/sign_up.dart';
import 'screens/auth/login/login.dart';
import 'screens/clientsScreens/checkout/checkout.dart';
import 'screens/clientsScreens/clientsCurrency/client_currency.dart';
import 'screens/mainScreen/main_screen.dart';
import 'screens/synchrony/synchrony.dart';
import 'screens/clientsScreens/addProduct/add_product.dart';
import 'screens/clientsScreens/cashFlow/cashflow.dart';
import 'screens/clientsScreens/client/client.dart';
import 'screens/clientsScreens/clients/clients.dart';
import 'screens/clientsScreens/currency/currency.dart';
import 'screens/clientsScreens/productName/product_name.dart';
import 'screens/clientsScreens/qrCode/qr_code.dart';
import 'screens/clientsScreens/sale/sale.dart';
import 'screens/clientsScreens/storage/storage.dart';
import 'screens/target/edit_trade.dart';


class Routes {
  Routes._();
  static const String main = MainScreen.routeName;
  static const String clients = Clients.routeName;
  static const String client = Client.routeName;
  static const String sale = Sale.routeName;
  static const String currency = Currency.routeName;
  static const String storage = Storage.routeName;
  static const String productName = ProductName.routeName;
  static const String addProduct = AddProduct.routeName;
  static const String cashFlow = CashFlow.routeName;
  static const String barCode = QRCodeScreen.routeName;
  static const String checkout = Checkout.routeName;
  static const String clientCurrency = ClientCurrency.routeName;
  static const String accounts = Accounts.routeName;
  static const String signUp = SignUp.routeName;
  static const String login = Login.routeName;
  static const String synchrony = Synchrony.routeName;
  static const String settings = Settings.routeName;
  static const String loan = Loan.routeName;
  static const String loanDescription =LoanDescription.routeName;
  static const String storageBalance = StorageBalance.routeName;
  static const String productScreen = ProductScreen.routeName;
  static const String aktScreen = AktScreen.routeName;
  static const String actList = ActList.routeName;
  static const String productsScreen = ProductsSceen.routeName;
  static const String tabScreen = TabScreen.routeName;
  static const String character = Character.routeName;
  static const String trade = Target.routeName;
  static const String selectClient = SelectClient.routeName;
  static const String editProduct = EditProduct.routeName;
  static const String tradeRead = TradeRead.routeName;
  static const String cash = Cash.routeName;
  static const String payments = Payments.routeName;
  static const String readPayment = ReadPayment.routeName;
  static const String mapScreen =SingleBranchScreen.routeName;
  static const String selectLocation = SelectLocation.routeName;
  static const String editPayment = EditPayment.routeName;
  static const String editTrade = EditTrade.routeName;

  static final routes = <String, WidgetBuilder>{
    main: (BuildContext context) => MainScreen(),
    clients: (BuildContext context) => Clients(),
    client: (BuildContext context) => Client(),
    sale: (BuildContext context) => Sale(),
    currency: (BuildContext context) => Currency(),
    storage: (BuildContext context) => Storage(),
    productName: (BuildContext context) => ProductName(),
    addProduct: (BuildContext context) => AddProduct(),
    cashFlow: (BuildContext context) => CashFlow(),
    barCode: (BuildContext context) => QRCodeScreen(),
    checkout: (BuildContext context) => Checkout(),
    clientCurrency: (BuildContext context) => ClientCurrency(),
    accounts: (BuildContext context) => Accounts(),
    signUp:(BuildContext context) => SignUp(),
    login:(BuildContext context) => Login(),
    synchrony:(BuildContext context) => Synchrony(),
    settings:(BuildContext context) => Settings(),
    loan:(BuildContext context) => Loan(),
    loanDescription:(BuildContext context) => LoanDescription(),
    storageBalance:(BuildContext context) => StorageBalance(),
    productScreen:(BuildContext context) =>ProductScreen(),
    aktScreen:(BuildContext context) => AktScreen(),
    actList:(BuildContext context) => ActList(),
    productsScreen:(BuildContext context) => ProductsSceen(),
    tabScreen:(BuildContext context) => TabScreen(),
    character:(BuildContext context) => Character(),
    trade:(BuildContext context) => Target(),
    selectClient:(BuildContext context) => SelectClient(),
    editProduct:(BuildContext context) => EditProduct(),
    tradeRead:(BuildContext context) => TradeRead(),
    cash:(BuildContext context) => Cash(),
    payments:(BuildContext context) => Payments(),
    readPayment:(BuildContext context) => ReadPayment(),
    mapScreen:(BuildContext context) => SingleBranchScreen(),
    selectLocation:(BuildContext context) => SelectLocation(),
    editPayment:(BuildContext context) => EditPayment(),
    editTrade:(BuildContext context) => EditTrade()
  };
}
