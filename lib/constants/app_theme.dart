import 'package:basco/constants/app_text_style.dart';
import 'package:flutter/material.dart';

import 'app_font.dart';
import 'colors.dart';

final ThemeData themeData = ThemeData(
    // primarySwatch: Colors,
    fontFamily: AppFont.MontserratRegular,
    primaryColor: AppColor.brandColor,
    scaffoldBackgroundColor: Colors.white,
    appBarTheme: const AppBarTheme(
      elevation: 0.0,
      backgroundColor: AppColor.white,
      iconTheme: IconThemeData(
        color: AppColor.brandColor, //change your color here
      ),
      titleTextStyle:TextStyle(color: AppColor.black)
    ));


