import 'package:flutter/material.dart';

import 'app_font.dart';
import 'colors.dart';

class AppTextStyle {
  AppTextStyle._();

  static dynamic textStyle(double size, dynamic weight) {
    return TextStyle(
      color: AppColor.black,
      fontSize: size,
      fontFamily: AppFont.MontserratRegular,
      fontWeight: weight,
    );
  }

  static dynamic headerTextStyle() {
    return TextStyle(
      fontSize: 18,
      color: AppColor.black,
    );
  }

  static dynamic lightTextStyle() {
    return TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w600,
      fontSize: 22,
      fontFamily: AppFont.MontserratBold,
      shadows: <Shadow>[
        Shadow(
          offset: Offset(1.0, 1.0),
          blurRadius: 15.0,
          color: AppColor.shadow.withOpacity(0.4),
        ),
        Shadow(
          offset: Offset(1.0, 1.0),
          blurRadius: 15.0,
          color: AppColor.shadow.withOpacity(0.4),
        ),
      ],
    );
  }
}

final TextStyle ligthTextStyle = TextStyle(
    color: AppColor.white,
    fontWeight: FontWeight.w600,
    fontSize: 22,
    fontFamily: AppFont.MontserratBold,
    shadows: <Shadow>[
      Shadow(
        offset: Offset(1.0, 1.0),
        blurRadius: 15.0,
        color: AppColor.shadow.withOpacity(0.4),
      ),
      Shadow(
        offset: Offset(1.0, 1.0),
        blurRadius: 15.0,
        color: AppColor.shadow.withOpacity(0.4),
      )
    ]);

final TextStyle whiteText = TextStyle(
    color: AppColor.white,
    fontWeight: FontWeight.w600,
    fontSize: 18,
    fontFamily: AppFont.MontserratBold,
   );

final TextStyle brandColorText = TextStyle(
    color: AppColor.brandColor2,
    fontWeight: FontWeight.w600,
    fontSize: 18,
    fontFamily: AppFont.MontserratBold,
   );

final TextStyle grayColor = TextStyle(
    color: AppColor.grayText,
    fontWeight: FontWeight.w600,
    fontSize: 18,
    fontFamily: AppFont.MontserratBold,
   );

final TextStyle blackColor = TextStyle(
    color: AppColor.black.withOpacity(0.8),
    fontWeight: FontWeight.w600,
    fontSize: 15,
    fontFamily: AppFont.MontserratBold,
   );