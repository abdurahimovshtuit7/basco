class Images {
  Images._();

  //from icons directory
  static const String brandName = 'assets/icons/brandname.svg';
  static const String menu = 'assets/icons/menu.svg';
  static const String account = 'assets/icons/account.svg';
  static const String product = 'assets/icons/product.svg';
  static const String sale = 'assets/icons/sale.svg';
  static const String cashflow = 'assets/icons/cashflow.svg';
  static const String storage = 'assets/icons/storage.svg';
  static const String backlog = 'assets/icons/backlog.svg';
  static const String settings = 'assets/icons/settings.svg';
  static const String syncronization = 'assets/icons/syncronization.svg';
  static const String searchIcon = 'assets/icons/searchIcon.svg';
  static const String phone = 'assets/icons/phone.svg';
  static const String location = 'assets/icons/location.svg';
  static const String leftArrow = 'assets/icons/leftArrow.svg';
  static const String rigthMenu = 'assets/icons/rightMenu.svg';
  static const String rigthArrow = 'assets/icons/rightArrow.svg';
  static const String plus = 'assets/icons/plus.svg';
  static const String barCode = 'assets/icons/barCode.svg';
  static const String home = 'assets/icons/home.svg';
  static const String character = 'assets/icons/character.svg';
  static const String currency = 'assets/icons/currency.svg';
  static const String qr = 'assets/icons/qr.svg';
  static const String map =  'assets/icons/map.svg';

 

  //from images directory
  static const String background = 'assets/images/background.png';
  static const String vector = 'assets/images/vector.png';
  static const String profile = 'assets/images/profile.jpg';



}
