import 'package:flutter/material.dart';

class AppColor {
  AppColor._();
  static const Color white = Color(0xFFFFFFFF);
  static const Color black = Color(0xFF000000);
  static const Color red = Color(0xFFFF1818);

  static const Color primary = Color(0xFF0D2961);
  static const Color brandColor = Color(0xFF825D0F);
  static const Color brandColor2 = Color(0xFFC8B56B);
  static const Color transparent = Colors.transparent;
  static const Color green = Color(0xFF478C3E);
  static const Color shadow = Color(0xFFFFD600);
  static const Color avatarBorder1 = Color(0xFFE2D58F);
  static const Color avatarBorder2 = Color(0xFFCFB581);
  static const Color grayText = Color(0xFFC4C4C4);

  static const Color yellow = Color(0xffE6B025);
  static const Color yellow2 = Color(0xffD69F0B);
  static const Color gray = Color(0xFF898989);
  static const Color orange = Color(0xFFE2B44E);

  
}
// CDB179