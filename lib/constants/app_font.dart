class AppFont {
  AppFont._();

  static String MontserratBold = 'Montserrat-Bold';
  static String MontserratRegular = 'Montserrat-Regular';
  static String MontserratMedium = 'Montserrat-Medium';
  static String MontserratThin = 'Montserrat-Thin';
}
