import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';

class HiveBase {
  late Box _userBox;

  Future<void> initialize() async {
    final dir = await getApplicationDocumentsDirectory();
    Hive.init('${dir.path}/hiveBase');
    // Hive.init('assets/hiveBase');

    _userBox = await Hive.openBox('userHive');
    print(dir.path);
  }

  Box get userBox => _userBox;
}



