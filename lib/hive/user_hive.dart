import 'package:basco/models/account.dart';
import 'package:hive/hive.dart';

class UserHive {
  final Box _box;

  const UserHive(this._box);

  Future<void> saveAccount(List<AccountData> value) async {
    await _box.put("accounts", accountsToJson(value));
  }

  Future<List<AccountData>> getAccount() async {
     final json = await _box.get("accounts");
    return await accountsFromJson(json);
  }

}
