import 'package:basco/constants/colors.dart';
import 'package:flutter/material.dart';

class CustomBottomLinearDialog extends StatelessWidget {
  static bool _ignoring = true;
  static bool _canPop = false;

  const CustomBottomLinearDialog._({Key? key}) : super(key: key);

  static void show(BuildContext context, {bool ignoring = true}) async {
    _ignoring = ignoring;
    _canPop = true;
    Scaffold.of(context).showBottomSheet(
      (context) => GestureDetector(
        child: const CustomBottomLinearDialog._(),
        onVerticalDragStart: _ignoring ? (_) {} : null,
      ),
    );
  }

  static void hide(BuildContext context) {
    if (_canPop) Navigator.pop(context);
    _canPop = false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (!_ignoring) _canPop = false;
        return !_ignoring;
      },
      child: GestureDetector(
        onTap: _ignoring
            ? null
            : () {
                if (_canPop) Navigator.pop(context);
                _canPop = false;
              },
        behavior: HitTestBehavior.opaque,
        child: LinearProgressIndicator(
          minHeight: 8,
          backgroundColor: AppColor.green.withOpacity(0.5),
          color: AppColor.green,
        ),
      ),
    );
  }
}
