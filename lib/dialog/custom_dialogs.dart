import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/utils/navigation_services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class DialogUtils {
  // static DialogUtils _instance = new DialogUtils.internal();
  // DialogUtils.internal();
  // factory DialogUtils() => _instance;

// using dialog  DialogUtils.showCustomDialog(context,okBtnFunction: (){ print("Ha");});
// using dialog  DialogUtils.showLangDialog(context,okBtnFunction: (){ print("Ha");});

  static void showCustomDialog(BuildContext context,
      {required String text, required VoidCallback okBtnFunction}) {
    showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            backgroundColor: AppColor.black.withOpacity(0.7),
            contentPadding: EdgeInsets.all(0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            content: Container(
              padding: EdgeInsets.all(16),
              // color: Colors.red,
              height: 170,
              width: MediaQuery.of(context).size.width * 0.9,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(text, style: ligthTextStyle),
                  SizedBox(height: 20),
                  Material(
                    color: AppColor.transparent,
                    child: InkWell(
                      onTap: () => okBtnFunction(),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              Images.phone,
                              height: 20,
                            ),
                            SizedBox(width: 15),
                            Text(
                              "Кунгирок килиш",
                              style: brandColorText,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  // Material(
                  //   color: AppColor.transparent,
                  //   child: InkWell(
                  //     onTap: () {},
                  //     child: Padding(
                  //       padding: const EdgeInsets.all(8.0),
                  //       child: Row(
                  //         mainAxisAlignment: MainAxisAlignment.center,
                  //         children: [
                  //           SvgPicture.asset(
                  //             Images.location,
                  //             height: 20,
                  //           ),
                  //           SizedBox(width: 15),
                  //           Text(
                  //             "Манзилни куриш",
                  //             style: brandColorText,
                  //           )
                  //         ],
                  //       ),
                  //     ),
                  //   ),
                  // )
                ],
              ),
            ),
          );
        });
  }

  static void showLangDialog(BuildContext context,
      {required VoidCallback? okBtnFunction, text}) {
    showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            scrollable: true,
            backgroundColor: AppColor.black.withOpacity(0.9),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
                side: BorderSide(color: AppColor.black)),
            content: Container(
              padding: EdgeInsets.all(10),
              // height: 100,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                // mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(height: 15),
                  Text(
                    text,
                    style: whiteText.copyWith(fontSize: 18),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 15),
                  TextButton(
                      onPressed: () {
                        NavigationService.instance.goback();
                      },
                      child: Text(
                        "OK",
                        style: brandColorText,
                      ))
                ],
              ),
            ),
          );
        });
  }

    static void showMessageDialog(BuildContext context,
      {required VoidCallback? okBtnFunction, text}) {
    showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            scrollable: true,
            backgroundColor: AppColor.black.withOpacity(0.9),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
                side: BorderSide(color: AppColor.black)),
            content: Container(
              padding: EdgeInsets.all(10),
              // height: 100,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                // mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(height: 15),
                  Text(
                    text,
                    style: whiteText.copyWith(fontSize: 18),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 15),
                  TextButton(
                      onPressed: () {
                         Navigator.pop(context);
                          okBtnFunction!();
                      }
                      ,
                      child: Text(
                        "OK",
                        style: brandColorText,
                      ))
                ],
              ),
            ),
          );
        });
  }


  static void showChatDialog(BuildContext context,
      {required VoidCallback? okBtnFunction, required txt}) {
    showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            contentPadding: EdgeInsets.zero,
            backgroundColor: Colors.white,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
                side: BorderSide(color: AppColor.white)),
            content: Container(
              padding:
                  EdgeInsets.only(left: 16, right: 16, bottom: 40, top: 12),
              height: 250,
              width: MediaQuery.of(context).size.width - 50,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      Expanded(child: SizedBox()),
                      IconButton(
                          onPressed: () {},
                          icon: Icon(Icons.close_rounded, color: Colors.black))
                    ],
                  ),
                  Expanded(flex: 1, child: SizedBox()),
                  Text(txt, style: whiteText),
                  Expanded(flex: 1, child: SizedBox()),
                  ElevatedButton(
                      onPressed: okBtnFunction,
                      child: Text('Ha', style: whiteText),
                      style: ElevatedButton.styleFrom(
                          primary: Colors.red,
                          fixedSize: Size(120, 50),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(
                                12), // side: BorderSide(color: MyColor.loginBtnColor2)
                          )))
                ],
              ),
            ),
          );
        });
  }

  static void showLocationDialog(BuildContext context,
      {required String text, required VoidCallback toMap,required VoidCallback selectLocation}) {
    showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            backgroundColor: AppColor.black.withOpacity(0.9),
            contentPadding: EdgeInsets.all(0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            content: Container(
              padding: EdgeInsets.all(16),
              // color: Colors.red,
              height: 270,
              // width: MediaQuery.of(context).size.width * 0.9,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    Images.map,
                    height: 30,
                    width: 30,
                  ),
                  SizedBox(height: 10),
                  Text(text, style: ligthTextStyle),
                  SizedBox(height: 20),
                  Material(
                    color: AppColor.transparent,
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                        selectLocation();
                      },
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: AppColor.orange,
                            borderRadius: BorderRadius.circular(10)),
                        padding: const EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10),
                        child: Text(
                          "Ўзгартириш",
                          style: blackColor.copyWith(fontSize: 16),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 15),
                  Material(
                    color: AppColor.transparent,
                    child: InkWell(
                      onTap: (){
                         Navigator.pop(context);
                        toMap();
                     
                      },
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: AppColor.orange,
                            borderRadius: BorderRadius.circular(10)),
                        padding: const EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10),
                        child: Text(
                          "Харитадан кўриш",
                          style: blackColor.copyWith(fontSize: 16),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

    static void showDeleteDialog(BuildContext context,
      {required String text, required VoidCallback toDelete}) {
    showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            backgroundColor: AppColor.black.withOpacity(0.9),
            contentPadding: EdgeInsets.all(0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            content: Container(
              padding: EdgeInsets.all(16),
              // color: Colors.red,
              height: 200,
              // width: MediaQuery.of(context).size.width * 0.9,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // SvgPicture.asset(
                  //   Images.,
                  //   height: 30,
                  //   width: 30,
                  // ),
                  SizedBox(height: 10),
                  Text(text, textAlign: TextAlign.center,style: ligthTextStyle),
                  SizedBox(height: 20),
                  Row(
                    children: [
                      Expanded(
                        child: Material(
                          color: AppColor.transparent,
                          child: InkWell(
                            onTap: () {
                              Navigator.pop(context);
                               toDelete();
                            },
                            child: Container(
                              // width: double.infinity,
                              decoration: BoxDecoration(
                                  color: AppColor.orange,
                                  borderRadius: BorderRadius.circular(10)),
                              padding: const EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 10),
                              child: Text(
                                "Ҳа",
                                style: blackColor.copyWith(fontSize: 16),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 15),
                      Expanded(
                        child: Material(
                          color: AppColor.transparent,
                          child: InkWell(
                            onTap: (){
                               Navigator.pop(context);
                            
                           
                            },
                            child: Container(
                              // width: double.infinity,
                              decoration: BoxDecoration(
                                  color: AppColor.orange,
                                  borderRadius: BorderRadius.circular(10)),
                              padding: const EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 10),
                              child: Text(
                                "Йўқ",
                                style: blackColor.copyWith(fontSize: 16),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        });
  }
}
