import 'package:basco/constants/colors.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/repositories/services_repositories.dart';
import 'package:get_it/get_it.dart';


GetIt di = GetIt.instance;

Future<void>  locatorSetUp() async {
  di.registerSingleton(HiveBase());
  await di.get<HiveBase>().initialize();
  di.registerFactory(() => UserHive(di.get<HiveBase>().userBox));
  di.registerSingleton(ServicesRepository());
  
}