import 'dart:io';
import 'dart:math';

import 'package:basco/constants/app_theme.dart';
import 'package:basco/routes.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'di/locator.dart';
import 'translations/codegen_loader.g.dart';
import 'package:device_preview/device_preview.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:permission_handler/permission_handler.dart';


import 'utils/navigation_services.dart';
import 'utils/shared_pref.dart';
final snackBarKey = GlobalKey<ScaffoldMessengerState>();


class MyBlocObserver extends BlocObserver {
  @override
  void onCreate(BlocBase bloc) {
    super.onCreate(bloc);
    print('onCreate -- ${bloc.runtimeType}');
  }

  @override
  void onEvent(Bloc bloc, Object? event) {
    super.onEvent(bloc, event);
    print('onEvent -- ${bloc.runtimeType}, $event');
  }

  @override
  void onChange(BlocBase bloc, Change change) {
    super.onChange(bloc, change);
    print('onChange -- ${bloc.runtimeType}, $change');
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print('onTransition -- ${bloc.runtimeType}, $transition');
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    print('onError -- ${bloc.runtimeType}, $error');
    super.onError(bloc, error, stackTrace);
  }

  @override
  void onClose(BlocBase bloc) {
    super.onClose(bloc);
    print('onClose -- ${bloc.runtimeType}');
  }
}
Future<void> main() async {
  dynamic initialRoute;
  WidgetsFlutterBinding.ensureInitialized();
  await locatorSetUp();
  // Bloc.observer = SimpleBlocObserver();

  await EasyLocalization.ensureInitialized();
  // bool platform = UniversalPlatform.isIOS;

  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent, // transparent status bar
      statusBarBrightness: Brightness.light,
      statusBarIconBrightness: Brightness.light // dark text for status bar
      ));

  Future<String?> _getId() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      var iosDeviceInfo = await deviceInfo.iosInfo;
      await SharedPref().save('deviceID', iosDeviceInfo.identifierForVendor);
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      await SharedPref().save('deviceID', androidDeviceInfo.androidId);
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }
  await _getId();
 BlocOverrides.runZoned(
    () {
        runApp(
    EasyLocalization(
      supportedLocales: const [
        Locale('ru'),
        Locale('uz'),
      ],
      path:
          'assets/translations', // <-- change the path of the translation files
      fallbackLocale: const Locale('ru'),
      startLocale: const Locale('ru'),
      assetLoader: const CodegenLoader(),
      useOnlyLangCode: true,
      child:  MyApp(initialRoute: initialRoute),
      
    ),
  );
  // Use cubits...
    },
    blocObserver: MyBlocObserver(),
  );

}

class MyApp extends StatefulWidget {
  final dynamic initialRoute;
  const MyApp({Key? key, this.initialRoute}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late Permission permission;
  PermissionStatus permissionStatus = PermissionStatus.denied;
  void _listenForPermission() async {
   final status =  await Permission.location.status;
    setState(() {
      permissionStatus = status;
    });
    switch(status){
      case PermissionStatus.denied:
      requestForPermission();
      break;
      case PermissionStatus.granted:
      break;
      case PermissionStatus.limited:
      Navigator.pop(context);
      break;
      case PermissionStatus.restricted:
      Navigator.pop(context);
      break;
      case PermissionStatus.permanentlyDenied:
      Navigator.pop(context);
      break;
    }
  }

  Future<void> requestForPermission() async {
    final status = await Permission.storage.request();
    setState(() {
      permissionStatus = status;
    });

  }
  @override
  void initState() {
    _listenForPermission();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    int a = 300;
    sqrt(a);
    return MaterialApp(
      useInheritedMediaQuery: true,
      localizationsDelegates: context.localizationDelegates,
      // supportedLocales: context.supportedLocales,
      // locale: DevicePreview.locale(context),
      debugShowCheckedModeBanner: false,
      builder: DevicePreview.appBuilder,
      theme: themeData,
      scaffoldMessengerKey: snackBarKey,
      initialRoute: Routes.accounts,
      routes: Routes.routes,
      navigatorKey: NavigationService.instance.navigationKey
    );
  }
}
