import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/dialog/custom_dialogs.dart';
import 'package:basco/utils/custom_boxshadow.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class GainedItem extends StatelessWidget {
  final String text1;
  final dynamic value;
  final dynamic color;
  final bool shadow;
  final dynamic keyboardType;
  final double limit;
  final Function change;

  const GainedItem(
      {Key? key,
      required this.text1,
      required this.value,
      required this.keyboardType,
      required this.limit,
      required this.change,
      this.color = AppColor.white,
      this.shadow = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // int n = int.tryParse(value.text)??0;
    // if(n>limit){
    //   return DialogUtils.showLangDialog(context, okBtnFunction: (){},text: "Маълумотлар муваффиқиятли кўчирилди.");

    // }

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            text1,
            style: textStyle(),
          ),
          SizedBox(width: 20),
          Expanded(
            child: TextFormField(
              controller: value,
              style: whiteText,
              onChanged: (String val) {
                change(val);
              },
              // readOnly: true,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              keyboardType: keyboardType,
              inputFormatters: <TextInputFormatter>[
                // FilteringTextInputFormatter.digitsOnly,
                FilteringTextInputFormatter.deny(","),
                FilteringTextInputFormatter.deny("-"),
                Leading(mantissa: 5),
                CustomMaxValueInputFormatter(maxInputValue: limit)
                // CustomMaxValueInputFormatter()
                // FilteringTextInputFormatter.
                // FilteringTextInputFormatter.allow(RegExp(r'^ ?\d*'))
              ],
              validator: (value) {
                if (value == null) {
                  return null;
                }
              },
              // buildCounter: (context,
              //             {required currentLength,
              //             required isFocused,
              //             maxLength}) =>
              // SizedBox(),

              textAlign: TextAlign.end,
              cursorColor: AppColor.brandColor2,
              decoration: InputDecoration(
                border: UnderlineInputBorder(
                    borderSide: BorderSide(style: BorderStyle.none, width: 0)),
              ),
            ),
          ),
          verticalDivider(),
          InkWell(
            onTap: () {
              double count = double.tryParse(value.text) ?? 0;
              if (count >=1) count--;
              value.text = count.toString();
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 13, vertical: 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: AppColor.black.withOpacity(0.8),
                boxShadow: [
                  CustomBoxShadow(
                    color: AppColor.brandColor2,
                    blurRadius: 10.0,
                  )
                ],
              ),
              child: Text(
                "-",
                style: whiteText,
              ),
            ),
          ),
          verticalDivider(),
          InkWell(
            onTap: () {
              double count = double.tryParse(value.text) ?? 0;
              if (count < limit-1) count++;
              value.text = count.toString();
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: AppColor.black.withOpacity(0.8),
                boxShadow: [
                  CustomBoxShadow(
                    color: AppColor.brandColor2,
                    blurRadius: 10.0,
                  )
                ],
              ),
              child: Text(
                "+",
                style: whiteText,
              ),
            ),
          ),
          verticalDivider(),
        ],
      ),
    );
  }

  verticalDivider() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 7),
      width: 1,
      height: 30,
      color: AppColor.gray,
    );
  }

  textStyle() {
    return shadow
        ? TextStyle(
            fontSize: 16,
            color: AppColor.white,
            fontFamily: AppFont.MontserratBold,
            fontWeight: FontWeight.w600,
            shadows: [
                Shadow(
                  offset: Offset(1.0, 1.0),
                  blurRadius: 10.0,
                  color: AppColor.shadow.withOpacity(0.3),
                ),
                Shadow(
                  offset: Offset(1.0, 1.0),
                  blurRadius: 10.0,
                  color: AppColor.shadow.withOpacity(0.3),
                ),
              ])
        : TextStyle(
            fontSize: 16,
            color: AppColor.white,
            fontFamily: AppFont.MontserratBold,
            fontWeight: FontWeight.w600,
          );
  }
}

class CustomMaxValueInputFormatter extends TextInputFormatter {
  late final double maxInputValue;
  CustomMaxValueInputFormatter({required this.maxInputValue});
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    final TextSelection selection = newValue.selection;
    String truncated = newValue.text;

    if (maxInputValue != null) {
      final double value = double.tryParse(newValue.text)??0;
      // if (value == null) {
      //   return TextEditingValue(
      //     text: truncated,
      //     selection: selection,
      //   );
      // }
      if (value > maxInputValue) {
        truncated = maxInputValue.toString();
      }
    }
    return TextEditingValue(text: truncated,selection: 
    selection);
  }
}
class Leading extends TextInputFormatter {
  final int mantissa;
  Leading({required this.mantissa});
  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    if (newValue.text.startsWith('0') && newValue.text.length > mantissa + 2) {
      return TextEditingValue().copyWith(
        text: newValue.text.replaceAll(RegExp(r'^0+(?=.)'), ''),
        selection: newValue.selection.copyWith(
          baseOffset: newValue.text.length - (mantissa + 2),
          extentOffset: newValue.text.length - (mantissa + 2),
        ),
      );
    }
     if (newValue.text.startsWith('0') && newValue.text.length > mantissa + 1) {
      return TextEditingValue().copyWith(
        text: newValue.text,
        selection: newValue.selection.copyWith(
          baseOffset: newValue.text.length - (mantissa + 1),
          extentOffset: newValue.text.length - (mantissa + 1),
        ),
      );
    }

    else {
      return newValue;
    }
  }
  }

