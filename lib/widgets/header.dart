import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/utils/gradient_mask.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CustomHeader extends StatelessWidget {
  final Function menuPressed;
  final Function rightButton;
  final bool icon;
  final bool leftIcon;
  const CustomHeader(
      {Key? key,
      required this.menuPressed,
      required this.rightButton,
      required this.icon,
      this.leftIcon = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double statusBarHeight = MediaQuery.of(context).viewPadding.top;
    print("STATUSBAR:$statusBarHeight");
    return Padding(
      padding:  EdgeInsets.only(top: statusBarHeight, bottom: 10),
      child: Container(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Material(
                color: AppColor.transparent,
                child: InkWell(
                  onTap: () => menuPressed(),
                  child: menu(),
                ),
              ),
              SvgPicture.asset(Images.brandName),
              InkWell(onTap: () => rightButton(), child: rightIcon())
            ],
          ),
        ),
      ),
    );
  }

  Widget menu() {
    return leftIcon
        ? Container(
            padding: EdgeInsets.symmetric(horizontal: 14,vertical: 8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                gradient: RadialGradient(
                  colors: [
                    AppColor.brandColor.withOpacity(0.3),
                    AppColor.black.withOpacity(0.6),
                  ],
                )),
            child: Center(
              child: SvgPicture.asset(
                Images.leftArrow,
              ),
            ))
        : Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                gradient: RadialGradient(
                  colors: [
                    AppColor.brandColor.withOpacity(0.3),
                    AppColor.black.withOpacity(0.6),
                  ],
                )),
            child: SvgPicture.asset(
              Images.menu,
            ));
  }

  Widget rightIcon() {
    return icon
        ? Container(
            height: 46,
            width: 46,
            decoration: BoxDecoration(
                // color: AppColor.black.withOpacity(0.5),
                gradient: RadialGradient(
                  colors: [
                    AppColor.brandColor.withOpacity(0.1),
                    AppColor.black.withOpacity(0.5),
                    // AppColor.black.withOpacity(0.4),
                  ],
                ),
                borderRadius: BorderRadius.circular(23)),
            child: LinearGradientMask(
              child: Padding(
                padding: const EdgeInsets.only(top: 3),
                child: ClipOval(
                  child: SizedBox.fromSize(
                      // size: Size.fromRadius(10), // Image radius
                      child: SvgPicture.asset(Images.account)),
                ),
              ),
            ),
          )
        : Container(
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              gradient: RadialGradient(
                colors: [
                  AppColor.brandColor2.withOpacity(0.1),
                  AppColor.black.withOpacity(0.9),
                ],
              ),
            ),
            child: SvgPicture.asset(Images.searchIcon),
          );
  }
}
