import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DateTimeInput extends StatelessWidget {
  final String label;
  final dynamic value;
  final dynamic dateTimePicker;
  final dynamic minimunDate;

  DateTimeInput(
      {Key? key,
      required this.label,
      required this.value,
      required this.dateTimePicker,
      this.minimunDate})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    void _showDatePicker(ctx) {
      // showCupertinoModalPopup is a built-in function of the cupertino library
      showCupertinoModalPopup(
          context: ctx,
          builder: (_) => Container(
                height: 500,
                color: AppColor.black.withOpacity(0.8),
                child: Column(
                  children: [
                    Container(
                      height: 400,
                      // color: AppColor.black.withOpacity(0.5),
                      child: CupertinoTheme(
                        data: CupertinoThemeData(
                          brightness: Brightness.dark,
                        ),
                        child: CupertinoDatePicker(
                            mode: CupertinoDatePickerMode.date,
                            initialDateTime: DateTime.now(),
                            // backgroundColor: AppColor.black.withOpacity(0.5),
                            // firstDate: DateTime(2019, 1),
                            // lastDate: DateTime.now(),
                            maximumDate: DateTime.now(),
                            minimumDate: minimunDate,
                            // maximumYear: DateTime.now().year,
                            onDateTimeChanged: (val) {
                              dateTimePicker(val);
                            }),
                      ),
                    ),

                    // Close the modal
                    CupertinoButton(
                      child: Text('OK',style: ligthTextStyle,),
                      onPressed: () => Navigator.of(ctx).pop(),
                    )
                  ],
                ),
              ));
    }

    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 5,
      ),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 3, vertical: 5),
          child: Text(
            label,
            style: TextStyle(
              color: AppColor.white,
              fontFamily: AppFont.MontserratMedium,
              fontSize: 16,
              fontWeight: FontWeight.w600
            ),
            // textAlign: TextAlign.start,
          ),
        ),
        TextFormField(
          controller: value,
          readOnly: true,
          keyboardType: TextInputType.datetime,
          decoration: InputDecoration(
            hintText: label,
            hintStyle:
                TextStyle(color: AppColor.white,fontSize: 14, fontFamily: AppFont.MontserratRegular),

            fillColor: AppColor.black.withOpacity(0.5),
            // labelText: 'Номер',
            
            floatingLabelBehavior: FloatingLabelBehavior.auto,
            labelStyle: const TextStyle(
                fontSize: 14.0, height: 0.7, color: AppColor.brandColor),
            // contentPadding: const EdgeInsets.only(
            //     left: 14.0, bottom: 18.0, top: 15.0),
            filled: true,
            isDense: true,
            suffixIcon: IconButton(
              onPressed: () => _showDatePicker(context),
              icon: Icon(
                CupertinoIcons.calendar,
                color: AppColor.brandColor2,
                size: 30,
              ),
            ),
            enabledBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(color: AppColor.brandColor),
            ),
            focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(color: AppColor.brandColor2),
            ),
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(color: AppColor.brandColor),
            ),
          ),
          style: whiteText.copyWith(fontSize: 13),
          // controller: _emailController,
          autocorrect: false,
          // validator: _validateEmail,
        ),
      ]),
    );
  }
}
