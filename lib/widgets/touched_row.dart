import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class TouchedRow extends StatelessWidget {
  final String text1, text2;
  final dynamic color;
  final Function onPress;
  final bool shadow;
  const TouchedRow(
      {Key? key,
      required this.text1,
      required this.text2,
      required this.onPress,
      this.color = AppColor.white,
      this.shadow = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColor.transparent,
      child: InkWell(
        onTap: () => onPress(),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 3),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text(text1, style: textStyle()),
              ),
              Text(text2,style: whiteText.copyWith(fontSize: 15),),
              SvgPicture.asset(Images.rigthArrow),
            ],
          ),
        ),
      ),
    );
  }

  textStyle() {
    return shadow
        ? TextStyle(
            fontSize: 16,
            color: AppColor.white,
            fontFamily: AppFont.MontserratBold,
            fontWeight: FontWeight.w600,
            shadows: [
                Shadow(
                  offset: Offset(1.0, 1.0),
                  blurRadius: 10.0,
                  color: AppColor.shadow.withOpacity(0.3),
                ),
                Shadow(
                  offset: Offset(1.0, 1.0),
                  blurRadius: 10.0,
                  color: AppColor.shadow.withOpacity(0.3),
                ),
              ])
        : TextStyle(
           fontSize: 16,
            color: AppColor.white,
            fontFamily: AppFont.MontserratBold,
            fontWeight: FontWeight.w600,
        );
  }
}
