import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/widgets/formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
// import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:intl/intl.dart';
import 'package:pattern_formatter/numeric_formatter.dart';
import '';
// import 'package:pattern_formatter/pattern_formatter.dart';

class TextInputWithTitle extends StatelessWidget {
  final String text1;
  final dynamic value;
  final dynamic color;
  final bool shadow;
  final dynamic keyboardType;
  final dynamic validator;
  final dynamic mantissa;
  final VoidCallback onSubmit;

  const TextInputWithTitle({
    Key? key,
    required this.text1,
    required this.value,
    required this.keyboardType,
    required this.validator,
    this.color = AppColor.white,
    this.shadow = true,
    this.mantissa = 2,
    required this.onSubmit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // value.selection = TextSelection.fromPosition(TextPosition(offset: value.text.length-3));
    //  this.value..text = viewModel.text
    print("CVALUE:${value.text}");
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            text1,
            style: textStyle(),
          ),
          SizedBox(width: 20),
          Flexible(
            child: TextFormField(
              controller: value,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              inputFormatters: [
                // ThousandsFormatter(formatter: NumberFormat('#,###.'+'0'*mantissa, "en_US"),allowFraction: true),

                // CurrencyInputFormatter(),
                // FilteringTextInputFormatter.allow(
                //     RegExp(r'[0-9]+[,.]{0,1}[0-9]*')),
                // TextInputFormatter.withFunction(
                  
                //   (oldValue, newValue) => newValue.copyWith(
                //     text: newValue.text.replaceAll(RegExp(r'\B(?=(\d{3})+(?!\d))'), ','),

                //   ),
                // ),
                // CurrencyTextInputFormatter(locale: 'en_US', name: "", decimalDigits:4),
                // ThousandsFormatter(formatter: NumberFormat.compactSimpleCurrency(locale: 'en_US',name:'',decimalDigits: 2),allowFraction: true),
                FilteringTextInputFormatter.deny(RegExp(r'-')),
                MoneyInputFormatter(
                  mantissaLength: mantissa,
                  thousandSeparator: ThousandSeparator.Comma,
                ),
                NoLeadingZeros(mantissa:mantissa)
                //  CurrencyInputFormatter()
                // MoneyMaskedTextController()
              ],
              // onFieldSubmitted: (value){
              // MoneyInputFormatter().isZero(value);
              // },
              
              style: whiteText,
              keyboardType: TextInputType.numberWithOptions(
                  decimal: true, signed: true), //keyboardType,
              textAlign: TextAlign.end,
              cursorColor: AppColor.brandColor2,
              decoration: InputDecoration(
                border: UnderlineInputBorder(
                    borderSide: BorderSide(style: BorderStyle.none, width: 0)),
                // errorStyle: TextStyle()   
                errorMaxLines: 3
              ),
              validator: (value) => validator(value),
              onFieldSubmitted:(String value){
                onSubmit();
              } ,
            ),
          ),
        ],
      ),
    );
  }

  textStyle() {
    return shadow
        ? TextStyle(
            fontSize: 16,
            color: AppColor.white,
            fontFamily: AppFont.MontserratBold,
            fontWeight: FontWeight.w600,
            shadows: [
                Shadow(
                  offset: Offset(1.0, 1.0),
                  blurRadius: 10.0,
                  color: AppColor.shadow.withOpacity(0.3),
                ),
                Shadow(
                  offset: Offset(1.0, 1.0),
                  blurRadius: 10.0,
                  color: AppColor.shadow.withOpacity(0.3),
                ),
              ])
        : TextStyle(
            fontSize: 16,
            color: AppColor.white,
            fontFamily: AppFont.MontserratBold,
            fontWeight: FontWeight.w600,
          );
  }
}

// class CurrencyInputFormatter extends TextInputFormatter {
//   @override
//   TextEditingValue formatEditUpdate(
//       TextEditingValue oldValue, TextEditingValue newValue) {
//     if (newValue.selection.baseOffset == 0) {
//       print(true);
//       return newValue;
//     }

//     double value = double.tryParse(newValue.text) ?? 0;

//     final formatter =  NumberFormat("###,##0.00#", "en_US");

//     String newText = formatter.format(value / 100);

//     return newValue.copyWith(
//         text: newText,
//         selection: TextSelection.collapsed(offset: newText.length));
//   }

// }

class CurrencyInputFormatter extends TextInputFormatter {

    TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {

        if(newValue.selection.baseOffset == 0){
            print(true);
            return newValue;
        }

        double value = double.parse(newValue.text);

        final formatter = NumberFormat.simpleCurrency(locale: "en_US",name:'',decimalDigits: 4);

        String newText = formatter.format(value/100);

        return newValue.copyWith(
            text: newText,
            selection: new TextSelection.collapsed(offset: newText.length));
    }
}

class NoLeadingZeros extends TextInputFormatter {
  final int mantissa;
  NoLeadingZeros({required this.mantissa});
  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    if (newValue.text.startsWith('0') && newValue.text.length > mantissa + 2) {
      return TextEditingValue().copyWith(
        text: newValue.text.replaceAll(RegExp(r'^0+(?=.)'), ''),
        selection: newValue.selection.copyWith(
          baseOffset: newValue.text.length - (mantissa + 2),
          extentOffset: newValue.text.length - (mantissa + 2),
        ),
      );
    }
     if (newValue.text.startsWith('0') && newValue.text.length > mantissa + 1) {
      return TextEditingValue().copyWith(
        text: newValue.text,
        selection: newValue.selection.copyWith(
          baseOffset: newValue.text.length - (mantissa + 1),
          extentOffset: newValue.text.length - (mantissa + 1),
        ),
      );
    }
// else if(newValue.text == '0'){
//    return TextEditingValue().copyWith(
//     text: newValue.text.replaceAll(RegExp(r'^0'), ''),
//     selection: newValue.selection.copyWith(
//       baseOffset: newValue.text.length - (mantissa + 1),
//       extentOffset: newValue.text.length - (mantissa + 1),
//     ),
//   );

// }
    else {
      return newValue;
    }
  }
}
