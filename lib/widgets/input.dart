import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:flutter/material.dart';

class Input extends StatelessWidget {
  final String title;
  final dynamic value;
  final dynamic onChange;
  final dynamic validator;
  final dynamic keyboardType;
  final dynamic maxLength;
  final dynamic icon;
  final dynamic onPress;

  // Input(  this.keyboardType);
  Input(
      {Key? key,
      required this.title,
      required this.value,
      required this.onChange,
      required this.validator,
      required this.keyboardType,
      this.maxLength = null,
      this.icon,
      this.onPress})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
        Container(
          padding: const EdgeInsets.only(left: 3, bottom: 10),
          child: Text(
            title,
            style: grayColor,
          ),
        ),
        Container(
          child: TextFormField(
            controller: value,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            maxLength: maxLength,
            keyboardType: keyboardType,
            decoration: InputDecoration(
              isDense: true,
              
              suffixIcon: icon != null
                  ? IconButton(
                      onPressed: () => {onPress()},
                      icon: Icon(
                        icon,
                        color: AppColor.brandColor,
                      ),
                    )
                  : null,
              // hintStyle: TextStyle(fontSize: 14,fontFamily: AppFont.MontserratRegular),
              fillColor: AppColor.black.withOpacity(0.5),
              filled: true,
              enabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                borderSide: BorderSide(color: AppColor.transparent),
              ),
              focusedBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                borderSide: BorderSide(color: AppColor.transparent),
              ),
              border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                borderSide: BorderSide(color: AppColor.transparent),
              ),
              focusedErrorBorder:const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                borderSide: BorderSide(color: AppColor.brandColor2),
              ),
              errorBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                borderSide: BorderSide(color: AppColor.brandColor2),
              ),

              errorStyle: TextStyle(
                color: AppColor.brandColor2,
                fontSize: null,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              ),
              // errorColor: Color(0xffd32f2f),
            ),
            
            style: whiteText,
            cursorColor: AppColor.white,
            validator: (String? value) => validator(value),
            onChanged: (value) => onChange(value),
          ),
        ),
      ]),
    );
  }
}
