import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SearchBar extends StatelessWidget {
  final String title;
  // final bool rightMenu;
  final double size;
  final Function ontap;
  const SearchBar({ Key? key ,required this.title,this.size=22,required this.ontap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double statusBarHeight = MediaQuery.of(context).padding.top;

    return  Container(
              margin: EdgeInsets.only(top: statusBarHeight),
              padding: EdgeInsets.symmetric(horizontal: 15),
              decoration: BoxDecoration(color: AppColor.black.withOpacity(0.5)),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                        padding:
                            EdgeInsets.only(top: 15, bottom:15,right: 20,left:10),
                        // color: AppColor.white,
                        child: SvgPicture.asset(Images.leftArrow)),
                  ),
                  Expanded(
                    child: Text(
                     title,
                     textAlign: TextAlign.center,
                     maxLines: 1,
                      style: TextStyle(
                        color: AppColor.white,
                        fontWeight: FontWeight.w600,
                        fontSize: size,
                        fontFamily: AppFont.MontserratBold,
                        shadows: <Shadow>[
                          Shadow(
                            offset: Offset(1.0, 1.0),
                            blurRadius: 15.0,
                            color: AppColor.shadow.withOpacity(0.4),
                          ),
                          Shadow(
                            offset: Offset(1.0, 1.0),
                            blurRadius: 15.0,
                            color: AppColor.shadow.withOpacity(0.4),
                          ),
                        ],
                      ),
                    ),
                  ),
                InkWell(
                  onTap: ()=>ontap(),
                  child: Container(
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              gradient: RadialGradient(
                  colors: [
                    AppColor.brandColor2.withOpacity(0.1),
                    AppColor.black.withOpacity(0.9),
                  ],
              ),
            ),
            child: SvgPicture.asset(Images.searchIcon),
          ),
                )
                ],
              ),
            );
  }
}