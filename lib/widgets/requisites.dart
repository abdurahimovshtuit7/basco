import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Requisites extends StatefulWidget {
  final String name;
  // final bool client;
  final Function storageNavigation;
  final Function currencyNavigation;
  final Function clientsNavigation;
  final dynamic currency;
  final dynamic store;
  final TextEditingController comment;
  final String count;
  final String total;
  final int itemLength;
  final bool send;
  final dynamic sinxTime;
  final dynamic date;
  final bool readOnly;
  final bool isTemplateSelected;
  final Function templateChanged;
  final Function toDate;

  Requisites(
      {Key? key,
      required this.name,
      // required this.client,
      required this.storageNavigation,
      required this.currencyNavigation,
      required this.clientsNavigation,
      required this.currency,
      required this.store,
      required this.comment,
      required this.count,
      required this.total,
      required this.itemLength,
      required this.send,
      required this.sinxTime,
      required this.date,
      required this.readOnly,
      required this.isTemplateSelected,
      required this.templateChanged,
      required this.toDate,
      })
      : super(key: key);

  @override
  State<Requisites> createState() => _RequisitesState();
}

class _RequisitesState extends State<Requisites> {
  // bool isTemplateSelected = false;
  late bool isSend = widget.send;
  late final _lastDate =
      TextEditingController(text: DateFormat("dd.MM.yyyy").format(DateTime.now()));

  @override
  Widget build(BuildContext context) {
    
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 12),
      padding: EdgeInsets.symmetric(horizontal: 19, vertical: 10),
      decoration: BoxDecoration(
          color: AppColor.black.withOpacity(0.5),
          borderRadius: BorderRadius.circular(16)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Row(
              children: [
                Expanded(
                    child: Text(
                  "Возврат товар",
                  textAlign: TextAlign.center,
                  style: ligthTextStyle.copyWith(fontSize: 18),
                )),
                CupertinoSwitch(
                  value: widget.isTemplateSelected,
                  onChanged: (bool value) {
                   widget.templateChanged(value);
                  },
                  activeColor: AppColor.brandColor2,
                ),
              ],
            ),
          ),
          Divider(color: AppColor.white),
          Row(
            children: [
              Text(
                "Ҳужжат вақти",
                style: ligthTextStyle.copyWith(fontSize: 16),
              ),
              Flexible(
                child: CustomDateTime(
                  label: 'гача:',
                  value: widget.date,
                  dateTimePicker: (item)=>widget.toDate(item),
                  minimunDate:widget.sinxTime,
                  length:widget.itemLength
                ),
              ),
            ],
          ),
          Divider(color: AppColor.white),
          TouchedRow(
            text1: "Омбор",
            text2: widget.store != null ? widget.store.ombor : '',
            onPress: () => widget.storageNavigation(),
          ),
          Divider(color: AppColor.white),

          TouchedRow(
            text1: "Мижоз",
            text2: widget.name!= null ? widget.name : '',
            onPress: () => widget.clientsNavigation(),
          ),
          // TextRow(
          //   text1: "Мижоз",
          //   text2: widget.name,
          //   color: AppColor.brandColor,
          // ),
          Divider(color: AppColor.white),
          TouchedRow(
            text1: "Валюта",
            text2: widget.currency != null ? widget.currency.valyutaNomi : "",
            onPress: () => widget.currencyNavigation(),
          ),
          Divider(color: AppColor.white),
          TextRow(
              text1: "Курс",
              text2: widget.currency != null
                  ? widget.currency.valyutaKursi.toString()
                  : ""),
          Divider(color: AppColor.white),
          TextRow(text1: "Жами товарлар суммаси", text2: widget.total),
          Divider(color: AppColor.white),
          TextRow(text1: "Жами товарлар сони", text2: widget.count),
          Divider(color: AppColor.white),
           widget.send?
          Column(
            children: [
              Container(
                child: Row(
                  children: [
                    Expanded(
                        child: Text(
                      "Серверга жунатилди",
                      // textAlign: TextAlign.center,
                      style: ligthTextStyle.copyWith(fontSize: 16),
                    )),
                    CupertinoSwitch(
                      value: isSend,
                      onChanged: (bool value) {
                        if(!widget.send){
                        setState(() {
                          isSend = value;
                        });
                        }
                        else {
                          setState(() {
                            isSend = true;
                          });
                        }
                       
                      },
                      activeColor: AppColor.brandColor2,
                    ),
                  ],
                ),
              ),
               Divider(color: AppColor.white),
            ],
          ):Container(),
          // Divider(color: AppColor.white),
          // TextRow(text1: "Жунатилган вакт", text2: "----"),
         
          Text("Изох", style: ligthTextStyle.copyWith(fontSize: 16)),
          TextFormField(
            controller:widget.comment,
            maxLines: 3,
            readOnly: widget.readOnly,
            cursorColor: AppColor.brandColor2,
            style: whiteText,
            decoration: InputDecoration(
              border: UnderlineInputBorder(
                  borderSide: BorderSide(style: BorderStyle.none, width: 0)),
            ),
          ),
         
        ],
      ),
    );
  }

 
}
