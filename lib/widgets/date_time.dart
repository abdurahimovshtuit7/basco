import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomDateTime extends StatelessWidget {
  final String label;
  final dynamic value;
  final dynamic dateTimePicker;
  final dynamic minimunDate;
  final int length;

  CustomDateTime(
      {Key? key,
      required this.label,
      required this.value,
      required this.dateTimePicker,
      this.minimunDate,
      required this.length
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    void _showDatePicker(ctx) {
      // showCupertinoModalPopup is a built-in function of the cupertino library
      showCupertinoModalPopup(
          context: ctx,
          builder: (_) => Container(
                height: 500,
                color: AppColor.black.withOpacity(0.8),
                child: Column(
                  children: [
                    Container(
                      height: 400,
                      // color: AppColor.black.withOpacity(0.5),
                      child: CupertinoTheme(
                        data: CupertinoThemeData(
                          brightness: Brightness.dark,
                        ),
                        child: CupertinoDatePicker(
                            mode: CupertinoDatePickerMode.date,
                            initialDateTime: DateTime.now(),
                            // backgroundColor: AppColor.black.withOpacity(0.5),
                            // firstDate: DateTime(2019, 1),
                            // lastDate: DateTime.now(),
                            maximumDate: DateTime.now(),
                            minimumDate: minimunDate,
                            // maximumYear: DateTime.now().year,
                            onDateTimeChanged: (val) {
                              dateTimePicker(val);
                            }),
                      ),
                    ),

                    // Close the modal
                    CupertinoButton(
                      child: Text('OK',style: ligthTextStyle,),
                      onPressed: () => Navigator.of(ctx).pop(),
                    )
                  ],
                ),
              ));
    }

    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 5,
      ),
      child: TextFormField(
        controller: value,
        readOnly: true,
        onTap: () {
          if(length == 0){
            _showDatePicker(context);
          }
        } ,
        keyboardType: TextInputType.datetime,
        textAlign: TextAlign.end,
        decoration: InputDecoration(
          hintText: label,
          hintStyle:
              TextStyle(color: AppColor.white,fontSize: 14, fontFamily: AppFont.MontserratRegular),
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          labelStyle: const TextStyle(
              fontSize: 14.0, height: 0.7, color: AppColor.brandColor),
          contentPadding: const EdgeInsets.only(
              bottom: 10.0, top: 10.0),
          filled: true,
          isDense: true,
          border: InputBorder.none,
         
        ),
        style: whiteText.copyWith(fontSize:14),
        // controller: _emailController,
        autocorrect: false,
        // validator: _validateEmail,
      ),
    );
  }
}
