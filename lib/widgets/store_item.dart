import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:flutter/material.dart';

class StoreItem extends StatelessWidget {
  final String text1;
  final Function onPress;
  final int index;
  const StoreItem(
      {Key? key,
      required this.text1,
      required this.onPress,
      required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColor.transparent,
      child: InkWell(
        onTap: () => onPress(),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 4),
          // padding:
          //     EdgeInsets.symmetric(vertical: 15, horizontal: 10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: AppColor.black.withOpacity(0.5)),
          child: Row(
            children: [
              Container(
                height: 60,
                width: 60,
                decoration: BoxDecoration(
                  color: AppColor.black.withOpacity(0.5),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Center(
                    child: Text(
                  (index+1).toString(),
                  style: whiteText,
                )),
              ),
              SizedBox(width: 17),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Text(
                        text1,
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        style: TextStyle(
                            color: AppColor.white,
                            fontSize: 18,
                            fontFamily: AppFont.MontserratBold,
                            overflow: TextOverflow.ellipsis),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 15)
            ],
          ),
        ),
      ),
    );
  }
}
