import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/colors.dart';
import 'package:flutter/material.dart';
class TextRow extends StatelessWidget {
  final String text1,text2;
  final dynamic color;
  final bool shadow;

  const TextRow({ Key? key,required this.text1,required this.text2,  this.color =AppColor.white, this.shadow = true }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            text1,
            style: textStyle(),
          ),
          Expanded(
            child: Text(
              text2,
              textAlign: TextAlign.end,
              overflow: TextOverflow.ellipsis,
               maxLines: 2,
              style: TextStyle(
                fontSize: 16,
                color: color,
                fontFamily: AppFont.MontserratBold,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ],
      ),
    );
  }
   textStyle() {
    return shadow
        ? TextStyle(
            fontSize: 16,
            color: AppColor.white,
            fontFamily: AppFont.MontserratBold,
            fontWeight: FontWeight.w600,
            shadows: [
                Shadow(
                  offset: Offset(1.0, 1.0),
                  blurRadius: 10.0,
                  color: AppColor.shadow.withOpacity(0.3),
                ),
                Shadow(
                  offset: Offset(1.0, 1.0),
                  blurRadius: 10.0,
                  color: AppColor.shadow.withOpacity(0.3),
                ),
              ])
        : TextStyle(
           fontSize: 16,
            color: AppColor.white,
            fontFamily: AppFont.MontserratBold,
            fontWeight: FontWeight.w600,
        );}
}