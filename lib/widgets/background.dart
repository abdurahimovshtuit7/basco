import 'package:basco/constants/images.dart';
import 'package:flutter/material.dart';

class BackgroundView extends StatefulWidget {
  final Widget child;
  const BackgroundView({Key? key, required this.child}) : super(key: key);

  @override
  State<BackgroundView> createState() => _BackgroundViewState();
}

class _BackgroundViewState extends State<BackgroundView> {
  void didChangeDependencies() {
    precacheImage(AssetImage(Images.background), context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(Images.background), fit: BoxFit.fill)),
      child: widget.child,
    );
  }
  
}

