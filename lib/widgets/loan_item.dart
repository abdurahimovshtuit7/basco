import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/utils/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class LoanItem extends StatelessWidget {
  final String name,currency,value;
  final Function onPress;
  final int index;
  const LoanItem({ Key? key,required this.name,required this.currency,required this.value,required this.onPress,required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColor.transparent,
      child: InkWell(
        onTap: ()=>onPress(),
        child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                          // padding:
                          //     EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: AppColor.black.withOpacity(0.5)),
                          child: Row(
                            children: [
                              Container(
                                  height: 50,
                                  width: 50,
                                  decoration: BoxDecoration(
                                      color: AppColor.black.withOpacity(0.5),
                                      borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Center(child: Text((index+1).toString(),style: whiteText,)),
                                      ),
                              SizedBox(width: 8),
                              Expanded(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                              
                                     Expanded(
                                       flex: 3,
                                       child: Text(
                                        name,
                                        // textAlign: TextAlign.end,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: AppColor.white,
                                            fontSize: 18,
                                            fontFamily: AppFont.MontserratBold,
                                            overflow: TextOverflow.ellipsis
                                            ),
                                    ),
                                     ),
                                    Container(
                                      height: 30,
                                      width: 1,
                                      margin: EdgeInsets.symmetric(vertical: 2,horizontal: 2),
                                      decoration: BoxDecoration(border: Border.all(color: AppColor.brandColor2
                                      )) ,),
                                    Expanded(
                                      flex: 2,
                                       child: Text(
                                        currency
                                        ,
                                        textAlign: TextAlign.center,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: AppColor.white,
                                            fontSize: 18,
                                            fontFamily: AppFont.MontserratBold,
                                            overflow: TextOverflow.ellipsis
                                            ),
                                    ),
                                     ),
                                      Container(
                                      height: 30,
                                      width: 1,
                                      margin: EdgeInsets.symmetric(vertical: 2,horizontal: 2),
                                      decoration: BoxDecoration(border: Border.all(color: AppColor.brandColor2
                                      )) ,),
                                     Expanded(
                                       flex: 3,
                                       child: Text(
                                        Services.currencyFormatter(value),
                                        textAlign: TextAlign.end,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: AppColor.white,
                                            fontSize: 18,
                                            fontFamily: AppFont.MontserratBold,
                                            overflow: TextOverflow.ellipsis
                                            ),
                                    ),
                                     ),
                                  ],
                                ),
                                
                              ),
                              SizedBox(width: 15)
                            ],
                          ),
                        ),
      ),
    );
  }
}