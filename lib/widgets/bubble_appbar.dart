import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
class BubbleAppBar extends StatelessWidget {
  final String title;
  final Function onpress;
  const BubbleAppBar({ Key? key,required this.title ,required this.onpress}) : super(key: key);

  @override
  Widget build(BuildContext context) {
        double statusBarHeight = MediaQuery.of(context).padding.top;

    return Container(
              margin: EdgeInsets.only(top: statusBarHeight,left: 12,right: 12),
              padding: EdgeInsets.symmetric(horizontal: 15),
              decoration: BoxDecoration(color: AppColor.black.withOpacity(0.5),borderRadius: BorderRadius.circular(16)),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                        // color: AppColor.white,
                        child: SvgPicture.asset(Images.leftArrow)),
                  ),
                  Material(
                    color: AppColor.transparent,
                    child: InkWell(
                      onTap: (){onpress();},
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                        child: Text(
                         title,
                          style: ligthTextStyle.copyWith(fontSize: 20)
                        ),
                      ),
                    ),
                  ),
                 
                ],
              ),
            );
  }
}