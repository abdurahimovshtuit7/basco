import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AccountAppBar extends StatelessWidget {
  final String title;
  final Function addButton;
  final Function toolsButton;
  const AccountAppBar({Key? key, required this.title,required this.addButton,required this.toolsButton})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double statusBarHeight = MediaQuery.of(context).padding.top;

    return Container(
      margin: EdgeInsets.only(top: statusBarHeight),
      padding: EdgeInsets.symmetric(horizontal: 13),
      decoration: BoxDecoration(color: AppColor.black.withOpacity(0.5)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Text(
              title,
              textAlign: TextAlign.center,
              maxLines: 1,
              style: TextStyle(
                color: AppColor.white,
                fontWeight: FontWeight.w600,
                fontSize: 22,
                fontFamily: AppFont.MontserratBold,
                shadows: <Shadow>[
                  Shadow(
                    offset: Offset(1.0, 1.0),
                    blurRadius: 15.0,
                    color: AppColor.shadow.withOpacity(0.4),
                  ),
                  Shadow(
                    offset: Offset(1.0, 1.0),
                    blurRadius: 15.0,
                    color: AppColor.shadow.withOpacity(0.4),
                  ),
                ],
              ),
            ),
          ),
          Material(
             color: AppColor.transparent,
              child: InkWell(
                onTap: ()=>addButton(),
                  child: SvgPicture.asset(
            Images.plus,
            color: AppColor.white.withOpacity(0.7),
            height: 27,
          ))),
          SizedBox(width: 10),
          Material(
             color: AppColor.transparent,
              child: InkWell(
                onTap: ()=>toolsButton,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: SvgPicture.asset(Images.rigthMenu),
                  ))),
          
          //  rightMenu?SvgPicture.asset(Images.rigthMenu):Container(),
        ],
      ),
    );
  }
}
