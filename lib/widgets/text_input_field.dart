import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:flutter/material.dart';

class TextInputField extends StatelessWidget {
  final String text1;
  final dynamic value;
  final dynamic color;
  final bool shadow;
  final dynamic keyboardType;
  final dynamic validator;

  const TextInputField(
      {Key? key,
      required this.text1,
      required this.value,
      required this.keyboardType,
      required this.validator,
      this.color = AppColor.white,
      this.shadow = true,
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            text1,
            style: textStyle(),
          ),
          SizedBox(width: 20),
          Flexible(
            child: TextFormField(
              controller: value,
              autovalidateMode:AutovalidateMode.onUserInteraction ,
              style:whiteText,
              keyboardType:keyboardType ,
              textAlign: TextAlign.end,
              cursorColor: AppColor.brandColor2,
              decoration: InputDecoration(
                border: UnderlineInputBorder(
                    borderSide: BorderSide(style: BorderStyle.none, width: 0)),
              ),
              validator: (value)=> validator(value),
              
            ),
          ),
        ],
      ),
    );
  }

  textStyle() {
    return shadow
        ? TextStyle(
            fontSize: 16,
            color: AppColor.white,
            fontFamily: AppFont.MontserratBold,
            fontWeight: FontWeight.w600,
            shadows: [
                Shadow(
                  offset: Offset(1.0, 1.0),
                  blurRadius: 10.0,
                  color: AppColor.shadow.withOpacity(0.3),
                ),
                Shadow(
                  offset: Offset(1.0, 1.0),
                  blurRadius: 10.0,
                  color: AppColor.shadow.withOpacity(0.3),
                ),
              ])
        : TextStyle(
            fontSize: 16,
            color: AppColor.white,
            fontFamily: AppFont.MontserratBold,
            fontWeight: FontWeight.w600,
          );
  }
}
