import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/constants/images.dart';
import 'package:basco/dialog/custom_dialogs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:url_launcher/url_launcher.dart';

class Item extends StatelessWidget {
  final String name;
  final String phone;
  final String address;
  final Function onPress;
  final String image;
  const Item(
      {Key? key,
      required this.name,
      required this.phone,
      required this.address,
      required this.onPress,
      required this.image})
      : super(key: key);

  Future<void> _makePhoneCall(String url) async {
    if (await canLaunch('tel:$url')) {
      await launch('tel:$url');
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColor.transparent,
      child: InkWell(
          onTap: () => onPress(),
          onLongPress: () => DialogUtils.showCustomDialog(context,
              text: name, okBtnFunction: () => _makePhoneCall(phone)),
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 3, horizontal: 12),
            padding: EdgeInsets.symmetric(vertical: 13, horizontal: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                color: AppColor.black.withOpacity(0.5)),
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.all(4), // Border width
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: AppColor.avatarBorder1,
                          spreadRadius: 1,
                          blurRadius: 7,
                          offset: Offset(0, 0), // changes position of shadow
                        ),
                      ],
                      gradient: LinearGradient(colors: [
                        AppColor.avatarBorder1,
                        AppColor.avatarBorder2
                      ]),
                      shape: BoxShape.circle),
                  child: ClipOval(
                    child: SizedBox.fromSize(
                      size: Size.fromRadius(32), // Image radius
                      child: Image.network(
                        image,
                        fit: BoxFit.cover,
                        errorBuilder: (BuildContext context, Object exception,
                            StackTrace? stackTrace) {
                          return Image.asset(Images.profile);
                        },
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 13,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 4),
                        child: Text(
                          name,
                          maxLines: 2,
                          style: TextStyle(
                            color: AppColor.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                            fontFamily: AppFont.MontserratBold,
                            shadows: <Shadow>[
                              Shadow(
                                offset: Offset(1.0, 1.0),
                                blurRadius: 15.0,
                                color: AppColor.shadow.withOpacity(0.4),
                              ),
                              Shadow(
                                offset: Offset(1.0, 1.0),
                                blurRadius: 15.0,
                                color: AppColor.shadow.withOpacity(0.4),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          SvgPicture.asset(Images.phone),
                          SizedBox(width: 6),
                          Text(
                            phone,
                            style: TextStyle(
                              color: AppColor.brandColor2,
                              fontSize: 15,
                              fontFamily: AppFont.MontserratBold,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          SvgPicture.asset(Images.location),
                          SizedBox(width: 6),
                          Text(
                            address,
                            style: TextStyle(
                              color: AppColor.brandColor2,
                              fontSize: 15,
                              fontFamily: AppFont.MontserratBold,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          )),
    );
  }
}
