import 'package:basco/constants/app_font.dart';
import 'package:basco/constants/colors.dart';
import 'package:basco/utils/gradient_mask.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class IconWithButton extends StatelessWidget {
  final String image;
  final String text;
  final Function onPress;
  const IconWithButton({ Key? key ,required this.image,required this.text,required this.onPress}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 19, vertical: 11),
        child: InkWell(
          onTap:()=>onPress(),
          child: Container(
            height: 156,
            width: 100,
            padding: EdgeInsets.symmetric(
              horizontal:10
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                color: AppColor.black.withOpacity(0.5)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                LinearGradientMask(
                    child: SvgPicture.asset(
                  image,
                )),
                SizedBox(height: 26),
                Text(
                  text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: AppColor.white,
                    fontWeight: FontWeight.w600,
                    fontSize: 17,
                    fontFamily: AppFont.MontserratBold,
                    shadows: <Shadow>[
                      Shadow(
                        offset: Offset(1.0, 1.0),
                        blurRadius: 15.0,
                        color: AppColor.shadow.withOpacity(0.4),
                      ),
                      Shadow(
                        offset: Offset(1.0, 1.0),
                        blurRadius: 15.0,
                        color: AppColor.shadow.withOpacity(0.4),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );
  }
}