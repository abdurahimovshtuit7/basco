import 'package:basco/constants/app_text_style.dart';
import 'package:basco/constants/colors.dart';
import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  final String title;
  final Function onPress;
  final double margin;
  const Button({Key? key,required this.title,required this.onPress,this.margin = 20.0}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 0, vertical: margin),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                AppColor.brandColor,
                AppColor.brandColor2,
                AppColor.brandColor
              ])),
      child: Material(
        color: AppColor.transparent,
        child: InkWell(
          onTap: () => onPress(),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            child: Text(
             title,
              style: whiteText,
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
