import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/models/user.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:intl/intl.dart';

class Services {
  Services._();
  static final UserHive _hive = di.get<UserHive>();
  Map<String, dynamic> data = {};

  static currencyFormatter(String amount,{mantissa = 1}) {
    var format = NumberFormat("###,##0.00"+"#"*mantissa, "en_US");
    if (amount.isEmpty) {
      return format.format(0);
    }
    return format.format(double.parse(amount));
  }

  static translate(String language, String uz, String ru) {
    print("LAMGIA:$language");
    if (language == "ru") {
      return ru;
    } else if (language == "uz") {
      return uz;
    }
  }

   static Future<double> converter(valId1,sum,valId2) async {
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    List currency = accountData.data["user"]!=null?User.fromJson(accountData.data['user']).valyutalar :[];
      // setState(() {
      //   data = accountData.data;
      //   currency = data["user"]!=null?User.fromJson(data['user']).valyutalar:[];
      // });
    
   num a = currency.where((x)=>x.valyutaId == valId1).single.valyutaKursi as num;
   num b = currency.where((x)=>x.valyutaId == valId2).single.valyutaKursi as num;
   print("B: $b");
   print("CC:${valId1}");
   print("CC1:${sum}");
   print("CC3:${valId2}");
   print("a:$a");
    return (b/a)*sum;
  }
  
  static double converterWithoutAsync(valId1,sum,valId2,currency) {
   
   num a = currency.where((x)=>x.valyutaId == valId1).single.valyutaKursi as num;
   num b = currency.where((x)=>x.valyutaId == valId2).single.valyutaKursi as num;
 
    return (b/a)*sum;
  }
  

}


