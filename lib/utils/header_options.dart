import 'dart:convert';

import 'package:basco/di/locator.dart';
import 'package:basco/hive/hive.dart';
import 'package:basco/utils/shared_pref.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

// var  async url = await HeaderOptions.setUrl();

class HeaderOptions {
  static final UserHive _hive = di.get<UserHive>();
  
  static setUrl() async {
    final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);

    final data = accountData.data["settings"] ?? {};

    String path = data.isNotEmpty
        ? "http://${data["addressServer"]}:${data["port"]}/${data["folder"]}/hs/${data["api"]}/"
        : "";

    return path;
  }

  Future<Options> option() async {
     final baza = await _hive.getAccount();
    String name = await SharedPref().read("activeUser");

    final accountData = baza.firstWhere((x) => x.name == name);
    bool isFind =accountData.data["settings"]!=null;
    var username = isFind?accountData.data["settings"]["username"]:"";
    var password = isFind?accountData.data["settings"]["password"]:"";
    print("uuuuuu:${ base64Encode(utf8.encode('$username:$password'))}");
    return Options(
      headers: {
        "Accept": 'application/json',
        "Content-Type": "application/json",
        "Authorization": "Basic " + base64Encode(utf8.encode('$username:$password'))
      },
      contentType: Headers.formUrlEncodedContentType,
      receiveTimeout: 50000,
    );
  }

  static Future<Dio> getDio() async {
    String uri = await setUrl();
    BaseOptions options = BaseOptions(
      baseUrl: uri,
      responseType: ResponseType.json,
      connectTimeout: 60000,
      receiveTimeout: 50000,
    );
    Dio dio = Dio(options);
    return dio;
  }
}
