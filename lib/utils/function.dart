import 'package:flutter/material.dart';
import '../main.dart';

void showSnackBar(dynamic msg)  {
   snackBarKey.currentState?.showSnackBar(
    SnackBar(content: Text("$msg"), duration: const Duration(seconds: 3)),
  );
}