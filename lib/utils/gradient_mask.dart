import 'package:basco/constants/colors.dart';
import 'package:flutter/material.dart';
class LinearGradientMask extends StatelessWidget {
  LinearGradientMask({required this.child});
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (bounds) {
        return LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          // radius: 1,
          colors: [
            AppColor.brandColor.withOpacity(1),
            AppColor.brandColor2.withOpacity(0.8),
            AppColor.brandColor.withOpacity(1)
          ],
          // tileMode: TileMode.clamp,
        ).createShader(bounds);
      },
      child: child,
    );
  }
}