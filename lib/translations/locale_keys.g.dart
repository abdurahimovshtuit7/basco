// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const purpose = 'purpose';
  static const beginning = 'beginning';
  static const enter = 'enter';
  static const login = 'login';
  static const password = 'password';
  static const done = 'done';
  static const no_task = 'no_task';
  static const new_task = 'new_task';
  static const add_task = 'add_task';
  static const short_name = 'short_name';
  static const description = 'description';
  static const time = 'time';
  static const add = 'add';
  static const add_done = 'add_done';
  static const remain = 'remain';
  static const new_addition = 'new_addition';
  static const back_to_main = 'back_to_main';
  static const todays_task = 'todays_task';
  static const edit = 'edit';
  static const delete = 'delete';
  static const settings = 'settings';
  static const score = 'score';
  static const bill = 'bill';
  static const account = 'account';
  static const namoz = 'namoz';
  static const notes = 'notes';
  static const more = 'more';
  static const value = 'value';
  static const help = 'help';
  static const exit = 'exit';
  static const close = 'close';
  static const call = 'call';
  static const communicate = 'communicate';
  static const morning = 'morning';
  static const afternoon = 'afternoon';
  static const evening = 'evening';
  static const night = 'night';
  static const account_settings = 'account_settings';
  static const language = 'language';
  static const name = 'name';
  static const last_name = 'last_name';
  static const address = 'address';
  static const Bomdod = 'Bomdod';
  static const Sun = 'Sun';
  static const Peshin = 'Peshin';
  static const Asr = 'Asr';
  static const Shom = 'Shom';
  static const Xufton = 'Xufton';
  static const birthday = 'birthday';
  static const adult = 'adult';
  static const jinsi = 'jinsi';
  static const beginNamaz = 'beginNamaz';
  static const send = 'send';

}
